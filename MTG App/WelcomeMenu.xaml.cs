﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MtgLib;
using Microsoft.Win32;
using System.IO;
using System.Collections.ObjectModel;
using System.Net;
using MtgApp.Online;
using System.Net.Sockets;
using System.Web;
using System.Text.RegularExpressions;
using System.Windows.Threading;
using System.Threading;
using System.ComponentModel;

namespace MtgApp
{
    /// <summary>
    /// Interaction logic for WelcomeMenu.xaml
    /// </summary>
    public partial class WelcomeMenu : Window
    {
        public ObservableCollection<DeckElement> Player_One;
        public ObservableCollection<DeckElement> Player_Two;
        public string Player_One_Name;
        public string Player_Two_Name;
        public Socket MainSocket;

        DateTime LastPing;
        bool Hosting;
        bool LAN;
        IPAddress Address;
        Socket Listener;
        Socket Handler;
        byte[] Recieved;

        private bool StopReceiving;

        IPAddress OwnLocal;

        bool _connected;
        bool Connected
        {
            get
            {
                return _connected;
            }
            set
            {
                _connected = value;
                if (value)
                {
                    SolidColorBrush Br = new SolidColorBrush(Colors.LightGreen); ;
                    ellipse_connected.Fill = Br;
                    label_ip.Content = Handler.RemoteEndPoint.ToString();
                }
                else
                {
                    ellipse_connected.Fill = new SolidColorBrush(Colors.Red);
                    label_ip.Content = "Not Connected";
                }
            }
        }

        public WelcomeMenu(bool ishost, bool lan, IPAddress address)
        {
            InitializeComponent();
            Player_One = new ObservableCollection<DeckElement>();
            Player_Two = new ObservableCollection<DeckElement>();

            list_p1_colours.ItemsSource = Player_One;
            list_p2_colours.ItemsSource = Player_Two;

            Hosting = ishost;
            Address = address;

            LAN = lan;
        }

        private void button_p1_browse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog Open = new OpenFileDialog();
            Open.Filter = "Text Files (.txt)|*.txt";
            if (Open.ShowDialog().Value)
            {
                Player_One.Clear();
                Player_One.AddRange(JsonMtg.GetElements(File.ReadAllText(Open.FileName)));
                UpdateOpponent();
            }
        }

        private void button_p1_paste_Click(object sender, RoutedEventArgs e)
        {
            InputDeck Input = new InputDeck();
            if (Input.ShowDialog().Value)
            {
                Player_One.Clear();
                Player_One.AddRange(JsonMtg.GetElements(Input.cards));
                UpdateOpponent();
            }
        }

        private void button_p2_browse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog Open = new OpenFileDialog();
            Open.Filter = "Text Files (.txt)|*.txt";
            if (Open.ShowDialog().Value)
            {
                Player_Two.Clear();
                Player_Two.AddRange(JsonMtg.GetElements(File.ReadAllText(Open.FileName)));
                UpdateOpponent();
            }
        }

        private void button_p2_paste_Click(object sender, RoutedEventArgs e)
        {
            InputDeck Input = new InputDeck();
            if (Input.ShowDialog().Value)
            {
                Player_Two.Clear();
                Player_Two.AddRange(JsonMtg.GetElements(Input.cards));
                UpdateOpponent();
            }
        }

        private void Button_Done_Click(object sender, RoutedEventArgs e)
        {
            Done();

            if (radio_p2_real.IsChecked.Value)
                Handler.Send(new StartGame().ToByte());
        }

        public void Done()
        {
            App.Current.Dispatcher.Invoke((Action)delegate
         {
             if (radio_p2_real.IsChecked.Value)
             {
                 StopReceiving = true;
                 Handler.Send(new EndRecieve().ToByte());
                 Player_One_Name = Text_P1Name.Text;
                 Player_Two_Name = Text_P2Name.Text;
                 MainSocket = Handler;
             }

             if (Player_One.Count > 0 && Player_Two.Count > 0)
                 DialogResult = true;
             else
                 MessageBox.Show("Both players need a deck!");
         });
        }

        private string getExternalIp()
        {
            try
            {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                             .Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch { return null; }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //By default we are not connected
            Connected = false;

            //Work out external IP
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    OwnLocal = ip;
                    break;
                }
            }

            Listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Listener.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

            try
            {
                Listener.Bind(new IPEndPoint(OwnLocal, OwnPort));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            Recieved = new byte[99999];


            if (Hosting == false)
            {
                Handler = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                Handler.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                Handler.Bind(new IPEndPoint(OwnLocal, OwnPort));
                //Connect the socket to the other player
                Handler.Connect(Address, OppositePort);
                Connected = true;

                UpdateOpponent();
                Handler.BeginReceive(Recieved, 0, Recieved.Length, SocketFlags.None, new AsyncCallback(OnReceive), null);

                radio_p2_real.IsChecked = true;
                radio_p1_ai.IsEnabled = false;
                radio_p1_real.IsEnabled = false;
                radio_p2_ai.IsEnabled = false;
                radio_p2_real.IsEnabled = false;
                button_p1_browse.IsEnabled = false;
                button_p1_paste.IsEnabled = false;
                combo_p1ai.IsEnabled = false;
                combo_p2ai.IsEnabled = true;
                Text_P1Name.IsEnabled = false;
                combo_p2ai.IsEnabled = false;
                Button_Done.Visibility = System.Windows.Visibility.Hidden;
            }

            P1Real();
            if (Hosting)
                P2AI();
            else
                P2Real();
        }

        private void OnConnection(IAsyncResult ar)
        {
            Handler = Listener.EndAccept(ar);
            App.Current.Dispatcher.Invoke((Action)delegate
              {
                  Connected = true;
                  // Listener.Connect(Incoming.RemoteEndPoint);
              });
            Handler.BeginReceive(Recieved, 0, Recieved.Length, SocketFlags.None, new AsyncCallback(OnReceive), null);
        }

        int OppositePort
        {
            get
            {
                if (Hosting)
                    return 1001;
                else
                    return 1000;
            }
        }

        int OwnPort
        {
            get
            {
                if (Hosting)
                    return 1000;
                else
                    return 1001;
            }
        }

        private void OnReceive(IAsyncResult ar)
        {

            LastPing = DateTime.Now;
            OnlineData Data = OnlineData.FromByte(Recieved);
            if (Data is PlayerData)
            {
                PlayerData Opponent = PlayerData.FromByte(Recieved);
                if (Hosting)
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        Text_P2Name.Text = Opponent.Name;
                        Player_Two.Clear();
                        Player_Two.AddRange(Opponent.Deck);
                    });

                }
                else
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        Text_P1Name.Text = Opponent.Name;
                        Player_One.Clear();
                        Player_One.AddRange(Opponent.Deck);
                    });
                }

            }
            else if (Data is StartGame)
            {
                Done();
            }

            App.Current.Dispatcher.Invoke((Action)delegate
            {
                Connected = true;
            });
            Handler.EndReceive(ar);
            if (Handler.Connected == false)
                Handler.Connect(Handler.RemoteEndPoint);

            if (StopReceiving == false)
            {
                try
                {
                    Handler.BeginReceive(Recieved, 0, Recieved.Length, SocketFlags.None, new AsyncCallback(OnReceive), null);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }

        private void OnSend(IAsyncResult ar)
        {
            Handler.EndSend(ar);
        }

        private void P1Real()
        {
            if (Hosting == false)
                Text_P1Name.IsEnabled = true;
            combo_p1ai.IsEnabled = false;
            Text_P1Name.Focus();
        }

        private void P1AI()
        {
            Text_P1Name.IsEnabled = false;
            combo_p1ai.IsEnabled = true;
            combo_p1ai.Focus();

        }

        private void P2Real()
        {
            if (Hosting)
            {
                button_p2_browse.IsEnabled = false;
                button_p2_paste.IsEnabled = false;
                Text_P2Name.IsEnabled = false;
                combo_p2ai.IsEnabled = false;
            }
            grid_mp.Visibility = System.Windows.Visibility.Visible;
        }

        private void P2AI()
        {
            Text_P2Name.IsEnabled = false;
            combo_p2ai.IsEnabled = true;
            combo_p2ai.Focus();
            if (Text_P2Name.Text == "")
                Text_P2Name.Text = "Name";
            button_p2_browse.IsEnabled = true;
            button_p2_paste.IsEnabled = true;
            grid_mp.Visibility = System.Windows.Visibility.Hidden;
        }

        private void Text_P1Name_GotFocus(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
            {
                P1Real();
                if (Text_P1Name.Text == "Name")
                    Text_P1Name.Text = "";
            }
        }

        private void combo_p1ai_GotFocus(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
            {
                P1AI();
            }
        }

        private void radio_p1_real_Checked(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
            {
                P1Real();
            }
        }

        private void radio_p1_ai_Checked(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
            {
                P1AI();
            }
        }

        private void Text_P2Name_GotFocus(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
            {
                if (Hosting == false && Text_P2Name.Text == "Name")
                    Text_P2Name.Text = "";
            }
        }

        private void combo_p2ai_GotFocus(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
            {

            }
        }

        private void radio_p2_real_Checked(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
            {
                if (Handler == null)
                {
                    Listener.Listen(1);
                    BackgroundWorker Worker = new BackgroundWorker();
                    Worker.DoWork += new DoWorkEventHandler(Worker_DoWork);
                    Worker.RunWorkerAsync(Listener);
                }
                P2Real();
            }
        }

        void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //Set variables
            Socket sock = (Socket)e.Argument;
            bool StillChecking = true;

            //Loop forever until either the user cancels or we get a connection
            while (StillChecking)
            {
                App.Current.Dispatcher.Invoke((Action)delegate
                       {
                           StillChecking = radio_p2_real.IsChecked.Value && Handler == null;
                       });
                Thread.Sleep(500);
                sock.BeginAccept(new AsyncCallback(OnConnection), null);
            }
        }

        private void radio_p2_ai_Checked(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
            {
                P2AI();
            }
        }

        private void Text_P2Name_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Hosting && Text_P2Name.Text == "")
                Text_P2Name.Text = "Name";

            UpdateOpponent();
        }

        public void UpdateOpponent()
        {
            if (Handler == null)
                return;

            PlayerData send = new PlayerData();
            if (Hosting)
            {
                send.Deck = new List<DeckElement>(Player_One.ToArray());
                send.Name = Text_P1Name.Text;
            }
            else
            {
                send.Deck = new List<DeckElement>(Player_Two.ToArray());
                send.Name = Text_P2Name.Text;
            }

            try
            {
                Handler.Send(send.ToByte());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error sending data: " + ex.Message);
            }
        }

        private void Text_P1Name_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Text_P1Name.Text == "")
                Text_P1Name.Text = "Name";
            UpdateOpponent();
        }
    }

    public static class ObservableExtensions
    {
        public static void AddRange(this ObservableCollection<DeckElement> collection, IEnumerable<DeckElement> add)
        {
            foreach (DeckElement element in add)
            {
                collection.Add(element);
            }
        }
    }
}
