﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Windows;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Win32;
using MtgAI;
using MtgLib;
using MtgApp.Properties;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Controls;
using System.ComponentModel;
using StringExtensions;
using System.Reflection;
using System.Net.Sockets;
using MtgApp.Online;
using System.Threading;

namespace MtgApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Global Variables
        /// <summary>
        /// The main socket used for all data transfer
        /// </summary>
        Socket serverSocket;
        /// <summary>
        /// Whether or not this game is online
        /// </summary>
        bool Online = false;
        /// <summary>
        /// The buffer containing recieved data from another computer in the form of bytes
        /// </summary>
        byte[] Buffer = new byte[10000];
        /// <summary>
        /// True if the local player is hosting or not
        /// </summary>
        bool Hosting;
        /// <summary>
        /// True if the game has started
        /// </summary>
        bool GameStarted = false;
        /// <summary>
        /// True if the game is a LAN game
        /// </summary>
        bool LAN;

        /// <summary>
        /// Represents the current game
        /// </summary>
        MtgGame Game;
        /// <summary>
        /// Represents the local player
        /// </summary>
        MtgPlayer Me;
        /// <summary>
        /// Represents the opposing player
        /// </summary>
        MtgPlayer Them;

        /// <summary>
        /// The enemy card that the local player is choosing to block
        /// </summary>
        MtgCard CurrentlyBlocking;

        /// <summary>
        /// The floating info box instance
        /// </summary>
        InfoBox Info;

        /// <summary>
        /// The type of the AI used by the first player
        /// </summary>
        Type P1AIType;
        /// <summary>
        /// The type of the AI used by the second player
        /// </summary>
        Type P2AIType;

        /// <summary>
        /// The first player's name
        /// </summary>
        string P1Name;
        /// <summary>
        /// The second player's name
        /// </summary>
        string P2Name;

        /// <summary>
        /// The AI instance used by the first player
        /// </summary>
        MtgPlayerAI P1AI;
        /// <summary>
        /// The AI instance used by the second player
        /// </summary>
        MtgPlayerAI P2AI;

        /// <summary>
        /// A dictionary of all the cards that the game has downloaded in the past
        /// </summary>
        Dictionary<string, MtgCardData> Saved;
        /// <summary>
        /// The list of all the individual cards used in the first player's deck. Multiple instances
        /// of the card may be present
        /// </summary>
        List<MtgCardData> PlayerOneDeck;
        /// <summary>
        /// The list of all the individual cards used in the second player's deck. Multiple instances
        /// of the card may be present
        /// </summary>
        List<MtgCardData> PlayerTwoDeck;

        /// <summary>
        /// The current list of targets to be filled by the player
        /// </summary>
        List<MtgTarget> Selecting;
        /// <summary>
        /// The current card whose abilities are still being targetted
        /// </summary>
        MtgCard AlmostPlayed;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of the main window
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Events

        /// <summary>
        /// Fires after any change to any properties that are being watched 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties and their Fields

        public Type[] AITypes
        {
            get
            {
                return GetAI();
            }
        }

        private string _otherstring;
        /// <summary>
        /// Gets or sets the current contents of the info box's text box
        /// </summary>
        public string OtherString
        {
            get
            {
                return _otherstring;
            }
            set
            {
                _otherstring = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("OtherString"));
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns an array of available AI types in the namespace
        /// </summary>
        /// <returns></returns>
        public Type[] GetAI()
        {
            var type = typeof(MtgPlayerAI);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p));
            return types.ToArray();
        }

        private void OnReceive(IAsyncResult ar)
        {
            OnlineData[] DataPackets = OnlineData.MultiFromByte(Buffer);
            foreach (OnlineData Data in DataPackets)
            {
                if (Data is MoveData)
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        ((MoveData)Data).Sync(Game);
                    });
                }
            }

            serverSocket.EndReceive(ar);
            serverSocket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, new AsyncCallback(OnReceive), null);
        }

        void StartGame()
        {
            //Set bindings and add zoom handler
            Binding Hand = new Binding("GetHand");
            list_hand.DataContext = Me;
            list_hand.SetBinding(ListView.ItemsSourceProperty, Hand);

            Binding Creatures = new Binding("GetInPlayCreatures");
            bf_self_creatures.DataContext = Me;
            bf_op_creatures.DataContext = Them;
            bf_self_creatures.inner_list.SetBinding(ListView.ItemsSourceProperty, Creatures);
            bf_op_creatures.inner_list.SetBinding(ListView.ItemsSourceProperty, Creatures);

            Binding Lands = new Binding("GetInPlayLands");
            bf_self_land.DataContext = Me;
            bf_op_land.DataContext = Them;
            bf_self_land.inner_list.SetBinding(ListView.ItemsSourceProperty, Lands);
            bf_op_land.inner_list.SetBinding(ListView.ItemsSourceProperty, Lands);

            Binding Other = new Binding("GetInPlayOther");
            bf_self_other.DataContext = Me;
            bf_op_other.DataContext = Them;

            bf_self_other.inner_list.SetBinding(ListView.ItemsSourceProperty, Other);
            bf_op_other.inner_list.SetBinding(ListView.ItemsSourceProperty, Other);

            //Add zoom handlers
            bf_self_creatures.Zoomed += new ZoomedEventHandler(bf_zoomed);
            bf_op_creatures.Zoomed += new ZoomedEventHandler(bf_zoomed);
            bf_self_land.Zoomed += new ZoomedEventHandler(bf_zoomed);
            bf_op_land.Zoomed += new ZoomedEventHandler(bf_zoomed);
            bf_self_other.Zoomed += new ZoomedEventHandler(bf_zoomed);
            bf_op_other.Zoomed += new ZoomedEventHandler(bf_zoomed);

            //Start game
            Game.GameWon += new EventHandler(Game_GameWon);
            Them.Shuffled += new EventHandler(Them_Shuffled);
            Game.StartGame();

            MiguelUtil.UtilMethods.DebugWrite("Error.txt", "Game started");

            //Show info box
            try
            {
                Info = new InfoBox(Game, this, Me);
                Info.Show();
                Info.NewTurn += new EventHandler(Info_NewTurn);
                Info.EndTurn += new EventHandler(Info_EndTurn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            GameStarted = true;

            MiguelUtil.UtilMethods.DebugWrite("Error.txt", "Finished Setup");
        }

        void Info_EndTurn(object sender, EventArgs e)
        {
            if (Game.Phase != MtgPhase.Combat)
            {

                //Put AlmostPlayed into play
                Selecting.Clear();
                if (AlmostPlayed != null)
                {
                    PlayCard(AlmostPlayed);
                    AlmostPlayed = null;
                }
                OtherString = "";

                //End phase and send that
                MoveData Data = new MoveData();
                Data.Type = MoveType.EndPhase;
                if (Online)
                    SocketSend(Data);
                Game.EndTurn();

                //Update AI
                if (Me.IsAI)
                    P1AI.PhaseChanged();
                if (Them.IsAI)
                    P2AI.PhaseChanged();
            }
        }

        void bf_zoomed(object sender, ZoomedEventArgs e)
        {
            popup_zoom.IsOpen = e.ZoomIn;
            image_popup.Source = e.ZoomIn ? e.Source : null;
        }

        void SocketSend(OnlineData Data)
        {
            byte[] Send = Data.ToByte();
            int Result = 0;
            while (Result < Send.Length)
            {
                Result = serverSocket.Send(Send);
                if (Result < Send.Length)
                    Thread.Sleep(100);
            }
            //do
            //{
            //    Result = serverSocket.Send(Send);
            //}
            //while (serverSocket.Connected == false);
        }

        /// <summary>
        /// Only for when the card is DEFINITELY going into play NOW
        /// </summary>
        /// <param name="card"></param>
        void PlayCard(MtgCard card)
        {
            //card.UpdateIndex();
            //Work out index etc. BEFORE it's played
            MoveData Data = new MoveData();
            Data.Type = MoveType.PlayCard;
            Data.Data = card;

            //Send this to other player
            if (Online)
                SocketSend(Data);

            //Play the card
            Game.ActivePlayer.PlayCard(card);
            //  card.UpdateIndex();
        }

        #endregion

        #region Event Handlers

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Load the card dictionary
            Saved = new Dictionary<string, MtgCardData>();
            string saved_path = Path.Combine(Settings.Default.CardCacheLocation, "Cards.dat");
            if (File.Exists(saved_path))
            {
                BinaryFormatter Formatter = new BinaryFormatter();
                Saved = (Dictionary<string, MtgCardData>)Formatter.Deserialize(File.Open(saved_path, FileMode.Open));
            }

            //The deck lists
            PlayerOneDeck = new List<MtgCardData>();
            PlayerTwoDeck = new List<MtgCardData>();
            DeckElement[] PlayerOne = new DeckElement[0];
            DeckElement[] PlayerTwo = new DeckElement[0];

            //Initiate the target list
            Selecting = new List<MtgTarget>();

            //Set the data context
            DataContext = this;

            //Get hosting/joining information from the player
        Initial: InitialWindow Initial = new InitialWindow();
            if (Initial.ShowDialog().Value == false)
            {
                Application.Current.Shutdown();
                return;
            }

            //Set online information
            LAN = Initial.LAN;
            Hosting = Initial.Host;
            IPAddress TheirAddress = null;
            if (Hosting == false)
            {
                bool CorrectIP = IPAddress.TryParse(Initial.IPAddress, out TheirAddress);
                if (CorrectIP == false)
                    goto Initial;
            }

            //Create the welcome menu
            WelcomeMenu Welcome = new WelcomeMenu(Hosting, LAN, TheirAddress);

            //AI setup for the welcome menu
            Type[] types = GetAI();
            Binding typebinding = new Binding("AITypes");
            Welcome.DataContext = this;
            Welcome.combo_p1ai.SetBinding(ComboBox.ItemsSourceProperty, typebinding);
            Welcome.combo_p2ai.SetBinding(ComboBox.ItemsSourceProperty, typebinding);

            try
            {
                if (Welcome.ShowDialog().Value == false)
                {
                    Application.Current.Shutdown();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            P1AIType = Welcome.radio_p1_ai.IsChecked.Value ? (Type)Welcome.combo_p1ai.SelectedItem : null;
            P2AIType = Welcome.radio_p2_ai.IsChecked.Value ? (Type)Welcome.combo_p2ai.SelectedItem : null;

            PlayerOne = Welcome.Player_One.ToArray();
            PlayerTwo = Welcome.Player_Two.ToArray();

            foreach (DeckElement element in PlayerOne)
            {
                if (Saved.ContainsKey(element.CardName) == false)
                {
                    MtgCardData Data = JsonMtg.CardFromInput(element.CardName);
                    Saved.Add(Data.name, Data);
                }

                for (int i = 0; i < element.Number; i++)
                {
                    PlayerOneDeck.Add(Saved[element.CardName]);
                }
            }

            foreach (DeckElement element in PlayerTwo)
            {
                if (Saved.ContainsKey(element.CardName) == false)
                {
                    MtgCardData Data = JsonMtg.CardFromInput(element.CardName);
                    Saved.Add(Data.name, Data);
                }

                for (int i = 0; i < element.Number; i++)
                {
                    PlayerTwoDeck.Add(Saved[element.CardName]);
                }
            }

            P1Name = Welcome.radio_p1_real.IsChecked.Value ? Welcome.Text_P1Name.Text : (string)P1AIType.GetProperty("AIName").GetValue(null, null);
            P2Name = Welcome.radio_p2_real.IsChecked.Value ? Welcome.Text_P2Name.Text : (string)P2AIType.GetProperty("AIName").GetValue(null, null);

            if (Hosting)
            {
                Me = new MtgPlayer(P1Name, PlayerOneDeck.ToArray(), P1AIType != null, 0);
                Them = new MtgPlayer(P2Name, PlayerTwoDeck.ToArray(), P2AIType != null, 1);
                Game = new MtgGame(Me, Them);
            }
            else
            {
                Me = new MtgPlayer(P2Name, PlayerTwoDeck.ToArray(), P2AIType != null, 1);
                Them = new MtgPlayer(P1Name, PlayerOneDeck.ToArray(), P1AIType != null, 0);
                Game = new MtgGame(Them, Me);
            }

            Game.PhaseEnded += new EventHandler(Game_PhaseEnded);

            //AI stuff
            if (P1AIType != null)
                P1AI = (MtgPlayerAI)Activator.CreateInstance(P1AIType, Me, Game);
            if (P2AIType != null)
                P2AI = (MtgPlayerAI)Activator.CreateInstance(P2AIType, Them, Game);
            if (Me.IsAI == false && Them.IsAI == false)
            {
                Online = true;

            }

            if (Online)
            {
                serverSocket = Welcome.MainSocket;
                serverSocket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, new AsyncCallback(OnReceive), null);
            }

            StartGame();
        }

        void Game_PhaseEnded(object sender, EventArgs e)
        {
            Selecting.Clear();
            if (AlmostPlayed != null)
            {
                //Work out index etc. BEFORE it's played
                MoveData Data = new MoveData();
                Data.Type = MoveType.PlayCard;
                Data.Data = AlmostPlayed;

                AlmostPlayed.PutIntoPlay();
                AlmostPlayed = null;

                if (Online)
                    SocketSend(Data);
            }
            OtherString = "";

            //Update AI
            if (Me.IsAI)
                P1AI.PhaseChanged();
            if (Them.IsAI)
                P2AI.PhaseChanged();
        }

        void Them_Shuffled(object sender, EventArgs e)
        {
            //MoveData Data = new MoveData();
            //Data.Type = MoveType.Shuffle;
            //SocketSend(Data);
        }

        void Game_GameWon(object sender, EventArgs e)
        {
            if (Game.Players[0].LifeTotal <= 0)
            {
                MessageBox.Show("Player two, " + Game.Players[1].Name + ", has won the game!");
            }
            if (Game.Players[1].LifeTotal <= 0)
            {
                MessageBox.Show("Player one, " + Game.Players[0].Name + ", has won the game!");
            }

            Info.Close();
            Window_Loaded(null, null);
        }

        void Info_NewTurn(object sender, EventArgs e)
        {
            //Put AlmostPlayed into play
            Selecting.Clear();
            if (AlmostPlayed != null)
            {
                PlayCard(AlmostPlayed);
                AlmostPlayed = null;
            }
            OtherString = "";

            //End phase and send that
            MoveData Data = new MoveData();
            Data.Type = MoveType.EndPhase;
            if (Online)
                SocketSend(Data);
            Game.EndPhase();

            //Update AI
            if (Me.IsAI)
                P1AI.PhaseChanged();
            if (Them.IsAI)
                P2AI.PhaseChanged();
        }

        //Click a card in the hand
        private void list_hand_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MtgCard Current = (MtgCard)list_hand.SelectedItem;

            if (Me.CanPlay(Current))
            {
                //If there are abilities that need targets
                if (Current.Abilities.Count > 0 && Selecting.Count == 0)
                {
                    foreach (MtgAbility ability in Current.Abilities)
                    {
                        foreach (MtgTarget target in ability.targets)
                        {
                            if (target.Target == null)
                            {
                                Selecting.Add(target);
                            }
                        }
                    }
                }

                if (Selecting.Count > 0)
                {
                    OtherString = Selecting[0].Description;
                    AlmostPlayed = Current;
                    return;
                }
                else
                {
                    try
                    {
                        PlayCard(Current);

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            else
            {
                MessageBox.Show("You can't play that for some reason (your turn, mana?)");
            }
        }

        private void list_self_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //Click own creature
            MtgCard Current = (MtgCard)bf_self_creatures.inner_list.SelectedItem;
            //Current.UpdateIndex();
            try
            {
                MoveData AttackData = new MoveData();
                AttackData.Data = Current;

                if (Game.Phase == MtgPhase.Combat)
                {
                    if (Me.Attacking.Contains(Current))
                    {
                        AttackData.Type = MoveType.Withdraw;
                        Me.WithdrawFromCombat(Current);
                    }
                    else
                    {
                        AttackData.Type = MoveType.Attack;
                        Me.AttackWith(Current);
                    }
                }
                else if (Game.Phase == MtgPhase.Defending)
                {
                    //  CurrentlyBlocking.UpdateIndex();
                    AttackData.Type = MoveType.Defend;
                    Current.Block(CurrentlyBlocking);
                }

                if (Online)
                    SocketSend(AttackData);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            OpenFileDialog Open = new OpenFileDialog();
            Open.ShowDialog();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            SettingsDialog Set = new SettingsDialog(Settings.Default);
            if (Set.ShowDialog().Value)
            {
                Settings.Default.Save();
            }
            else
            {
                Settings.Default.Reload();
            }
        }

        private void Ability_clicked(object sender, RoutedEventArgs e)
        {
            MenuItem Menu = (MenuItem)sender;
            MtgAbility ability = (MtgAbility)Menu.DataContext;
            ability.Activate();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            string saved_path = Path.Combine(Settings.Default.CardCacheLocation, "Cards.dat");
            FileStream steam = File.Create(saved_path);
            BinaryFormatter ser = new BinaryFormatter();
            ser.Serialize(steam, Saved);
            steam.Close();
            Info.Close();
        }

        //Opponent card click
        private void bf_opp_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Battlefield area = (Battlefield)sender;
            MtgCard opponentcard = (MtgCard)area.inner_list.SelectedItem;

            //If defending
            if (Game.Phase == MtgPhase.Defending && Me.IsTurn)
            {
                if (CurrentlyBlocking == opponentcard)
                {
                    CurrentlyBlocking = null;
                    OtherString = "";
                }
                else
                {
                    CurrentlyBlocking = opponentcard;
                    OtherString = "Currently Blocking: " + CurrentlyBlocking.name + ".";
                }
            }
            else if (Selecting.Count > 0)
            {
                foreach (MtgTargetFilter filter in Selecting[0].Filters)
                {
                    //If valid target selected
                    if ((filter.Negated == false && opponentcard.type.ContainsAnyCase(filter.Type)) || filter.Negated && opponentcard.type.ContainsAnyCase(filter.Type) == false)
                    {

                    }
                    else
                    {
                        return;
                    }

                }

                Selecting[0].Target = opponentcard;
                Selecting.RemoveAt(0);
                if (Selecting.Count == 0)
                {
                    PlayCard(AlmostPlayed);
                    OtherString = "";
                    AlmostPlayed = null;

                }
                else
                    OtherString = Selecting[0].Description;
            }
        }

        private void ContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.ContextMenu Menu = (System.Windows.Controls.ContextMenu)sender;
            Battlefield Parent = (Battlefield)Menu.PlacementTarget;
            MtgCard Target = (MtgCard)Parent.inner_list.SelectedItem;

            if (Target != null)
            {
                MenuItem sub_menu = (MenuItem)Menu.Items[0];
                sub_menu.ItemsSource = Target.ActivatedAbilities;
            }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            Hax window = new Hax();
            if (window.ShowDialog().Value)
            {
                string hack = window.Result;
                string[] split = hack.Split(' ');
                if (split[0] == "add")
                {
                    Me.InPlay.Add(new MtgCard(JsonMtg.CardFromInput(hack.Substring(split[0].Length + 1))));
                }
            }
        }

        #endregion

        private void Image_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            popup_zoom.IsOpen = e.Delta > 0;
            image_popup.Source = popup_zoom.IsOpen ? ((Image)sender).Source : null;
            e.Handled = true;
        }

        private void popup_zoom_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            popup_zoom.IsOpen = e.Delta > 0;
            if (popup_zoom.IsOpen == false)
                image_popup.Source = null;
        }
    }

    #region WPF value converters

    public class TappedToRotateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            bool tapped = (bool)value;
            return tapped ? 90 : 0;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            int Angle = (int)value;
            return Angle == 90;
        }
    }

    public class SSBlurConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            bool ss = (bool)value;
            return ss ? 20 : 0;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            int blur = (int)value;
            return blur == 20;
        }
    }

    public class CardStatetoColourConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            MtgCardState tapped = (MtgCardState)value;
            Color Start = Colors.Black;
            Color End = Colors.Black;

            switch (tapped)
            {
                case MtgCardState.Attacking:
                    return Colors.Orange;

                case MtgCardState.Defending:
                    return Colors.Blue;
            }

            return Colors.Transparent;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            Color Cl = (Color)value;

            if (Cl == Colors.Orange)
                return MtgCardState.Attacking;

            if (Cl == Colors.Blue)
                return MtgCardState.Defending;

            return MtgCardState.None;
        }
    }

    public class CardStatetoBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            MtgCardState tapped = (MtgCardState)value;
            Color Start = Colors.Black;
            Color End = Colors.Black;

            switch (tapped)
            {
                case MtgCardState.Attacking:
                    Start = Colors.Yellow;
                    End = Colors.Orange;
                    break;

                case MtgCardState.Defending:
                    Start = Colors.Blue;
                    End = Colors.Navy;
                    break;

                case MtgCardState.None:
                    Start = Colors.Transparent;
                    End = Colors.Transparent;
                    break;
            }

            RadialGradientBrush brush = new RadialGradientBrush();
            brush.GradientStops.Add(new GradientStop(Start, 0));
            brush.GradientStops.Add(new GradientStop(End, 2));
            return brush;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            RadialGradientBrush Brush = (RadialGradientBrush)value;

            if (Brush.GradientStops[0].Color == Colors.Transparent)
                return MtgCardState.None;

            if (Brush.GradientStops[0].Color == Colors.Orange)
                return MtgCardState.Attacking;

            if (Brush.GradientStops[0].Color == Colors.Blue)
                return MtgCardState.Defending;

            return MtgCardState.None;
        }
    }

    class HWRatioConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((double)value) * 0.72;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((double)value) / 0.72;
        }
    }

    #endregion
}
