﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using MtgLib;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace MtgApp
{
    [Serializable]
    public struct DeckElement
    {
        public DeckElement(string name, int number)
        {
            CardName = name;
            Number = number;
        }

        public override string ToString()
        {
            return CardName;
        }

        public string CardName;

        public int Number;
    }

    public static class JsonMtg
    {
        public static DeckElement[] GetElements(string deck)
        {
            List<DeckElement> cards = new List<DeckElement>();

            foreach (string match in Regex.Split(deck, @"\r\n"))
            {
                int number = 1;
                string name = "";

                foreach (string token in Regex.Split(match, @"([xX])|(\d+)|(.+)"))
                {
                    if (token != "")
                    {
                        if (char.IsDigit(token[0]))
                            number = int.Parse(token);
                        // else if (char.ToLower(token[0]) == 'x')
                        else
                        {
                            name = token.Trim();
                        }
                    }
                }
                cards.Add(new DeckElement(name, number));
            }
            return cards.ToArray();
        }

        public static MtgCardData CardFromJson(JToken token)
        {
            MtgCardData card = new MtgCardData();

            card.artist = (string)token["artist"];
            card.card_image = (string)token["card_image"];
            card.card_set_id = (string)token["card_set_id"];
            card.colors = MtgColour.Colourless;
            foreach (string inner in (JArray)token["colors"])
            {
                switch (inner)
                {
                    case "blue":
                        card.colors |= MtgColour.Blue;
                        break;

                    case "green":
                        card.colors |= MtgColour.Green;
                        break;

                    case "black":
                        card.colors |= MtgColour.Black;
                        break;

                    case "white":
                        card.colors |= MtgColour.White;
                        break;

                    case "red":
                        card.colors |= MtgColour.Red;
                        break;
                }
            }
            card.convertedmanacost = (int)token["convertedmanacost"];
            card.description = (string)token["description"];
            card.flavor = (string)token["flavor"];
            card.Id = int.Parse((string)token["Id"]);
            card.loyalty = (int)token["loyalty"];
            card.manacost = (string)token["manacost"];
            card.name = (string)token["name"];
            card.power = (int)token["power"];
            card.rarity = (string)token["rarity"];
            card.card_released_at = DateTime.Parse((string)token["released_at"], CultureInfo.GetCultureInfo("en-US"));
            card.card_set_name = (string)token["card_set_name"];
            card.set_number = (int)token["set_number"];
            card.subtype = (string)token["subtype"];
            card.toughness = (int)token["toughness"];
            card.type = (string)token["type"];

            return card;
        }

        public static MtgCardData CardFromInput(string input)
        {
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(@"http://api.mtgdb.info/cards/?name=" + input);
            HttpWebResponse resp = (HttpWebResponse)rq.GetResponse();
            using (Stream s = resp.GetResponseStream())
            {
                StreamReader reader = new StreamReader(s);

                string str;
                str = reader.ReadLine();
                JArray array = JArray.Parse(str);
                MtgCardData Card = JsonMtg.CardFromJson(array[array.Count - 1]);

                return Card;
            }
        }

        public static MtgCardData[] CardsFromInput(string input)
        {
            List<MtgCardData> cards = new List<MtgCardData>();

            foreach (string match in Regex.Split(input, @"\r\n"))
            {
                int number = 1;
                string name = "";

                foreach (string token in Regex.Split(match, @"([xX])|(\d+)|(.+)"))
                {
                    if (token != "")
                    {
                        if (char.IsDigit(token[0]))
                            number = int.Parse(token);
                        // else if (char.ToLower(token[0]) == 'x')
                        else
                        {
                            name = token.Trim();
                        }
                    }
                }

                cards.Add(CardFromInput(match));
            }

            return cards.ToArray();
        }
    }
}
