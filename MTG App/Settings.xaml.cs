﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MtgApp.Properties;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.IO;
using Microsoft.Win32;
using WPFFolderBrowser;

namespace MtgApp
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class SettingsDialog : Window
    {
        Settings SettingsData;

        internal SettingsDialog(Settings Set)
        {
            InitializeComponent();
            SettingsData = Set;
        }

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);

            DataContext = SettingsData;
            text_cache.SetBinding(TextBox.TextProperty, "CardCacheLocation");
        }

        private void Button_Save_Click(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(text_cache.Text))
                DialogResult = true;
            else
            {
                MessageBox.Show("This directory doesn't exist");
            }
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            WPFFolderBrowserDialog Browse = new WPFFolderBrowserDialog();
            if (Browse.ShowDialog().Value)
            {
                text_cache.Text = Browse.FileName;
            }
        }
    }
}
