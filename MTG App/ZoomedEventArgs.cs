﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace MtgApp
{
    public class ZoomedEventArgs : EventArgs
    {
        public ZoomedEventArgs(ImageSource source, bool zoom)
        {
            Source = source;
            ZoomIn = zoom;
        }

        public ImageSource Source;
        public bool ZoomIn;
    }
}
