﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MtgApp
{
    /// <summary>
    /// Interaction logic for Hax.xaml
    /// </summary>
    public partial class Hax : Window
    {
        public string Result;

        public Hax()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Result = textBox1.Text;
            DialogResult = true;
        }
    }
}
