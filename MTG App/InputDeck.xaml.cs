﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MtgLib;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MtgApp
{
    /// <summary>
    /// Interaction logic for InputDeck.xaml
    /// </summary>
    public partial class InputDeck : Window
    {
        public string cards;

        public InputDeck()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            cards = textBox1.Text;

            DialogResult = true;
        }
    }
}
