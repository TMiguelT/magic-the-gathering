﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MtgApp
{
    /// <summary>
    /// Interaction logic for InitialWindow.xaml
    /// </summary>
    public partial class InitialWindow : Window
    {
        public bool Host;
        public bool LAN;
        public string IPAddress;

        public InitialWindow()
        {
            InitializeComponent();
        }

        private void text_IP_GotFocus(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
                text_IP.Text = "";
        }

        private void text_IP_LostFocus(object sender, RoutedEventArgs e)
        {
            if (IsLoaded && text_IP.Text == "")
                text_IP.Text = "IP Address";
        }

        private void radio_join_Checked(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
                text_IP.IsEnabled = radio_join.IsChecked.Value;
        }

        private void radio_host_Checked(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
                text_IP.IsEnabled = radio_join.IsChecked.Value;
        }

        private void button_go_Click(object sender, RoutedEventArgs e)
        {
            IPAddress = text_IP.Text;
            Host = radio_host.IsChecked.Value;
            LAN = check_lan.IsChecked.Value;
            DialogResult = true;
        }
    }
}
