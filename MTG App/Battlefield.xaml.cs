﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MtgApp
{
    public delegate void ZoomedEventHandler(object sender, ZoomedEventArgs e);

    /// <summary>
    /// Interaction logic for Battlefield.xaml
    /// </summary>
    public partial class Battlefield : UserControl
    {
    
        public event ZoomedEventHandler Zoomed;

        public Battlefield()
        {
            InitializeComponent();
        }

        private void ContextMenu_Opened(object sender, RoutedEventArgs e)
        {

        }

        private void UserControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {

        }

        private void Image_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            Zoomed(sender,  new ZoomedEventArgs(((Image)sender).Source, e.Delta > 0));
            e.Handled = true;
        }
    }
}
