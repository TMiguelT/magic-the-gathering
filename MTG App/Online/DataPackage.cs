﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using MtgLib;
using System.Collections;
using System.Net;

namespace MtgApp.Online
{
    public class CardLocation
    {
        public Zone CardZone;
        public int Index;

        public CardLocation(Zone zone, int index)
        {
            CardZone = zone;
            Index = index;
        }
    }

    public enum Zone
    {
        Graveyard,
        Hand,
        InPlay,
        Exile
    }

    public enum SpellType
    {
        Instant,
        Enchantment,
        Sorcery,
        Creature,
        Artifact
    }

    public enum MoveType
    {
        PlayCard,
        ActivateAbility,
        EndPhase,
        Shuffle,
        Attack,
        Defend,
        Withdraw
    }

    [Serializable]
    public class MoveData : OnlineData
    {
        public MoveType Type;
        public object Data;

        public MoveData()
        {
        }

        public MoveData(byte[] input)
        {
            BinaryFormatter Formatter = new BinaryFormatter();
            MemoryStream Stream = new MemoryStream(input);
            MoveData result = (MoveData)Formatter.Deserialize(Stream);
            Type = result.Type;
            Data = result.Data;
        }

        /// <summary>
        /// Syncs the gamedata with the given game file, sent from the given player number
        /// </summary>
        /// <param name="game">The game to sync with</param>
        /// <param name="PlayerNumber">The player number that sent this message</param>
        public void Sync(MtgGame game)
        {
            MtgCard RemoteCard = null;
            MtgCard LocalCard = null;
            MtgPlayer CardOwner = null;
            if (Data is MtgCard)
            {
                RemoteCard = (MtgCard)Data;
                CardOwner = game.Players[RemoteCard.OwnerIndex];
                LocalCard = game.Localise(RemoteCard, CardOwner, true);
            }

            switch (Type)
            {
                case MoveType.PlayCard:
                    CardOwner.PlayCard(LocalCard);
                    break;
                case MoveType.ActivateAbility:
                    break;
                case MoveType.EndPhase:
                    game.EndPhase();
                    break;
                case MoveType.Shuffle:
                    break;
                case MoveType.Attack:
                    CardOwner.AttackWith(LocalCard);
                    break;
                case MoveType.Defend:
                    MtgCard BlockingAgainst = game.Localise(RemoteCard.BlockingAgainst, CardOwner.Opponent, true);
                    LocalCard.Block(BlockingAgainst);
                    break;
                case MoveType.Withdraw:
                    CardOwner.WithdrawFromCombat(LocalCard);
                    break;
                default:
                    break;
            }
        }

    }

    [Serializable]
    public class IPData : OnlineData
    {
        public IPAddress Address;

        public static IPData FromByte(byte[] data)
        {
            BinaryFormatter Formatter = new BinaryFormatter();
            MemoryStream Stream = new MemoryStream(data);
            return (IPData)Formatter.Deserialize(Stream);
        }
    }

    [Serializable]
    public class PingData : OnlineData
    {
        public DateTime Time;

        public static PingData FromByte(byte[] data)
        {
            BinaryFormatter Formatter = new BinaryFormatter();
            MemoryStream Stream = new MemoryStream(data);
            return (PingData)Formatter.Deserialize(Stream);
        }
    }

    [Serializable]
    public class PlayerData : OnlineData
    {
        public List<DeckElement> Deck;
        public string Name;

        public PlayerData()
        {
            Deck = new List<DeckElement>();
        }

        public PlayerData(DeckElement[] deck, string name)
            : this()
        {
            Deck.AddRange(deck);
            Name = name;
        }

        public static PlayerData FromByte(byte[] input)
        {
            BinaryFormatter Formatter = new BinaryFormatter();
            MemoryStream Stream = new MemoryStream(input);
            PlayerData result = (PlayerData)Formatter.Deserialize(Stream);
            return result;
        }
    }

    [Serializable]
    public class StartGame : OnlineData
    {
        public static StartGame FromByte(byte[] data)
        {
            BinaryFormatter Formatter = new BinaryFormatter();
            MemoryStream Stream = new MemoryStream(data);
            return (StartGame)Formatter.Deserialize(Stream);
        }
    }

    [Serializable]
    public class EndRecieve : OnlineData
    {
        public static EndRecieve FromByte(byte[] data)
        {
            BinaryFormatter Formatter = new BinaryFormatter();
            MemoryStream Stream = new MemoryStream(data);
            return (EndRecieve)Formatter.Deserialize(Stream);
        }
    }


    [Serializable]
    public class OnlineData
    {
        public virtual byte[] ToByte()
        {
            BinaryFormatter Formatter = new BinaryFormatter();
            MemoryStream Stream = new MemoryStream();
            Formatter.Serialize(Stream, this);
            return Stream.ToArray();
        }

        public static OnlineData FromByte(byte[] input)
        {
            BinaryFormatter Formatter = new BinaryFormatter();
            MemoryStream Stream = new MemoryStream(input);
            OnlineData result = (OnlineData)Formatter.Deserialize(Stream);
            return result;
        }

        public static OnlineData[] MultiFromByte(byte[] input)
        {
            List<OnlineData> Return = new List<OnlineData>();
            BinaryFormatter Formatter = new BinaryFormatter();
            MemoryStream Stream = new MemoryStream(input);

            while (true)
            {
                try
                {
                    OnlineData result = (OnlineData)Formatter.Deserialize(Stream);
                    Return.Add(result);
                }
                catch
                {
                    break;
                }
            }
            return Return.ToArray(); ;
        }
    }
}
