﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interop;
using MtgLib;

namespace MtgApp
{
    /// <summary>
    /// Interaction logic for InfoBox.xaml
    /// </summary>
    public partial class InfoBox : Window
    {
        public event EventHandler NewTurn;
        public event EventHandler EndTurn;

        MtgGame info;
        MainWindow ParentWindow;
        MtgPlayer LocalPlayer;

        public InfoBox(MtgGame gameinfo, MainWindow parent, MtgPlayer localplayer)
        {
            InitializeComponent();
            info = gameinfo;
            DataContext = info;
            LocalPlayer = localplayer;

            Binding name = new Binding("ActivePlayer.Name");
            name.Mode = BindingMode.OneWay;
            text_activeplayer.SetBinding(TextBox.TextProperty, name);

            Binding phase = new Binding("Phase");
            phase.Mode = BindingMode.OneWay;
            text_phase.SetBinding(TextBox.TextProperty, phase);

            Binding mylife = new Binding("Players[0].LifeTotal");
            mylife.Mode = BindingMode.OneWay;
            text_yourlife.SetBinding(TextBox.TextProperty, mylife);

            Binding theirlife = new Binding("Players[1].LifeTotal");
            theirlife.Mode = BindingMode.OneWay;
            text_theirlife.SetBinding(TextBox.TextProperty, theirlife);

            ParentWindow = parent;
            text_other.DataContext = ParentWindow;
            Binding other = new Binding("OtherString");
            other.Mode = BindingMode.OneWay;
            text_other.SetBinding(TextBox.TextProperty, other);

            info.PhaseEnded += new EventHandler(info_PhaseEnded);

            if (info.ActivePlayer != LocalPlayer)
                button1.IsEnabled = false;
        }

        void info_PhaseEnded(object sender, EventArgs e)
        {
            button1.IsEnabled = info.ActivePlayer == LocalPlayer;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            NewTurn(this, new EventArgs());
            if (info.ActivePlayer != LocalPlayer)
                button1.IsEnabled = false;
        }

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            EndTurn(this, new EventArgs());
            if (info.ActivePlayer != LocalPlayer)
                button1.IsEnabled = false;
        }
    }
}
