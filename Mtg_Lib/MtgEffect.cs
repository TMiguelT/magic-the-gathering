﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MtgLib
{
    [Serializable]
    public enum MtgTrigger
    {
        EntersPlay,
        DealsPlayerDamage,
        DealsCombatDamage,
        TakesCombatDamage,
        Death,
        None
    }

    /// <summary>
    /// If the ability is triggered or activated to be used
    /// </summary>
    [Serializable]
    public enum MtgAbilityActivation
    {
        Triggered,
        Mana,
        Activated
    }

    [Serializable]
    public class MtgAbilityCost
    {
        public MtgAbilityCost()
        {
            Tap = false;
            ManaCost = null;
        }

        public bool Tap;
        public MtgMana[] ManaCost;
    }

    /// <summary>
    /// The result of the ability on the target
    /// </summary>
    [Serializable]
    public enum MtgActionType
    {
        None,
        DrawCard,
        Tap,
        Detain,
        ReturnToHand,
        Discard,
        Untap,
        ChangeStats,
        Counter,
        Destroy,
        GainLife,
        LoseLife,
        Mana
    }

    /// <summary>
    /// How the target is chosen
    /// </summary>
    [Serializable]
    public enum MtgTargetSelection
    {
        Target,
        Self,
        All
    }

    /// <summary>
    /// Filters that restrict the type of target
    /// </summary>
    [Serializable]
    public struct MtgTargetFilter
    {
        public string Type;
        public bool Negated;
    }

    [Serializable]
    public class MtgTarget
    {
        public MtgTargetSelection Selection;
        public MtgTargetFilter[] Filters;
        public MtgCard Target;
        public MtgAbility Parent;
        public string Description;

        public bool IsFilled
        {
            get
            {
                return Target != null;
            }
        }
    }

    [Serializable]
    public class MtgAbility
    {
        public MtgAbilityActivation activation;
        public MtgTrigger trigger;
        public MtgTarget[] targets;
        public MtgTarget trigger_target;
        public MtgActionType action;
        public MtgAbilityCost cost;
        public string description;
        public object param_1;
        public object param_2;
        public MtgCard parent;

        public MtgAbility(MtgCard caster)
        {
            parent = caster;
        }

        public override string ToString()
        {
            return description;
        }

        public void Activate()
        {
            if (cost.Tap)
            {
                parent.Tap();
            }
            if (cost.ManaCost != null && cost.ManaCost.Length > 0)
            {
                //Do cost stuff
            }
            foreach (MtgTarget target in targets)
            {
                if (target.Target == null)
                    continue;
                MtgCard card = target.Target;
                switch (action)
                {
                    case MtgActionType.DrawCard:
                        for (int i = 0; i < (int)param_1; i++)
                        {
                            parent.Owner.DrawCard();
                        }
                        break;
                    case MtgActionType.None:
                        break;
                    case MtgActionType.Tap:
                        card.Tap();
                        break;
                    case MtgActionType.ReturnToHand:
                        card.Owner.InPlay.Remove(card);
                        card.Owner.Hand.Add(card);
                        break;
                    case MtgActionType.Discard:
                        card.Owner.Hand.Remove(card);
                        break;
                    case MtgActionType.Untap:
                        card.UnTap();
                        break;
                    case MtgActionType.ChangeStats:
                        card.power += (int)param_1;
                        card.toughness += (int)param_2;
                        break;
                    case MtgActionType.Counter:

                        break;
                    case MtgActionType.Destroy:

                        break;
                    case MtgActionType.GainLife:
                        break;
                    case MtgActionType.LoseLife:
                        break;
                    case MtgActionType.Mana:
                        break;
                    case MtgActionType.Detain:
                        card.Tap();
                        card.UntapsTurn = 2;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
