﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Text.RegularExpressions;
using StringExtensions;
using System.Runtime.Serialization;

namespace MtgLib
{
    public enum MtgZone
    {
        Hand,
        Library,
        Play,
        Graveyard,
        Exiled,
        Mulligan
    }

    public enum MtgCardState
    {
        None,
        Attacking,
        Defending
    }

    //[Serializable]
    //public struct CardRef
    //{
    //    public MtgZone Zone;
    //    public int Index;
    //    public int CardId;
    //}

    [Serializable]
    public class MtgCard : INotifyPropertyChanged
    {
        #region events

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region constructors

        public MtgCard()
        {
            Owner = null;
            blockers = new List<MtgCard>();
            SummoningSickness = true;
        }

        public MtgCard(MtgCardData data)
            : this()
        {
            artist = data.artist;
            card_image = data.card_image;
            card_set_id = data.card_set_id;
            colors = data.colors;
            convertedmanacost = data.convertedmanacost;
            description = data.description;
            flavor = data.flavor;
            id = data.Id;
            loyalty = data.loyalty;
            manacost = MtgMana.ParseCosts(data.manacost);
            name = data.name;
            power = data.power;
            rarity = data.rarity;
            released_at = data.card_released_at;
            set = data.card_set_name;
            set_number = data.set_number;
            subtype = data.subtype;
            toughness = data.toughness;
            type = data.type;

            if (IsLand)
                SummoningSickness = false;

            //FIX THESE
            Abilities = new List<MtgAbility>();
            SetEffects();
        }

        #endregion

        #region individual fields

        public List<MtgAbility> Abilities
        {
            get;
            set;
        }

        #endregion

        #region public methods

        [OnSerializing]
        private void OnSerializing(StreamingContext context)
        {
            UpdateIndex();
        }

        public void UpdateIndex()
        {
            _index = Owner.GetIndex(this);
            _ownerIndex = Owner.Index;
        }

        public void PutIntoPlay()
        {
            Owner.Hand.Remove(this);
            List<MtgMana> CurrentMana = new List<MtgMana>(Owner.GetMana());

            //Pay coloured mana cost
            foreach (MtgMana mana in GetColouredManaCost)
            {
                if (CurrentMana.Contains(mana))
                {
                    int first = CurrentMana.IndexOf(mana);
                    CurrentMana[first].Use();
                    CurrentMana.RemoveAt(first);
                }
                else
                    throw new Exception("Dun have mana");
            }

            //Pay colourless mana cost
            foreach (MtgMana mana in GetColourlessManaCost)
            {
                if (CurrentMana.Count > 0)
                {
                    int first = 0;
                    CurrentMana[first].Use();
                    CurrentMana.RemoveAt(first);
                }
                else
                    throw new Exception("Dun have mana");
            }

            if (IsLand)
                Owner.HasPlayedLand = true;

            //Put into play and activate triggers if permanent
            if (IsPermanent)
            {
                ChangeZone(MtgZone.Play);
            }
            //Activate spell effects if not permanent
            else
            {
                foreach (MtgAbility ability in Abilities)
                {
                    ability.Activate();
                }
            }
        }

        public MtgAbility[] ActivatedAbilities
        {
            get
            {
                var result = from MtgAbility ability in Abilities where ability.activation != MtgAbilityActivation.Triggered select ability;
                return result.ToArray();
            }
        }

        public void Block(MtgCard ToBlock)
        {
            if (ToBlock.State == MtgCardState.Attacking)
            {
                State = MtgCardState.Defending;
                BlockingAgainst = ToBlock;
                ToBlock.blockers.Add(this);
            }
            else
            {
                throw new Exception("He's not even attacking???");
            }
        }

        #endregion

        #region private methods

        /// <summary>
        /// Only copies attributes
        /// </summary>
        /// <returns></returns>
        internal MtgCard Copy()
        {
            MtgCard ret = new MtgCard();
            ret.artist = artist;
            ret.card_image = card_image;
            ret.card_set_id = card_set_id;
            ret.colors = colors;
            ret.convertedmanacost = convertedmanacost;
            ret.description = description;
            ret.flavor = flavor;
            ret.id = id;
            ret.loyalty = loyalty;
            ret.manacost = manacost;
            ret.name = name;
            ret.power = power;
            ret.rarity = rarity;
            ret.released_at = released_at;
            ret.set = set;
            ret.set_number = set_number;
            ret.subtype = subtype;
            ret.toughness = toughness;
            ret.type = type;

            ret.Owner = null;
            return ret;
        }

        internal void Tap()
        {
            IsTapped = true;
        }

        internal void UnTap()
        {
            IsTapped = false;
        }

        internal void TakeDamage(MtgCard dealer, int damage)
        {
            dealer.TriggerAbility(MtgTrigger.DealsCombatDamage, this);
            TriggerAbility(MtgTrigger.TakesCombatDamage);
            toughness -= damage;
            //dead
            if (toughness <= 0)
            {
                ChangeZone(MtgZone.Graveyard);
            }
        }

        internal void ChangeZone(MtgZone NewZone)
        {
            Zone = NewZone;

            switch (NewZone)
            {
                case MtgZone.Hand:
                    //TriggerAbility(MtgTrigger.EntersHand);
                    Owner.Library.Remove(this);
                    Owner.Hand.Add(this);
                    break;
                case MtgZone.Library:
                    break;
                case MtgZone.Play:
                    TriggerAbility(MtgTrigger.EntersPlay);
                    Owner.Hand.Remove(this);
                    Owner.InPlay.Add(this);
                    break;
                case MtgZone.Graveyard:
                    TriggerAbility(MtgTrigger.Death);
                    Owner.InPlay.Remove(this);
                    Owner.Graveyard.Add(this);
                    break;
                case MtgZone.Exiled:
                    break;
                default:
                    break;
            }

            UpdateIndex();
        }

        private MtgTrigger GetTrigger(string input)
        {
            if (input.Contains("enters the battlefield"))
                return MtgTrigger.EntersPlay;
            if (input.Contains("deals combat damage to a player"))
                return MtgTrigger.DealsPlayerDamage;

            return MtgTrigger.None;
        }

        private void SetTargets(MtgAbility ability, string input)
        {
            List<MtgTarget> Targets = new List<MtgTarget>();

            // foreach (Match match in Regex.Matches(input, "(" + name + @")|(target.*?(\b[^n][^o][^n]\w+))"))
            foreach (Match match in Regex.Matches(input, "(" + name + @")|(target.*?\b[^n\s][^o\s][^n\s]\w+)"))
            {
                List<MtgTargetFilter> Options = new List<MtgTargetFilter>();
                MtgTarget New = new MtgTarget();
                New.Description = match.Value;
                string[] words = match.Value.Split(' ');

                //If the target string is the card name, set the selectiontype and the target to self
                if (match.Value == name)
                {
                    New.Selection = MtgTargetSelection.Self;
                    ability.trigger_target = New;
                    continue;
                }
                //If the target string is a target selection, work out all the filters
                else if (words[0] == "target")
                {
                    //Work out the filters
                    foreach (string word in words)
                    {
                        string filtered = word;
                        if (word != "target")
                        {
                            if (word.EndsWith("s"))
                                filtered = filtered.Substring(0, word.Length - 1);

                            MtgTargetFilter Option = new MtgTargetFilter();
                            if (filtered.Substring(0, 3) == "non")
                            {
                                Option.Negated = true;
                                Option.Type = filtered.Substring(3);
                            }
                            else
                            {
                                Option.Type = filtered;
                                Option.Negated = false;
                            }
                            Options.Add(Option);
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
                New.Filters = Options.ToArray();
                Targets.Add(New);
            }

            ability.targets = Targets.ToArray();
        }

        private void SetActivationandCost(MtgAbility ability, string input)
        {
            MtgAbilityCost cost = new MtgAbilityCost();
            ability.activation = MtgAbilityActivation.Triggered;
            if (input.Contains("{Tap}"))
            {
                if (input.Contains("to your mana pool"))
                    ability.activation = MtgAbilityActivation.Mana;
                else
                {
                    ability.activation = MtgAbilityActivation.Activated;
                }
                cost.Tap = true;
            }
            else if (input.StartsWith("{"))
            {
                ability.activation = MtgAbilityActivation.Activated;
                //Set activation cost here
            }
            ability.cost = cost;
        }

        private void SetAction(MtgAbility input, string text)
        {
            input.action = MtgActionType.None;

            if (text.ContainsAnyCase("tap"))
            {
                input.action = MtgActionType.Tap;
            }

            if (text.ContainsAnyCase("to your mana pool"))
            {
                input.action = MtgActionType.Mana;
                List<MtgMana> add_mana = new List<MtgMana>();
                foreach (Match match in Regex.Matches(text, @"(?<={)(white|red|blue|white|black)(?=})", RegexOptions.IgnoreCase))
                {
                    switch (match.Value)
                    {
                        case "White":
                            add_mana.Add(new MtgMana(MtgColour.White));
                            break;

                        case "Black":
                            add_mana.Add(new MtgMana(MtgColour.Black));
                            break;

                        case "Blue":
                            add_mana.Add(new MtgMana(MtgColour.Blue));
                            break;

                        case "Red":
                            add_mana.Add(new MtgMana(MtgColour.Red));
                            break;

                        case "Green":
                            add_mana.Add(new MtgMana(MtgColour.Green));
                            break;
                    }
                }
                input.param_1 = add_mana.ToArray();
            }
            if (text.ContainsAnyCase("detain"))
            {
                input.action = MtgActionType.Detain;
            }
            if (text.ContainsAnyCase("draw a card"))
            {
                input.action = MtgActionType.DrawCard;
                input.param_1 = 1;
            }
        }

        private void SetEffects()
        {
            foreach (string token in Regex.Split(description, @"\r?\n"))
            {
                if (token.Length > 1)
                {
                    MtgAbility Ability = new MtgAbility(this);
                    Ability.description = token;
                    SetTargets(Ability, token);
                    Ability.trigger = GetTrigger(token);
                    SetAction(Ability, token);
                    SetActivationandCost(Ability, token);
                    Abilities.Add(Ability);
                }
            }
        }

        #endregion

        #region properties and their fields

        private bool _istapped;
        public bool IsTapped
        {
            get
            {
                return _istapped;
            }
            internal set
            {
                _istapped = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsTapped"));
                    PropertyChanged(this, new PropertyChangedEventArgs("rotation"));
                }
            }
        }

        private int _ownerIndex;
        public int OwnerIndex
        {
            get
            {
                if (Owner != null)
                    _ownerIndex = Owner.Index;
                return _ownerIndex;
            }
        }

        private int _index;
        public int Index
        {
            get
            {
                if (Owner != null)
                    _index = Owner.GetIndex(this);
                return _index;
            }
        }

        public MtgCard BlockingAgainst
        {
            get;
            internal set;
        }

        public bool IsLand
        {
            get
            {
                return type.Contains("Land");
            }
        }

        private MtgCardState _state;

        public MtgCardState State
        {
            get
            {
                return _state;
            }
            internal set
            {
                _state = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("State"));
                }
            }
        }

        private bool _summoningsickness;
        public bool SummoningSickness
        {
            get
            {
                return _summoningsickness;
            }
            internal set
            {
                _summoningsickness = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("SummoningSickness"));
            }
        }

        public int UntapsTurn
        {
            get;
            internal set;
        }

        internal List<MtgCard> blockers
        {
            get;
            set;
        }

        public MtgCard[] Blockers
        {
            get
            {
                return blockers.ToArray();
            }
        }

        public bool Tapped
        {
            get;
            internal set;
        }

        [NonSerialized]
        private MtgPlayer _owner;
        public MtgPlayer Owner
        {
            get
            {
                return _owner;
            }
            internal set
            {
                _owner = value;
                if (_owner != null)
                    _ownerIndex = _owner.Index;
            }
        }

        [NonSerialized]
        private MtgGame _parentGame;
        public MtgGame ParentGame
        {
            get
            {
                return _parentGame;
            }
            internal set
            {
                _parentGame = value;
            }
        }

        public MtgZone Zone
        {
            get;
            internal set;
        }

        public bool IsCreature
        {
            get
            {
                return type.Contains("Creature");
            }
        }

        public bool IsEnchantment
        {
            get
            {
                return type.Contains("Enchantment");
            }
        }

        public bool IsPermanent
        {
            get
            {
                return IsArtifact || IsCreature || IsEnchantment || IsLand;
            }
        }

        public bool IsArtifact
        {
            get
            {
                return type.Contains("Artifact");
            }
        }

        public bool IsSorcery
        {
            get
            {
                return type.Contains("Sorcery");
            }
        }

        public bool IsInstant
        {
            get
            {
                return type.Contains("Instant");
            }
        }

        public bool IsSpell
        {
            get
            {
                return IsSorcery || IsInstant;
            }
        }

        public int id
        {
            get;
            internal set;
        }

        public int set_number
        {
            get;
            internal set;
        }

        public string name
        {
            get;
            internal set;
        }

        public string description
        {
            get;
            internal set;
        }

        public string flavor
        {
            get;
            internal set;
        }

        public MtgColour colors
        {
            get;
            internal set;
        }

        public MtgMana[] manacost
        {
            get;
            internal set;
        }

        public MtgMana[] GetColouredManaCost
        {
            get
            {
                var Result = from MtgMana mana in manacost where mana.Type != MtgColour.Colourless select mana;
                return Result.ToArray();
            }
        }

        public MtgMana[] GetColourlessManaCost
        {
            get
            {
                var Result = from MtgMana mana in manacost where mana.Type == MtgColour.Colourless select mana;
                return Result.ToArray();
            }
        }

        public int convertedmanacost
        {
            get;
            internal set;
        }

        public string set
        {
            get;
            internal set;
        }

        public string type
        {
            get;
            internal set;
        }

        public string subtype
        {
            get;
            internal set;
        }

        public int power
        {
            get;
            internal set;
        }

        public int toughness
        {
            get;
            internal set;
        }

        public int loyalty
        {
            get;
            internal set;
        }

        public string rarity
        {
            get;
            internal set;
        }

        public string artist
        {
            get;
            internal set;
        }

        public string card_image
        {
            get;
            internal set;
        }

        public string card_set_id
        {
            get;
            internal set;
        }

        public DateTime released_at
        {
            get;
            internal set;
        }

        public override string ToString()
        {
            return name;
        }

        #endregion

        #region ability trigger methods

        internal void TriggerAbility(MtgTrigger trigger, object parameter = null)
        {
            foreach (MtgAbility ability in Abilities)
            {
                if (ability.activation == MtgAbilityActivation.Triggered && ability.trigger == trigger)
                    ability.Activate();
            }
        }

        #endregion
    }

    [Serializable]
    public struct MtgCardData
    {
        public int Id;
        public int set_number;
        public string name;
        public string description;
        public string flavor;
        public MtgColour colors;
        public string manacost;
        public int convertedmanacost;
        public string card_set_name;
        public string type;
        public string subtype;
        public int power;
        public int toughness;
        public int loyalty;
        public string rarity;
        public string artist;
        public string card_image;
        public string card_set_id;
        public DateTime card_released_at;
    }
}
