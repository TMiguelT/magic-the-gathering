﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MtgLib
{
    public enum MtgGameType
    {
        Normal,
        TwoHeadedGiant,
        Archenemy
    }

    /// <summary>
    /// Current Phase, yo
    /// </summary>
    public enum MtgPhase
    {
        Defending,
        Beginning,
        PrecombatMain,
        Combat,
        PostCombatMain,
        End,
    }
    //^ Best enum ever

    //public class MtgTeam : INotifyPropertyChanged
    //{
    //    public event PropertyChangedEventHandler PropertyChanged;

    //    public MtgTeam(params MtgPlayer[] players)
    //    {
    //        Players = players;
    //    }

    //    private void OnPlayerChanged()
    //    {
    //        if (PropertyChanged != null)
    //            PropertyChanged(this, new PropertyChangedEventArgs("Players"));
    //    }

    //    public MtgPlayer[] Players
    //    {
    //        get;
    //        set;
    //    }
    //}

    public class MtgGame : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler GameWon;
        public MtgPlayer PlayerOne;
        public MtgPlayer PlayerTwo;

        public MtgGame(MtgPlayer playerone, MtgPlayer playertwo)
        {
            PlayerOne = playerone;
            PlayerTwo = playertwo;

            PlayerOne.Opponent = PlayerTwo;
            PlayerTwo.Opponent = PlayerOne;

            PlayerOne.ParentGame = this;
            PlayerTwo.ParentGame = this;

            PlayerOne.PropertyChanged += new PropertyChangedEventHandler(OnPlayerChanged);
            PlayerTwo.PropertyChanged += new PropertyChangedEventHandler(OnPlayerChanged);
        }

        public void OnPlayerChanged(object sender, PropertyChangedEventArgs e)
        {
            foreach (MtgPlayer player in Players)
            {
                if (player.LifeTotal <= 0)
                {
                    if (GameWon != null)
                    {
                        GameWon(this, new EventArgs());
                    }
                }
            }
        }

        public MtgCard Localise(MtgCard remotecard, MtgPlayer owner, bool UpdateAbilities)
        {
            MtgCard Local = owner.CardInZone(remotecard.Zone, remotecard.Index);

            if (UpdateAbilities)
            {
                for (int i = 0; i < Local.Abilities.Count; i++)
                {
                    for (int j = 0; j < Local.Abilities[i].targets.Length; j++)
                    {
                        if (remotecard.Abilities[i].targets[j].IsFilled)
                        {
                            ////THIS PART HERE THE OWNER IS WRONG
                            int OwnerIndex = remotecard.Abilities[i].targets[j].Target.OwnerIndex;
                            Local.Abilities[i].targets[j].Target = Localise(remotecard.Abilities[i].targets[j].Target, OwnerIndex, false);
                        }
                    }
                }
            }

            return Local;
        }

        public MtgCard Localise(MtgCard remotecard, int playerIndex, bool UpdateAbilities)
        {
            return Localise(remotecard, Players[playerIndex], UpdateAbilities);
        }


        public void StartGame()
        {
            foreach (MtgPlayer player in Players)
            {
                player.SetupLibrary();
                player.ShuffleLibrary();
                player.DrawHand();
            }

            Random gen = new Random();

            ActivePlayer = Players[0];
            Phase = MtgPhase.Beginning;
            TurnNumber = 0;
        }

        /// <summary>
        /// One player
        /// </summary>
        public int TurnNumber
        {
            get;
            internal set;
        }

        /// <summary>
        /// Both players having a turn = 1 round
        /// </summary>
        public int RoundNumber
        {
            get
            {
                return (int)Math.Floor(TurnNumber / 2.0);
            }
        }

        public MtgGameType Type
        {
            get;
            internal set;
        }

        //public List<MtgTeam> Team
        //{
        //    get;
        //    internal set;
        //}

        private MtgPlayer _activeplayer;

        public MtgPlayer ActivePlayer
        {
            get
            {
                return _activeplayer;
            }
            internal set
            {
                _activeplayer = value;
                _activeplayer.HasPlayedLand = false;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ActivePlayer"));
            }
        }

        //public MtgTeam[] Teams
        //{
        //    get;
        //    internal set;
        //}

        public MtgPlayer[] Players
        {
            get
            {
                return new MtgPlayer[] { PlayerOne, PlayerTwo };
            }
        }

        private MtgPhase _phase;

        public MtgPhase Phase
        {
            get
            {
                return _phase;
            }
            internal set
            {
                _phase = value;
                if (Phase == MtgPhase.Beginning && TurnNumber != 0)
                {
                    ActivePlayer.StartTurn();
                }
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Phase"));
            }
        }

        public event EventHandler PhaseEnded;

        public void EndPhase()
        {
            switch (Phase)
            {
                case MtgPhase.Beginning:
                    Phase = MtgPhase.PrecombatMain;
                    break;

                case MtgPhase.PrecombatMain:
                    Phase = MtgPhase.Combat;
                    break;

                case MtgPhase.Combat:
                    if (ActivePlayer.Attacking.Length > 0)
                    {
                        ActivePlayer = ActivePlayer.Opponent;
                        Phase = MtgPhase.Defending;
                    }
                    else
                    {
                        Phase = MtgPhase.PostCombatMain;
                    }
                    break;

                //COMBAT DAMAGE DONE HERE ATM
                case MtgPhase.Defending:
                    ActivePlayer = ActivePlayer.Opponent;
                    Phase = MtgPhase.PostCombatMain;
                    foreach (MtgCard attacker in ActivePlayer.Attacking)
                    {
                        if (attacker.Blockers.Length > 0)
                        {
                            int power = attacker.power;

                            //Foreach blocker, deal any damage remaining to it, take damage from it
                            foreach (MtgCard blocker in attacker.Blockers)
                            {
                                if (blocker.toughness <= power)
                                    blocker.TakeDamage(attacker, blocker.toughness);
                                else
                                    blocker.TakeDamage(attacker, power);
                                //Attacker takes damage
                                attacker.TakeDamage(attacker, blocker.power);
                                blocker.State = MtgCardState.None;
                            }
                        }
                        else
                        {
                            ActivePlayer.DealDamage(attacker);
                        }
                        attacker.Tap();
                        attacker.State = MtgCardState.None;
                    }
                    ActivePlayer.ClearAttackers();
                    break;

                case MtgPhase.PostCombatMain:
                    foreach (MtgCard card in ActivePlayer.InPlay)
                    {
                        card.State = MtgCardState.None;
                    }
                    Phase = MtgPhase.End;
                    break;

                case MtgPhase.End:
                    EndTurn();
                    return;
            }

            if (PhaseEnded != null)
                PhaseEnded(this, new EventArgs());
        }

        public void EndTurn()
        {
            ActivePlayer = ActivePlayer.Opponent;
            Phase = MtgPhase.Beginning;
            TurnNumber++;

            if (PhaseEnded != null)
                PhaseEnded(this, new EventArgs());
        }
    }
}
