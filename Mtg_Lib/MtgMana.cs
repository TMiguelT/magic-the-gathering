﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using StringExtensions;

namespace MtgLib
{
    [Serializable]
    public struct MtgMana
    {
        public override string ToString()
        {
            return Type.ToString();
        }

        public MtgMana(MtgColour type)
        {
            Type = type;
            Land = null;
        }

        public MtgMana(MtgColour type, MtgCard land)
            : this(type)
        {
            Type = type;
            Land = land;
        }

        public override bool Equals(object obj)
        {
            MtgMana compareto = (MtgMana)obj;
            return Type == compareto.Type;
        }

        public MtgColour Type;
        public MtgCard Land;

        public void Use()
        {
            if (Land != null)
            {
                Land.Tap();
            }
        }

        public static MtgColour ParseColour(string input)
        {
            MtgColour Return = MtgColour.Colourless;
            foreach (char colour in input)
            {
                switch (colour)
                {
                    case 'B':
                        Return |= MtgColour.Black;
                        break;

                    case 'U':
                        Return |= MtgColour.Blue;
                        break;

                    case 'W':
                        Return |= MtgColour.White;
                        break;

                    case 'R':
                        Return |= MtgColour.Red;
                        break;

                    case 'G':
                        Return |= MtgColour.Green;
                        break;

                    default:
                        break;
                }
            }
            return Return;
        }

        public static MtgMana[] ParseCosts(string input)
        {
            List<MtgMana> Return = new List<MtgMana>();
            //  foreach (string match in Regex.Split(input, @"^(\d+)|{|}"))
            foreach (Match match in Regex.Matches(input, @"((?<={).+?(?=\}))|(\w)"))
            {
                if (match.Value != "")
                {
                    int Out;
                    if (int.TryParse(match.Value, out Out))
                    {
                        for (int i = 0; i < Out; i++)
                        {
                            Return.Add(new MtgMana(MtgColour.Colourless));
                        }
                    }
                    else
                    {
                        Return.Add(new MtgMana(ParseColour(match.Value)));
                    }
                }
            }
            return Return.ToArray();
        }
    }
}
