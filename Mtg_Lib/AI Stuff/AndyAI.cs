﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MtgLib;

namespace MtgAI
{
    public class AndyAI : MtgPlayerAI
    {
        public AndyAI(MtgPlayer player, MtgGame game)
            : base(player, game)
        { }

        public override string Name
        {
            get 
            {
                return Name;
            }
        }

        new public static string AIName
        {
            get
            {
                return "AndyAI";
            }
        }


        public override void PhaseChanged()
        {

            if (Player.IsTurn)
            {
                switch (Game.Phase)
                {
                    case (MtgPhase.Beginning):

                        break;

                    case (MtgPhase.PostCombatMain):

                        break;

                    case (MtgPhase.PrecombatMain):


                        //Find a land and play it
                        foreach (MtgCard card in Player.Hand)
                        {
                            if (card.IsLand)
                            {
                                if (Player.CanPlay(card))
                                {
                                    Player.PlayCard(card);
                                    break;
                                }
                            }
                        }

                        //Find a creature card and play it
                    GoGoGadget: foreach (MtgCard card in Player.Hand)
                        {
                            if (card.IsCreature)
                            {
                                if (Player.CanPlay(card))
                                {
                                    Player.PlayCard(card);
                                    goto GoGoGadget;
                                }
                            }
                        }

                        break;

                    case (MtgPhase.Combat):
                        foreach (MtgCard card in Player.InPlay)
                        {
                            if (Player.CanAttackWith(card))
                            {
                                Player.AttackWith(card);
                            }
                        }

                        break;

                    case (MtgPhase.Defending):

                        break;

                    case (MtgPhase.End):

                        break;
                }
            }

            //If it's the main phase and it's my turn
            if (Game.Phase == MtgPhase.PrecombatMain && Player.IsTurn)
            {

            }
        }
    }
}
