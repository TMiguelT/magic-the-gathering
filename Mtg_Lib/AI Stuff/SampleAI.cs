﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MtgLib;

namespace MtgAI
{
    public class SampleAI : MtgPlayerAI
    {
        public override string Name
        {
            get { return "SampleAI"; }
        }

        public SampleAI(MtgPlayer player, MtgGame game)
            : base(player, game)
        { }

        public override void PhaseChanged()
        {
            //If it's the main phase and it's my turn
            if (Game.Phase == MtgPhase.PrecombatMain && Player.IsTurn)
            {
                //Find a land and play it
                foreach (MtgCard card in Player.Hand)
                {
                    if (card.IsLand)
                    {
                        Player.PlayCard(card);
                        return;
                    }
                }
            }
        }
    }
}
