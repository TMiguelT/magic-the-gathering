﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MtgLib;

namespace MtgAI
{
    public class DanAI : MtgPlayerAI
    {
        public DanAI(MtgPlayer player, MtgGame game)
            : base(player, game)
        { }
        
        public override string Name
        {
            get
            {
                return Name;
            }
        }

        new public static string AIName
        {
            get
            {
                return "DanAI";
            }
        }

        public override void PhaseChanged()
        {

            if (Player.IsTurn)
            {
                switch (Game.Phase)
                {
                    case (MtgPhase.Beginning):

                        break;

                    case (MtgPhase.PostCombatMain):

                        break;

                    case (MtgPhase.PrecombatMain):


                        //Find a land and play it
                        foreach (MtgCard card in Player.Hand)
                        {
                            if (card.IsLand)
                            {
                                if (Player.CanPlay(card))
                                {
                                    Player.PlayCard(card);
                                    break;
                                }
                            }
                        }


                        MtgCard most_expensive = null;
                        //Find a creature card, find the most expensive, and play it.
                        // start_loop: 
                        foreach (MtgCard card in Player.Hand)
                        {
                            if ((card.IsCreature) && (Player.CanPlay(card)))
                            {
                                if ((most_expensive == null))
                                {
                                    most_expensive = card;
                                }
                                else
                                {
                                    if (card.convertedmanacost > most_expensive.convertedmanacost)
                                    {
                                        most_expensive = card;
                                    }
                                }
                            }

                        }

                        if (most_expensive != null)
                        {
                            Player.PlayCard(most_expensive);
                        }
                        // goto start_loop;  
                        break;


                    case (MtgPhase.Combat):
                        foreach (MtgCard card in Player.InPlay)
                        {
                            if (Player.CanAttackWith(card))
                            {
                                Player.AttackWith(card);
                            }
                        }

                        break;

                    case (MtgPhase.Defending):

                        break;

                    case (MtgPhase.End):

                        break;
                }
                Game.EndPhase();
            }

            //If it's the main phase and it's my turn
            if (Game.Phase == MtgPhase.PrecombatMain && Player.IsTurn)
            {

            }
        }
    }
}
