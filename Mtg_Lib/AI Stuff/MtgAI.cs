﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MtgLib;

namespace MtgAI
{
    public abstract class MtgPlayerAI
    {
        protected MtgPlayer Player;
        protected MtgGame Game;

        public MtgPlayerAI(MtgPlayer player, MtgGame game)
        {
            Player = player;
            Game = game;
        }

        public abstract string Name
        {
            get;
        }

        public static string AIName
        {
            get
            {
                return "Implement this!!!";
            }
        }

        public abstract void PhaseChanged();

        public override string ToString()
        {
            return Name;
        }
    }
}
