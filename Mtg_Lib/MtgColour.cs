﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MtgLib
{
    [Flags()]
    public enum MtgColour
    {
        Colourless = 0,
        Black = 1,
        Blue = 2,
        Green = 4,
        Red = 8,
        White = 16
    }
}
