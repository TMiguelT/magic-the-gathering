﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace MtgLib
{
    public class MtgStack : Stack<MtgCard>, INotifyCollectionChanged
    {
        public virtual event NotifyCollectionChangedEventHandler CollectionChanged;

        public new MtgCard Pop()
        {
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove));
            return base.Pop();
        }

        public new void Push(MtgCard card)
        {
            base.Push(card);
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add));
        }

        public new void Clear()
        {
            base.Clear();
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }

   /* public class MtgQueue : Queue<MtgCard>, INotifyCollectionChanged
    {
        public virtual event NotifyCollectionChangedEventHandler CollectionChanged;

        public MtgCard Pop()
        {
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove));
            return base.Pop();
        }

        public void Push(MtgCard card)
        {
            base.Push(card);
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add));
        }

        public new void Clear()
        {
            base.Clear();
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }*/

    public class MtgList : List<MtgCard>, INotifyCollectionChanged
    {
        public virtual event NotifyCollectionChangedEventHandler CollectionChanged;

        public new void Add(MtgCard card)
        {
            base.Add(card);
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add));
        }

        public new void Clear()
        {
            base.Clear();
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }
}
