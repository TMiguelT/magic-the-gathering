﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using MiguelUtil;
using System.Collections;

namespace MtgLib
{
    public class MtgPlayer : INotifyPropertyChanged
    {
        private List<MtgCard> attacking;
        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return Name;
        }

        public MtgPlayer(string name)
        {
            Name = name;
            LifeTotal = 20;
            Hand = new ObservableCollection<MtgCard>();
            Library = new ObservableCollection<MtgCard>();
            Graveyard = new ObservableCollection<MtgCard>();
            Deck = new List<MtgCard>();
            InPlay = new ObservableCollection<MtgCard>();
            attacking = new List<MtgCard>();
            HasPlayedLand = false;

            InPlay.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(InPlay_CollectionChanged);
            Hand.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Hand_CollectionChanged);
            Graveyard.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Graveyard_CollectionChanged);
            Library.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Library_CollectionChanged);
        }

        void Library_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }

        void Graveyard_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }

        void Hand_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            PropertyChanged(this, new PropertyChangedEventArgs("GetHand"));
        }

        void InPlay_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            PropertyChanged(this, new PropertyChangedEventArgs("GetInPlayCreatures"));
            PropertyChanged(this, new PropertyChangedEventArgs("GetInPlayOther"));
            PropertyChanged(this, new PropertyChangedEventArgs("GetInPlayLands"));
        }

        public MtgPlayer(string name, MtgCardData[] deck, bool isai, int playerindex)
            : this(name)
        {
            IsAI = isai;
            _index = playerindex;
            foreach (MtgCardData card in deck)
            {
                MtgCard NewCard = new MtgCard(card);
                Deck.Add(NewCard);
                NewCard.Owner = this;
                NewCard.ParentGame = ParentGame;
            }
        }

        public MtgCard CardFromReceived(MtgCard rec)
        {
            MtgCard ret = CardInZone(rec.Zone, rec.Index);
            return ret;
        }

        public MtgCard CardInZone(MtgZone zone, int index)
        {
            switch (zone)
            {
                case MtgZone.Hand:
                    return Hand[index];
                case MtgZone.Library:
                    return Library[index];
                case MtgZone.Play:
                    return InPlay[index];
                case MtgZone.Graveyard:
                    return Graveyard[index];
                case MtgZone.Exiled:
                    break;
                default:
                    break;
            }
            return null;
        }

        public int GetIndex(MtgCard card)
        {
            switch (card.Zone)
            {
                case MtgZone.Hand:
                    return Hand.IndexOf(card);
                case MtgZone.Library:
                    return Library.IndexOf(card);
                case MtgZone.Play:
                    return InPlay.IndexOf(card);
                case MtgZone.Graveyard:
                    return Graveyard.IndexOf(card);
                case MtgZone.Exiled:
                    break;
                default:
                    break;
            }
            return -1;
        }

        public void PlayCard(MtgCard card)
        {
            if (ParentGame.ActivePlayer == this && (ParentGame.Phase == MtgPhase.PostCombatMain || ParentGame.Phase == MtgPhase.PrecombatMain))
            {
                if (card.type.Contains("Land"))
                {
                    if (ParentGame.ActivePlayer.HasPlayedLand == false)
                        card.PutIntoPlay();
                    else
                        throw new Exception("Can't play more than one land");
                }
                else
                {
                    if (CanPlay(card))
                    {
                        card.PutIntoPlay();
                    }
                    else
                        throw new Exception("Not enough mana");
                }

            }
            else
            {
                throw new Exception("Not your turn/main phase!");
            }
        }

        internal bool CanAttackWith(MtgCard card)
        {
            return ParentGame.Phase == MtgPhase.Combat && card.IsCreature && card.IsTapped == false && card.SummoningSickness == false && attacking.Contains(card) == false;
        }

        public void AttackWith(MtgCard card)
        {
            if (CanAttackWith(card))
            {
                attacking.Add(card);
                card.State = MtgCardState.Attacking;
            }
            else
            {
                throw new Exception("You can't attack yet bro");
            }
        }

        public void WithdrawFromCombat(MtgCard card)
        {
            if (attacking.Contains(card))
            {
                attacking.Remove(card);
                card.State = MtgCardState.None;
            }
            else
            {
                throw new Exception("He's not attacking bro");
            }
        }


        internal void DealDamage(MtgCard card)
        {
            card.Tap();
            card.TriggerAbility(MtgTrigger.DealsPlayerDamage, this);
            Opponent.LifeTotal -= card.power;

        }

        public MtgCard[] Attacking
        {
            get
            {
                if (attacking.Count > 0)
                    return attacking.ToArray();
                else
                    return new MtgCard[0];
            }
        }

        internal void ClearAttackers()
        {
            attacking.Clear();
        }

        public bool IsTurn
        {
            get
            {
                return ParentGame.ActivePlayer == this;
            }
        }

        public MtgPlayer Opponent
        {
            get;
            internal set;
        }

        public bool IsAI
        {
            get;
            internal set;
        }

        public MtgGame ParentGame
        {
            get;
            internal set;
        }

        internal void StartTurn()
        {
            DrawCard();
            foreach (MtgCard card in InPlay)
            {
                if (card.UntapsTurn == 0)
                    card.UnTap();
                else
                    card.UntapsTurn--;
                card.SummoningSickness = false;
            }
        }

        public event EventHandler Shuffled;

        internal void ShuffleLibrary()
        {
            int Seed = Name.GetHashCode();
            Library.Shuffle(Seed);
            if (Shuffled != null)
                Shuffled(this, new EventArgs());
        }

        public bool CanPlay(MtgCard card)
        {
            if (IsTurn && (ParentGame.Phase == MtgPhase.PostCombatMain || ParentGame.Phase == MtgPhase.PrecombatMain))
            {

                //List<MtgMana> ToUntap = new List<MtgMana>();
                List<MtgMana> CurrentMana = new List<MtgMana>(GetMana());

                foreach (MtgMana mana in card.GetColouredManaCost)
                {
                    if (CurrentMana.Contains(mana))
                    {
                        CurrentMana.Remove(mana);
                    }
                    else
                        return false;
                }

                foreach (MtgMana mana in card.GetColourlessManaCost)
                {
                    if (CurrentMana.Count > 0)
                    {
                        CurrentMana.RemoveAt(0);
                    }
                    else
                        return false;
                }

                if (card.IsLand && HasPlayedLand)
                    return false;

                return true;
            }

            return false;
        }

        internal void SetupLibrary()
        {
            Library = new ObservableCollection<MtgCard>(Deck);
        }

        public MtgMana[] GetMana()
        {
            List<MtgMana> Mana = new List<MtgMana>();
            foreach (MtgCard card in InPlay)
            {
                if (card.IsLand && card.IsTapped == false)
                    Mana.Add(new MtgMana(MtgMana.ParseColour(card.description), card));
            }
            return Mana.ToArray();
        }

        private int _number;
        public int Number
        {
            get
            {
                if (ParentGame != null)
                    _number = this == ParentGame.PlayerOne ? 1 : 2;
                return _number;
            }
        }

        private int _index;
        public int Index
        {
            get
            {
                if (ParentGame != null)
                    _index = ((IList)ParentGame.Players).IndexOf(this);
                return _index;
            }
        }

        public bool HasPlayedLand
        {
            get;
            set;
        }

        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            internal set
            {
                _name = value;
            }
        }

        private int _lifetotal;

        public int LifeTotal
        {
            get
            {
                return _lifetotal;
            }
            set
            {
                _lifetotal = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("LifeTotal"));
            }
        }

        internal void DrawHand()
        {
            Hand.Clear();
            for (int i = 0; i <= 7; i++)
            {
                MtgCard Draw = Library[i];
                Draw.ChangeZone(MtgZone.Hand);
            }
        }

        internal void DrawCard()
        {
            Hand.Add(Library[0]);
            Library.RemoveAt(0);
        }

        public MtgCard[] GetHand
        {
            get
            {
                return Hand.ToArray();
            }
        }


        public MtgCard[] GetGraveyard
        {
            get
            {
                return Graveyard.ToArray();
            }
        }

        public MtgCard[] GetInPlayCreatures
        {
            get
            {
                var creatures = from MtgCard card in InPlay where card.IsCreature select card;
                return creatures.ToArray();
            }
        }

        public MtgCard[] GetInPlayOther
        {
            get
            {
                var stuff = from MtgCard card in InPlay where card.IsPermanent && card.IsCreature == false && card.IsLand == false select card;
                return stuff.ToArray();
            }
        }

        public MtgCard[] GetInPlayLands
        {
            get
            {
                var lands = from MtgCard card in InPlay where card.IsLand select card;
                return lands.ToArray();
            }
        }

        internal ObservableCollection<MtgCard> Hand;

        internal List<MtgCard> Deck;

        internal ObservableCollection<MtgCard> Library;

        internal ObservableCollection<MtgCard> Graveyard;

        //Fix this
        public ObservableCollection<MtgCard> InPlay;
    }
}
