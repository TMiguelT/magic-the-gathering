﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MiguelUtil.Controls
{
    public class SelectionEventArgs : KeyEventArgs
    {
        public bool Manual;
        public bool KeyTyped;

        public SelectionEventArgs(Keys keys, bool manual = true, bool keytyped = false)
            : base(keys)
        {
            Manual = manual;
            KeyTyped = keytyped;
        }

        public SelectionEventArgs(bool manual = true, bool keytyped = false)
            : base(Keys.None)
        {
            Manual = manual;
            KeyTyped = keytyped;
        }
    }
}
