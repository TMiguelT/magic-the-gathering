﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MiguelUtil.Controls.Undoable.Data_Grid_View;

namespace MiguelUtil.Controls
{
    public static class ControlMethods
    {
        public static object[] GetValues(DataGridViewRow Row)
        {
            object[] Return = new object[Row.Cells.Count];
            for (int i = 0; i < Row.Cells.Count; i++)
            {
                Return[i] = Row.Cells[i].Value;
            }
            return Return;
        }


    }
}
