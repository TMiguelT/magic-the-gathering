﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
using MiguelUtil.Find;
using MiguelUtil.Find.ResultTypes;
using System.Text.RegularExpressions;
using MiguelUtil.Controls.APIClasses;
using MiguelUtil.Controls.Undoable;
using MiguelUtil.Controls.Undoable.Text;
using MiguelUtil.Controls.API_Classes;
using System.IO;
using MiguelUtil.RTF;

namespace MiguelUtil.Controls
{
    public partial class RichTextBoxEx : RichTextBox, IUndoableControl
    {
        #region Variables
        private const int PFM_SPACEAFTER = 128;
        private const int PFM_LINESPACING = 256;
        private const int EM_SETPARAFORMAT = 1095;
        private const int EM_SETSEL = 1201;

        public int LastSelectionStart;
        public int LastSelectionLength;
        private int CurrentSelectionStart;
        private int CurrentSelectionLength;

        private StringBuilder UndoBuild = new StringBuilder();
        public int UndoPosition = -1;
        public int UndoLength;
        public TextBoxUndoType UndoType = TextBoxUndoType.None;
        private UndoableActionList undoqueue = new UndoableActionList();

        public bool HasTyped = false;
        private bool SelectWasManual = true;
        private Keys Typed;

        private readonly char[] Brackets = new char[] { '{', '}' };

        protected int[] CurrentBrackets = new int[] { -1, -1 };

        public bool SearchSelection = false;

        private int _LineSpacing = 256;

        int? SavedSelectionStart;
        int? SavedSelectionLength;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new RichTextBox with default settings.
        /// </summary>
        public RichTextBoxEx()
        {
            LineSpacing = PFM_LINESPACING;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the RichTextBoxEx's UndoQueue.
        /// </summary>
        public UndoableActionList UndoQueue
        {
            get
            {
                return undoqueue;
            }
            set
            {
                undoqueue = value;
            }
        }

        /// <summary>
        /// Gets or sets the amount of spacing between individual lines in the text box.
        /// </summary>
        public int LineSpacing
        {
            get
            {
                return _LineSpacing;
            }
            set
            {
                _LineSpacing = value;

                ParagraphFormat Format = new ParagraphFormat();
                Format.cbSize = Marshal.SizeOf(Format);
                Format.dwMask |= PFM_LINESPACING | PFM_SPACEAFTER;
                Format.dyLineSpacing = _LineSpacing;
                Format.bLineSpacingRule = Convert.ToByte(4);
                SendMessage(Handle, EM_SETPARAFORMAT, 0, ref Format); // 0 - to all text. 1 - only to selected
            }
        }

        /// <summary>
        /// Returns the highest visible line of the text box
        /// </summary>
        public int TopLine
        {
            get
            {
                return SendMessage(Handle, SendMessageValues.EM_GETFIRSTVISIBLELINE, IntPtr.Zero, IntPtr.Zero);
            }
        }

        /// <summary>
        /// Gets or sets the current cursor position.
        /// </summary>
        new public int SelectionStart
        {
            get
            {
                return base.SelectionStart;
            }
            set
            {
                EnableUpdating(false);
                SelectWasManual = false;
                base.SelectionStart = value;
                EnableUpdating(true);
            }
        }

        /// <summary>
        /// Gets or sets the length of the current selection.
        /// </summary>
        new public int SelectionLength
        {
            get
            {
                return base.SelectionLength;
            }
            set
            {
                EnableUpdating(false);
                SelectWasManual = false;
                base.SelectionLength = value;
                EnableUpdating(true);
            }
        }

        /// <summary>
        /// Gets a value determining if the text box can redo.
        /// </summary>
        new public bool CanRedo
        {
            get
            {
                return UndoQueue.CanRedo;
            }
        }

        /// <summary>
        /// Do not use this property as it will result in an error.
        /// </summary>
        [System.ComponentModel.EditorBrowsable(EditorBrowsableState.Never)]
        new public string RedoActionName
        {
            get
            {
                throw new NotImplementedException("Use the proper undo methods.");
            }
        }

        /// <summary>
        /// Do not use this property as it will result in an error.
        /// </summary>
        [System.ComponentModel.EditorBrowsable(EditorBrowsableState.Never)]
        new public string UndoActionName
        {
            get
            {
                throw new NotImplementedException("Use the proper undo methods.");
            }
        }

        /// <summary>
        /// Gets a value determining if the text box can undo.
        /// </summary>
        new public bool CanUndo
        {
            get
            {
                return UndoQueue.CanUndo || UndoLength > 0;
            }
        }

        #endregion

        #region Event Overrides

        /// <summary>
        /// Fires when the textbox has been changed
        /// </summary>
        new public event EventHandler<SelectionEventArgs> SelectionChanged;

        /// <summary>
        /// Standard OnSelectionChanged
        /// </summary>
        /// <param name="e">Standard event args.</param>
        protected override void OnSelectionChanged(EventArgs e)
        {
            OnSelectionChanged(new SelectionEventArgs(Typed, SelectWasManual, HasTyped));
            if (SelectWasManual)
                HasTyped = false;
            SelectWasManual = true;
            base.OnSelectionChanged(e);
        }

        /// <summary>
        /// Improved OnSelectedChanged using a SelectionEventArgs
        /// </summary>
        /// <param name="e">A selection event args</param>
        protected virtual void OnSelectionChanged(SelectionEventArgs e)
        {
            if (e.Manual)
            {
                LastSelectionStart = CurrentSelectionStart;
                LastSelectionLength = CurrentSelectionLength;

                CurrentSelectionStart = SelectionStart;
                CurrentSelectionLength = SelectionLength;

                if (e.KeyTyped)
                {
                    if (SelectionStart < CurrentBrackets[1])
                        CurrentBrackets[1]++;
                    else if (SelectionStart < CurrentBrackets[0])
                        CurrentBrackets[0]++;
                }

                if (e.KeyTyped == false)
                    SaveUndoBlock();

                SearchSelection = false;

                if (SelectionLength == 0)
                {
                    DeColourSelectedBrackets();
                    if (SelectionStart < TextLength && Brackets.Contains(Text[SelectionStart]))
                    {
                        int attached = FindAttachedBracket(SelectionStart);
                        if (attached != -1)
                            CurrentBrackets = new int[] { SelectionStart, attached };
                        ColourSelectedBrackets();

                    }
                    else if (SelectionStart < TextLength && SelectionStart != 0 && Brackets.Contains(Text[SelectionStart - 1]))
                    {
                        int attached = FindAttachedBracket(SelectionStart - 1);
                        if (attached != -1)
                            CurrentBrackets = new int[] { SelectionStart - 1, attached };
                        ColourSelectedBrackets();
                    }
                }
            }
            if (BeforePaste != null)
                SelectionChanged(this, e);
        }

        /// <summary>
        /// Fires when the textbox selection changes.
        /// </summary>
        new public event EventHandler<ChangedEventArgs> TextChanged;

        /// <summary>
        /// Base OnSelectionChanged
        /// </summary>
        /// <param name="e"></param>
        protected override void OnTextChanged(EventArgs e)
        {
            OnTextChanged(new ChangedEventArgs(CanUndo));
        }

        /// <summary>
        /// Base OnSelectionChanged
        /// </summary>
        /// <param name="e"></param>
        protected void OnTextChanged(ChangedEventArgs e)
        {
            if (TextChanged != null)
                TextChanged(this, e);
        }

        /// <summary>
        /// Fires just before the text box peforms a paste.
        /// </summary>
        public event HandledEventHandler BeforePaste;

        /// <summary>
        /// Fires just before the text box peforms a paste.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected virtual bool OnBeforePaste(HandledEventArgs e)
        {
            string ToPaste = Clipboard.GetText();
            if (ToPaste.Length > 0)
                UndoQueue.ActionPeformed(new UndoableTextAction(TextBoxUndoType.AddText, SelectionStart, Clipboard.GetText(), this));
            if (BeforePaste != null)
                BeforePaste(this, e);
            return e.Handled;
        }

        /// <summary>
        /// You have to implement all the undo settings if you override this.
        /// </summary>
        new public event KeyEventHandler KeyDown;

        delegate void KeyMethod(object sender, KeyEventArgs e, char Typed);

        /// <summary>
        /// Improved OnKeyDown with undo building.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            Typed = e.KeyData;

            KeyMethod ToCall = null;

            if (e.KeyCode == Keys.Z && e.Control)
            {
                UndoType = TextBoxUndoType.None;
                ToCall = OnUndoPressed;
            }
            //Undo Building
            else if ((e.Modifiers & ~Keys.Shift) == Keys.None)
            {
                char AsChar = Convert.ToChar(MapVirtualKey((uint)e.KeyCode, 2));
                if (!e.Shift)
                    AsChar = char.ToLower(AsChar);
                bool CanRunSave = true;
                TextBoxUndoType PreviousUndo = UndoType;
                char[] InvalidChars = new char[] { '\0', '\b', '\t', '\r', '\n' };

                if (InvalidChars.Contains(AsChar) == false)
                {
                    HasTyped = true;
                    // If text is highlighted, it is about to be deleted.
                    if (SelectionLength > 0)
                    {
                        SaveUndoBlock();
                        // Fixes strange RTB behaviour
                        if (SelectedText.EndsWith("\n") && SelectionLength > 1)
                        {
                            int Index = SelectedRtf.LastIndexOf("\\par\r\n");
                            string NewRtf = SelectedRtf.Remove(Index, ("\\par").Length);
                            UndoQueue.ActionPeformed(new UndoableTextAction(TextBoxUndoType.DeleteText, SelectionStart, NewRtf, SelectionLength - 1, this));
                        }
                        else
                            UndoQueue.ActionPeformed(new UndoableTextAction(TextBoxUndoType.DeleteText, SelectionStart, SelectedRtf, SelectionLength, this));
                        RestartUndoBlock();
                        UndoType = TextBoxUndoType.None;
                    }
                }

                if (UndoPosition == -1)
                    UndoPosition = SelectionStart;

                // Normal character
                if (char.IsLetterOrDigit(AsChar) || char.IsPunctuation(AsChar) || char.IsSymbol(AsChar) || AsChar == ' ')
                {
                    ToCall = OnLetterTyped;
                    UndoType = TextBoxUndoType.AddText;
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    UndoType = TextBoxUndoType.AddText;
                    ToCall = OnEnterPressed;
                    CanRunSave = false;
                }
                else if (e.KeyCode == Keys.Back)
                {
                    UndoType = TextBoxUndoType.BackspaceText;
                    ToCall = OnBackspacePressed;
                }
                else if (e.KeyCode == Keys.Delete)
                {
                    UndoType = TextBoxUndoType.DeleteText;
                    ToCall = OnDeletePressed;
                }
                else if (e.KeyCode == Keys.Tab)
                {
                    UndoType = TextBoxUndoType.AddText;
                    ToCall = OnTabPressed;
                }
                else
                    CanRunSave = false;

                if (ToCall != null)
                    ToCall(this, e, AsChar);

                if (CanRunSave)
                {
                    if (PreviousUndo != UndoType && PreviousUndo != TextBoxUndoType.None)
                        if (UndoLength > 0)
                            SaveUndoBlock();
                        else
                            RestartUndoBlock();
                }
                else
                    UndoPosition = -1;

            }
            if (KeyDown != null)
                KeyDown(this, e);
            base.OnKeyDown(e);
        }

        #endregion

        #region KeyPress

        /// <summary>
        /// Fires when the tab key is about to be added to the undo builder. You must implement your own
        /// undo building of tab if this event is used.
        /// </summary>
        public event KeyEventHandler UndoPressed;

        /// <summary>
        /// Fires when the tab key is about to be added to the undo builder. You must implement your own
        /// undo building of tab if this event is used.
        /// </summary>
        /// <param name="sender">The sending control</param>
        /// <param name="e">Basic event args</param>
        /// <param name="typed">The character typed/.</param>
        private void OnUndoPressed(object sender, KeyEventArgs e, char key)
        {
            if (UndoPressed != null)
                UndoPressed(sender, e);
            else
            {
                AddUndoText("  ");
                SelectedText = "  ";
                e.SuppressKeyPress = true;
            }
        }

        /// <summary>
        /// Fires when the tab key is about to be added to the undo builder. You must implement your own
        /// undo building of tab if this event is used.
        /// </summary>
        public event KeyEventHandler TabPressed;

        /// <summary>
        /// Fires when the tab key is about to be added to the undo builder. You must implement your own
        /// undo building of tab if this event is used.
        /// </summary>
        /// <param name="sender">The sending control</param>
        /// <param name="e">Basic event args</param>
        /// <param name="typed">The character typed/.</param>
        private void OnTabPressed(object sender, KeyEventArgs e, char typed)
        {
            if (TabPressed != null)
                TabPressed(sender, e);
            else
                AddUndoText(typed);
        }

        /// <summary>
        /// Fires when the delete key is about to be added to the undo builder. You must implement your own
        /// undo building of delete if this event is used.
        /// </summary>
        public event KeyEventHandler DeletePressed;

        /// <summary>
        /// Fires when the delete key is about to be added to the undo builder. You must implement your own
        /// undo building of delete if this event is used.
        /// </summary>
        /// <param name="sender">The sending control</param>
        /// <param name="e">Basic event args</param>
        /// <param name="typed">The character typed/.</param>
        private void OnDeletePressed(object sender, KeyEventArgs e, char typed)
        {
            if (DeletePressed != null)
                DeletePressed(sender, e);
            else
            {
                if (SelectionLength == 0)
                {
                    if (SelectionStart != TextLength)
                    {
                        EnableUpdating(false);
                        Select(SelectionStart, 1);
                        AddUndoText(SelectedRtf, SelectionLength);
                        Select(SelectionStart, 0);
                        EnableUpdating(true);
                    }
                    else
                        e.Handled = true;
                }
                else
                {
                    if (SelectedText.EndsWith("\n") && SelectionLength > 1 && IsCharacterFirst(SelectionStart) == false)
                    {
                        int Index = SelectedRtf.LastIndexOf("\\par\r\n");
                        string NewRtf = SelectedRtf.Remove(Index, ("\\par").Length);
                        UndoQueue.ActionPeformed(new UndoableTextAction(TextBoxUndoType.DeleteText, SelectionStart, NewRtf, SelectionLength - 1, this));
                    }
                    else
                        UndoQueue.ActionPeformed(new UndoableTextAction(TextBoxUndoType.DeleteText, SelectionStart, SelectedRtf, SelectionLength, this));
                    RestartUndoBlock();
                }
            }
        }

        /// <summary>
        /// Fires when the backspace key is about to be added to the undo builder. You must implement your own
        /// undo building of backspace if this event is used.
        /// </summary>
        public event KeyEventHandler BackspacePressed;

        /// <summary>
        /// Fires when the backspace key is about to be added to the undo builder. You must implement your own
        /// undo building of backspace if this event is used.
        /// </summary>
        /// <param name="sender">The sending control</param>
        /// <param name="e">Basic event args</param>
        /// <param name="typed">The character typed/.</param>
        private void OnBackspacePressed(object sender, KeyEventArgs e, char typed)
        {
            if (BackspacePressed != null)
                BackspacePressed(sender, e);
            else
            {
                if (SelectionLength == 0)
                {
                    if (SelectionStart != 0)
                    {
                        EnableUpdating(false);
                        Select(SelectionStart - 1, 1);
                        AddUndoText(SelectedRtf, SelectionLength, true);
                        Select(SelectionStart + 1, 0);
                        EnableUpdating(true);
                    }
                    else
                        e.Handled = true;
                }
                else
                {
                    //if (SelectedText.EndsWith("\n") && SelectionLength > 1 && IsCharacterFirst(SelectionStart) == false)
                    //{
                    //    int Index = SelectedRtf.LastIndexOf("\\par\r\n");
                    //    string NewRtf = SelectedRtf.Remove(Index, ("\\par").Length);
                    //    UndoQueue.ActionPeformed(new UndoableTextAction(TextBoxUndoType.DeleteText, SelectionStart, NewRtf, SelectionLength - 1, this));
                    //}
                    //else
                    UndoQueue.ActionPeformed(new UndoableTextAction(TextBoxUndoType.DeleteText, SelectionStart, SelectedRtf, SelectionLength, this));
                    RestartUndoBlock();
                }
            }
        }

        /// <summary>
        /// Fires when the enter key is about to be added to the undo builder. You must implement your own
        /// undo building of enter if this event is used.
        /// </summary>
        public event KeyEventHandler EnterPressed;

        /// <summary>
        /// Fires when the enter key is about to be added to the undo builder. You must implement your own
        /// undo building of enter if this event is used.
        /// </summary>
        /// <param name="sender">The sending control</param>
        /// <param name="e">Basic event args</param>
        /// <param name="typed">The character typed/.</param>
        private void OnEnterPressed(object sender, KeyEventArgs e, char typed)
        {
            if (EnterPressed != null)
                EnterPressed(sender, e);
            else
            {
                SaveUndoBlock();
                UndoQueue.ActionPeformed(new UndoableTextAction(TextBoxUndoType.AddText, SelectionStart, "\r\n", 1, this));
                SaveUndoBlock();
            }
        }

        /// <summary>
        /// Fires when any standard letter or digit is about to be added to the undo builder. You must implement your own
        /// undo building of letters if this event is used.
        /// </summary>
        public event KeyEventHandler LetterTyped;

        /// <summary>
        /// Fires when any standard letter or digit is about to be added to the undo builder. You must implement your own
        /// undo building of letters if this event is used.
        /// </summary>
        /// <param name="sender">The sending control</param>
        /// <param name="e">Basic event args</param>
        /// <param name="typed">The character typed/.</param>
        private void OnLetterTyped(object sender, KeyEventArgs e, char typed)
        {
            if (LetterTyped != null)
                LetterTyped(sender, e);
            else
            {
                AddUndoText(typed);
            }
        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// Applies the red colouring to the brackets the cursor is currently on.
        /// </summary>
        protected void ColourSelectedBrackets()
        {
            if (CurrentBrackets[0] != -1)
            {
                int topline = TopLine;
                ColourText(CurrentBrackets[0], 1, Color.Red);
                ColourText(CurrentBrackets[1], 1, Color.Red);
                ScrollToIndex(topline);
            }
        }

        /// <summary>
        /// Removes the red colouring to the brackets the cursor is currently on.
        /// </summary>
        protected void DeColourSelectedBrackets()
        {
            if (CurrentBrackets[0] != -1)
            {
                int topline = TopLine;
                ColourText(CurrentBrackets[0], 1, Color.Black);
                ColourText(CurrentBrackets[1], 1, Color.Black);
                CurrentBrackets[0] = CurrentBrackets[1] = -1;
                ScrollToIndex(topline);
            }
        }

        /// <summary>
        /// Scrolls the text box to the selected line.
        /// </summary>
        /// <param name="index">The line index of the line to scroll to.</param>
        public void ScrollToIndex(int index)
        {
            int ScrollAmount = index - TopLine;
            EnableUpdating(false);
            SendMessage(Handle, (uint)SendMessageValues.EM_LINESCROLL, 0, ScrollAmount);
            UpdateSavedSelection();
            EnableUpdating(true);
            ClearSavedSelection();
        }

        /// <summary>
        /// Finds the character index of the associated bracket to the index
        /// of the one passed in.
        /// </summary>
        /// <param name="location">The global character index of the bracket.</param>
        /// <returns>The global character index of the bracket if found, or -1 if not found or an invalid index
        /// is passed in.</returns>
        public int FindAttachedBracket(int location)
        {
            int Offset = 0;

            if (Text[location] == '{')
            {
                Regex Brackets = new Regex("{|}");
                foreach (Match bracket in Brackets.Matches(Text, location + 1))
                {
                    if (bracket.Value == "{")
                        Offset++;
                    else if (bracket.Value == "}")
                    {
                        if (Offset == 0)
                            return bracket.Index;
                        else
                            Offset--;
                    }
                }
                return -1;
            }
            else if (Text[location] == '}')
            {
                Regex Brackets = new Regex("{|}", RegexOptions.RightToLeft);
                foreach (Match bracket in Brackets.Matches(Text, location))
                    if (bracket.Value == "}")
                        Offset--;
                    else if (bracket.Value == "{")
                        if (Offset == 0)
                            return bracket.Index;
                        else
                            Offset++;
                return -1;
            }
            else
                throw new Exception("The character at this location is not a bracket.");
        }

        /// <summary>
        /// Selects the given range of text.
        /// </summary>
        /// <param name="start">The point on which to start the selection.</param>
        /// <param name="length">The length of the new selection.</param>
        /// <param name="forsearch">Whether the selection is from a search.</param>
        public void Select(int start, int length, bool forsearch = false)
        {
            SelectWasManual = false;
            base.Select(start, length);
            SelectWasManual = true;
            if (forsearch)
                SearchSelection = true;
        }

        /// <summary>
        /// Restarts the current undo block by clearing the current string.
        /// </summary>
        public void RestartUndoBlock()
        {
            UndoBuild.Clear();
            UndoPosition = -1;
            UndoLength = 0;
        }

        /// <summary>
        /// Add the current undo block to the undo queue and restart the undo block.
        /// </summary>
        /// <returns>A bool determining if a value was added to the undo queue</returns>
        public bool SaveUndoBlock()
        {
            if (UndoBuild.Length > 0 && UndoPosition != -1)
            {
                bool Return = UndoQueue.ActionPeformed(new UndoableTextAction(UndoType, UndoPosition, UndoBuild.ToString(), UndoLength, this));
                RestartUndoBlock();
                return Return;
            }
            return false;
        }

        /// <summary>
        /// Returns the line index of the current line.
        /// </summary>
        /// <returns>The line index of the current line</returns>
        public int GetCurrentLineIndex()
        {
            return GetLineFromCharIndex(GetFirstCharIndexOfCurrentLine());
        }

        /// <summary>
        /// Determines if the character at the given index is the first character of a line.
        /// </summary>
        /// <returns>The character index to check.</returns>
        public bool IsCharacterFirst(int index)
        {
            return GetFirstCharIndexFromCharIndex(index) == index;
        }

        /// <summary>
        /// Returns the first character index on the same line as the index 
        /// passed in.
        /// </summary>
        /// <param name="index">The global character index from which to find the line start.</param>
        /// <returns>The global character index for the start of the line.</returns>
        public int GetFirstCharIndexFromCharIndex(int index)
        {
            return GetFirstCharIndexFromLine(GetLineFromCharIndex(index));
        }

        /// <summary>
        /// Searches using the given options inside the text box, and returns an array of results.
        /// </summary>
        /// <param name="options">The options with which to search.</param>
        /// <returns>An array of TextBoxSearchResults each containing one term that fits the options.</returns>
        public TextBoxSearchResult[] Find(FindOptions options)
        {
            List<TextBoxSearchResult> Results = new List<TextBoxSearchResult>();
            string SearchIn = options.InSelection ? SelectedText : Text;

            if (options.All)
                foreach (Match match in options.CompiledRegex.Matches(SearchIn, SelectionStart))
                    Results.Add(new TextBoxSearchResult(this, match, options.InSelection));
            else
            {
                Match match;
                if (options.InSelection)
                    match = options.CompiledRegex.Match(SearchIn);
                else if (SearchSelection && options.Direction == SearchDirection.Down)
                    match = options.CompiledRegex.Match(SearchIn, SelectionStart + SelectionLength);
                else
                    match = options.CompiledRegex.Match(SearchIn, SelectionStart);

                if (match.Success)
                    Results.Add(new TextBoxSearchResult(this, match, options.InSelection));
            }
            return Results.ToArray();
        }

        /// <summary>
        /// Enables or disables redrawing of the text box. Used for hiding programatic control of the text box
        /// or batch adding or removing.
        /// </summary>
        /// <param name="Enable"></param>
        public void EnableUpdating(bool Enable)
        {
            int EnableInt = Enable ? 1 : 0;
            SendMessage(Handle, 0x000B, EnableInt, 0);
            if (Enable)
            {
                Invalidate();
            }
        }

        /// <summary>
        /// Appends a new line to the end of the text box.
        /// </summary>
        /// <param name="text">The text to append as a line.</param>
        public void AppendLine()
        {
            AppendText("\r\n");
        }

        /// <summary>
        /// Appends the text with an added line break to the end of the text box.
        /// </summary>
        /// <param name="text">The text to append as a line.</param>
        public void AppendLine(string text)
        {
            AppendText(text + "\r\n");
        }

        /// <summary>
        /// Appends the text with an added line break to the end of the text box with the given colour.
        /// </summary>
        /// <param name="text">The text to append as a line.</param>
        /// <param name="colour">The colour to give the appended line.</param>
        public void AppendLine(string text, Color colour)
        {
            AppendText(text + "\r\n", colour);
        }

        /// <summary>
        /// Appends the text to the end of the text box with the given colour.
        /// </summary>
        /// <param name="text">The text to append.</param>
        /// <param name="colour">The colour of the appended text.</param>
        public void AppendText(string text, Color colour)
        {
            Color CurrentColour = SelectionColor;
            SelectionColor = colour;
            AppendText(text);
            SelectionColor = CurrentColour;
        }

        /// <summary>
        /// Appends the given rich text value to the end of the text box.
        /// </summary>
        /// <param name="text">The rich text to append.</param>
        public void AppendRichText(string Text)
        {
            Rtf += Text;
        }

        /// <summary>
        /// Appends the given rich text value with an added line break to the end of the text box.
        /// </summary>
        /// <param name="text">The rich text to append as a line.</param>
        public void AppendRichTextLine(string Text)
        {
            Rtf += Text + "\r\n";
        }

        /// <summary>
        /// Colours the given range of text in the given colour.
        /// </summary>
        /// <param name="start">The start index of the area to colour.</param>
        /// <param name="length">The length of the area to colour.</param>
        /// <param name="colour">The colour to assign to the text.</param>
        public void ColourText(int start, int length, Color colour)
        {
            EnableUpdating(false);
            UpdateSavedSelection();
            Color CurrentColour = SelectionColor;

            Select(start, length);
            SelectionColor = colour;

            Select(SavedSelectionStart.Value, SavedSelectionLength.Value);
            ClearSavedSelection();

            SelectionColor = CurrentColour;
            EnableUpdating(true);
        }

        /// <summary>
        /// Saves the current selection.
        /// </summary>
        private void UpdateSavedSelection()
        {
            SavedSelectionStart = SavedSelectionStart.HasValue ? SavedSelectionStart.Value : SelectionStart;
            SavedSelectionLength = SavedSelectionLength.HasValue ? SavedSelectionLength.Value : SelectionLength;
        }

        /// <summary>
        /// Clears the current saved selection.
        /// </summary>
        private void ClearSavedSelection()
        {
            SavedSelectionLength = null;
            SavedSelectionStart = null;
        }

        /// <summary>
        /// Adds the given value to the undo build.
        /// </summary>
        /// <param name="value">A dynamic value that will be added.</param>
        public void AddUndoText(dynamic value)
        {
            string append = Convert.ToString(value);
            UndoBuild.Append(append);
            UndoLength += append.Length;
        }

        /// <summary>
        /// Adds the given value to the undo build.
        /// </summary>
        /// <param name="value">A dynamic value that will be added.</param>
        public void AddUndoText(string value)
        {
            string append = Convert.ToString(value);
            UndoBuild.Append(append);
            UndoLength += append.Length;
        }

        /// <summary>
        /// Adds the given value to the undo build with a specified length.
        /// </summary>
        /// <param name="value">A dynamic value that will be added.</param>
        /// <param name="length">The length of the value.</param>
        public void AddUndoText(dynamic value, int length)
        {
            UndoBuild.Append(value);
            UndoLength += length;
        }

        /// <summary>
        /// Adds the given value to the undo build with a specified length.
        /// </summary>
        /// <param name="value">A dynamic value that will be added.</param>
        /// <param name="length">The length of the value.</param>
        public void AddUndoText(string value, int length, bool atstart = false)
        {
            string NewString;
            if (atstart)
                NewString = RichText.CombineRichText(value, UndoBuild.ToString());
            else
                NewString = RichText.CombineRichText(UndoBuild.ToString(), value);

            UndoBuild = new StringBuilder(NewString);
            UndoLength += length;
        }

        /// <summary>
        /// Peforms an undo if the text box is capable.
        /// </summary>
        new public void Undo()
        {
            SaveUndoBlock();
            if (CanUndo)
                UndoQueue.Undo();
        }

        /// <summary>
        /// Peforms a redo if the text box is capable.
        /// </summary>
        new public void Redo()
        {
            SaveUndoBlock();
            if (CanRedo)
                UndoQueue.Redo();
        }

        /// <summary>
        /// Returns a character from the Keys enumeration.
        /// </summary>
        /// <param name="KeyData">A keys enumeration value from which to find the character.</param>
        /// <returns>A character representation of the keys pressed.</returns>
        public static char GetCharFromKeys(Keys KeyData)
        {
            char Return = (char)MapVirtualKey((uint)KeyData, 2);
            if (KeyData.HasFlag(Keys.Shift) == false)
                Return = char.ToLower(Return);
            return Return;
        }

        #endregion

        #region Imports

        [DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, long msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int msg, int wParam, ref CharRange lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int msg, int wParam, ref ParagraphFormat lParam);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hWnd">Handle to the destination window.</param>
        /// <param name="Msg">Message to be sent.</param>
        /// <param name="wParam">First message parameter.</param>
        /// <param name="lParam">Second message parameter.</param>
        /// <returns>Specifies the result of the message processing; it depends on the message sent.</returns>
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        static extern int MapVirtualKey(uint uCode, uint uMapType);

        #endregion
    }
}
