﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.Controls
{
    public enum SyntaxType
    {
        Booleans,
        KeyWords,
        Strings,
        Numbers,
        Brackets,
        Comments
    }
}
