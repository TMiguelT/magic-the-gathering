﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace MiguelUtil.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.IO;
    using System.Windows.Forms;
    using System.ComponentModel;
    using System.Text.RegularExpressions;
    using System.Drawing;

    public partial class SyntaxHighlightTextBox : RichTextBoxEx
    {
        SyntaxHighlight _keywords;
        SyntaxHighlight _strings;
        SyntaxHighlight _numbers;
        SyntaxHighlight _brackets;
        SyntaxHighlight _bools;
        SyntaxHighlightList AllRegex;

        public Regex KeyWords
        {
            get
            {
                return _keywords.RegularExpression;
            }
            set
            {
                _keywords.RegularExpression = value;
            }
        }

        public Regex Strings
        {
            get
            {
                return _strings.RegularExpression;
            }
            set
            {
                _strings.RegularExpression = value;
            }
        }

        public Regex Numbers
        {
            get
            {
                return _numbers.RegularExpression;
            }
            set
            {
                _numbers.RegularExpression = value;
            }
        }

        public Regex Brackets
        {
            get
            {
                return _brackets.RegularExpression;
            }
            set
            {
                _brackets.RegularExpression = value;
            }
        }

        public Regex Bools
        {
            get
            {
                return _bools.RegularExpression;
            }
            set
            {
                _bools.RegularExpression = value;
            }
        }

        int BeforePasteLine = -1;

        bool Process = true;
        private bool _EnableHighlighting;

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
                ClearUndo();
            }
        }

        public bool EnableHighlighting
        {
            get
            {
                return _EnableHighlighting;
            }

            set
            {
                _EnableHighlighting = value;
                if (value)
                {
                    //Deal with this in a per program basis
                }
                else
                {
                    ColourText(0, Text.Length, Color.Black);
                }
            }
        }

        public override string SelectedText
        {
            get
            {
                return base.SelectedText;
            }
            set
            {
                if (Process)
                {
                    int Selection = GetLineFromCharIndex(SelectionStart);
                    base.SelectedText = value;
                    for (int i = Selection; i < GetCurrentLineIndex(); i++)
                    {
                        ProcessLine(i);
                    }
                }
                else
                {
                    base.SelectedText = value;
                }
            }
        }

        public SyntaxHighlightTextBox(bool highlightsyntax = true, params SyntaxHighlight[] highlightlist)
        {
            AllRegex = new SyntaxHighlightList();
            foreach (SyntaxHighlight highlight in highlightlist)
            {
                if (highlight.RegularExpression.ToString() != "")
                {
                    switch (highlight.Type)
                    {
                        case SyntaxType.Booleans:
                            _bools = highlight;
                            AllRegex.Add(_bools);
                            break;
                        case SyntaxType.KeyWords:
                            _keywords = highlight;
                            AllRegex.Add(_keywords);
                            break;
                        case SyntaxType.Strings:
                            _strings = highlight;
                            AllRegex.Add(_strings);
                            break;
                        case SyntaxType.Numbers:
                            _numbers = highlight;
                            AllRegex.Add(_numbers);
                            break;
                        case SyntaxType.Brackets:
                            _brackets = highlight;
                            AllRegex.Add(_brackets);
                            break;
                    }
                }
            }

            EnableHighlighting = highlightsyntax;
        }

        public SyntaxHighlightTextBox()
        {
            EnableHighlighting = false;
        }

        protected override bool OnBeforePaste(HandledEventArgs e)
        {
            BeforePasteLine = GetLineFromCharIndex(SelectionStart);
            base.OnBeforePaste(e);
            return e.Handled;
        }

        protected override void OnTextChanged(EventArgs e)
        {
            if (Process && BeforePasteLine != -1)
            {
                int AfterPasteLine = GetCurrentLineIndex();
                if (AfterPasteLine > BeforePasteLine)
                {
                    for (int i = BeforePasteLine; i < AfterPasteLine; i++)
                    {
                        ProcessLine(i);
                    }
                }
                BeforePasteLine = -1;
            }
            base.OnTextChanged(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Control)
            {
                if (e.KeyCode == Keys.Z)
                    Process = false;
                else if (e.KeyCode == Keys.R || e.KeyCode == Keys.L || e.KeyCode == Keys.J || e.KeyCode == Keys.E)
                {
                    e.Handled = true;
                }
            }
            else if (SelectionLength == 0)
            { 
                int LastStart = SelectionStart;
                int LastLength = SelectionLength;

                if (TextLength > SelectionStart)
                {
                    SelectionColor = Color.Black;
                    SelectionLength = 1;
                    if (SelectionColor == AllRegex[SyntaxType.Strings].Colour && SelectedText.Trim().Length > 0)
                    {
                        Select(LastStart, LastLength);
                        SelectionColor = AllRegex[SyntaxType.Strings].Colour;
                        goto End;
                    }
                }

                if (SelectionStart > 0)
                {
                    SelectionStart--;
                    SelectionLength = 1;
                    if (SelectionColor == AllRegex[SyntaxType.Numbers].Colour && char.IsNumber(GetCharFromKeys(e.KeyData)))
                    {
                        Select(LastStart, LastLength);
                        SelectionColor = AllRegex[SyntaxType.Numbers].Colour;
                        goto End;
                    }

                    Select(LastStart, LastLength);
                    SelectionColor = Color.Black;
                }
            }

        End: base.OnKeyDown(e);
        Process = true;
        }

        new public void EnableUpdating(bool Enable)
        {
            Process = Enable;
            base.EnableUpdating(Enable);
        }

        /// <summary>
        /// Don't use this method, it's horribly inefficient.
        /// </summary>
        public void ProcessAllLines()
        {
            if (Lines.Length > 0)
            {
                EnableUpdating(false);
                int LastSelectionStart = SelectionStart;
                int LastSelectionLength = SelectionLength;

                // Set all other text to black
                EnableUpdating(false);
                Select(0, TextLength);
                SelectionColor = Color.Black;

                // Process Syntax
                foreach (KeyValuePair<SyntaxType, SyntaxHighlight> Type in AllRegex)
                    foreach (Match SyntaxMatch in Type.Value.RegularExpression.Matches(Text))
                    {
                        Select(SyntaxMatch.Index, SyntaxMatch.Length);
                        SelectionStart = SyntaxMatch.Index;
                        SelectionLength = SyntaxMatch.Length;
                        SelectionColor = Type.Value.Colour;
                    }

                Select(LastSelectionStart, LastSelectionLength);
                SelectionColor = Color.Black;
                EnableUpdating(true);
                Invalidate();
            }
        }

        public void ProcessLine(int LineIndex, bool ProcessingAll = false)
        {
            int LastSelectionStart = SelectionStart;
            int LastSelectionLength = SelectionLength;

            int CharIndex = GetFirstCharIndexFromLine(LineIndex);
            int Top = TopLine;

            if (Lines.Length > 0)
            {
                // Set all other text to black
                EnableUpdating(false);
                Select(CharIndex, Lines[LineIndex].Length);
                SelectionColor = Color.Black;

                // Process Syntax
                foreach (KeyValuePair<SyntaxType, SyntaxHighlight> Type in AllRegex)
                    foreach (Match SyntaxMatch in Type.Value.RegularExpression.Matches(Lines[LineIndex]))
                    {
                        Select(SyntaxMatch.Index + CharIndex, SyntaxMatch.Length);
                        SelectionStart = SyntaxMatch.Index + CharIndex;
                        SelectionLength = SyntaxMatch.Length;
                        SelectionColor = Type.Value.Colour;
                    }

                Select(LastSelectionStart, LastSelectionLength);
                SelectionColor = Color.Black;
                EnableUpdating(true);
            }
            ScrollToIndex(Top);
        }

        public void AppendText(string text, SyntaxType type)
        {
            if (EnableHighlighting)
                AppendText(text, AllRegex[type].Colour);
            else
                AppendText(text);
        }

        public void AppendLine(string text, SyntaxType type)
        {
            if (EnableHighlighting)
                AppendLine(text, AllRegex[type].Colour);
            else
                AppendLine(text);
        }

        new public void ColourText(int start, int length, Color colour)
        {
            Process = false;
            base.ColourText(start, length, colour);
            Process = true;
        }

        protected override void OnSelectionChanged(SelectionEventArgs e)
        {
            if (SelectionLength == 0 && e.Manual)
            {
                if (EnableHighlighting && Process && e.KeyTyped == false)
                    ProcessLine(GetLineFromCharIndex(LastSelectionStart));
            }
            base.OnSelectionChanged(e);
        }
    }
}
