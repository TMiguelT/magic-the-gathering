﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.Controls
{
    class SyntaxHighlightList : Dictionary<SyntaxType, SyntaxHighlight>
    {
        public void Add(SyntaxHighlight element)
        {
            Add(element.Type, element);
        }
    }
}
