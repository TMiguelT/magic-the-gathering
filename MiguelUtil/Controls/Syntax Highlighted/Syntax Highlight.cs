﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;

namespace MiguelUtil.Controls
{
    public class SyntaxHighlight
    {
        public Regex RegularExpression;
        public Color Colour;
        public SyntaxType Type;

        public SyntaxHighlight(SyntaxType type, Regex regex, Color colour)
        {
            Type = type;
            RegularExpression = regex;
            Colour = colour;
        }

        public override string ToString()
        {
            return Type.ToString();
        }
    }
}
