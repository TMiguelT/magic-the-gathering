﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.Controls.Undoable
{
    public interface IUndoableControl
    {
        UndoableActionList UndoQueue { get; }
    }
}
