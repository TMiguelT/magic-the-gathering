﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MiguelUtil.Controls.Undoable
{
    public interface IUndoableAction
    {
        void Undo();

        void Redo();
    }
}
