﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.Controls.Undoable
{
    public enum KeyType
    {
        Tab,
        Key,
        Enter,
        Backspace,
        Delete,
        None
    }
}
