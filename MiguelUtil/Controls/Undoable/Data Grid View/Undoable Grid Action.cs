﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MiguelUtil.Controls.Undoable.Data_Grid_View;

namespace MiguelUtil.Controls.Undoable.Grid
{
    public class UndoableGridAction : IUndoableAction
    {
        public GridUndoType Type;
        public RowInfo[] Rows;
        public DataGridView Owner;

        public UndoableGridAction(DataGridView owner, GridUndoType type, RowInfo[] rows)
        {
            Type = type;
            Rows = rows;
            Owner = owner;
        }

        public UndoableGridAction(DataGridView owner, GridUndoType type, DataGridViewRowCollection rows)
        {
            Type = type;
            Rows = RowInfo.ConvertToRowInfo(rows);
            Owner = owner;
        }

        public UndoableGridAction(DataGridView owner, GridUndoType type, DataGridViewSelectedRowCollection rows)
        {
            Type = type;
            Rows = RowInfo.ConvertToRowInfo(rows);
            Owner = owner;
        }

        public UndoableGridAction(DataGridView owner)
        {
            Owner = owner;
        }

        public void Undo()
        {
            switch (Type)
            {
                case GridUndoType.AddRow:
                    UndoAdd();
                    break;

                case GridUndoType.ChangeValue:
                    UndoChangeValue();
                    break;

                case GridUndoType.DeleteRow:
                    UndoDelete();
                    break;

                case GridUndoType.MoveRow:
                    UndoMove();
                    break;

                case GridUndoType.PasteRow:
                    UndoPaste();
                    break;
            }
        }

        virtual protected void UndoAdd()
        {
            foreach (RowInfo Node in Rows)
            {
                Owner.Rows.Remove(Node.Row);
            }
        }

        virtual protected void UndoChangeValue()
        {
            for (int i = 0; i < Rows.Length; i++)
            {
                RowInfo Info = Rows[i];
                object[] NewValues = ControlMethods.GetValues(Info.Row);
                Info.Row.SetValues(Info.LastInfo as object[]);
                Info.LastInfo = NewValues;
            }
        }

        virtual protected void UndoDelete()
        {
            foreach (RowInfo Node in Rows)
            {
                Owner.Rows.Insert((int)Node.LastInfo, Node.Values);
            }
        }

        virtual protected void UndoMove()
        {
            for (int i = 0; i < Rows.Length; i++)
            {
                RowInfo Row = Rows[i];

                int NewIndex = Row.Row.Index;
                Owner.Rows.Remove(Row.Row);
                Owner.Rows.Insert((int)Row.LastInfo, Row.Row);
                Row.LastInfo = NewIndex;
            }
        }

        virtual protected void UndoPaste()
        {
            foreach (RowInfo Node in Rows)
            {
                Owner.Rows.Remove(Node.Row);
            }
        }

        public void Redo()
        {
            switch (Type)
            {
                case GridUndoType.AddRow:
                    RedoAdd();
                    break;

                case GridUndoType.ChangeValue:
                    RedoChangeValue();
                    break;

                case GridUndoType.DeleteRow:
                    RedoDelete();
                    break;

                case GridUndoType.MoveRow:
                    RedoMove();
                    break;

                case GridUndoType.PasteRow:
                    RedoPaste();
                    break;
            }
        }

        virtual protected void RedoAdd()
        {
            foreach (RowInfo Node in Rows)
            {
                Owner.Rows.Insert((int)Node.LastInfo, Node.Values);
            }
        }

        virtual protected void RedoChangeValue()
        {
            for (int i = 0; i < Rows.Length; i++)
            {
                RowInfo Info = Rows[i];
                object[] OldValues = ControlMethods.GetValues(Info.Row);
                Info.Row.SetValues(Info.LastInfo as object[]);
                Info.LastInfo = OldValues;
            }
        }

        virtual protected void RedoDelete()
        {
            foreach (RowInfo Node in Rows)
            {
                Owner.Rows.Remove(Node.Row);
            }
        }

        virtual protected void RedoMove()
        {
            for (int i = 0; i < Rows.Length; i++)
            {
                RowInfo Row = Rows[i];

                int OldIndex = Row.Row.Index;
                Owner.Rows.Remove(Row.Row);
                Owner.Rows.Insert((int)Row.LastInfo, Row.Row);
                Row.LastInfo = OldIndex;
            }
        }

        virtual protected void RedoPaste()
        {
            foreach (RowInfo Node in Rows)
            {
                Owner.Rows.Insert((int)Node.LastInfo, Node.Values);
            }
        }
    }
}
