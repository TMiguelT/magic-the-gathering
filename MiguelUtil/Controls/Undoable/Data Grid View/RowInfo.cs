﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MiguelUtil.Controls.Undoable.Data_Grid_View
{
    //public struct RowInfo
    //{
    //    object[] Values;
    //    int LastLocation;
    //    int NewLocation;

    //    public RowInfo(object[] values, int lastlocation, int newlocation)
    //    {
    //        Values = values;
    //        LastLocation = lastlocation;
    //        NewLocation = lastlocation;
    //    }

    //    public RowInfo(DataGridViewRow row, int lastlocation)
    //    {
    //        Values = ControlMethods.GetValues(row);
    //        LastLocation = lastlocation;
    //        NewLocation = row.Index;
    //    }
    //}

    public struct RowInfo
    {
        public DataGridViewRow Row;
        public object[] Values;
        public object LastInfo;

        public RowInfo(DataGridViewRow row, object lastinfo)
        {
            Row = row;
            LastInfo = lastinfo;
            Values = ControlMethods.GetValues(row);
        }

        public RowInfo(DataGridViewRow row)
        {
            Row = row;
            LastInfo = row.Index;
            Values = ControlMethods.GetValues(row);
        }

        public static RowInfo[] ConvertToRowInfo(DataGridViewRowCollection convert)
        {
            var Rows = from DataGridViewRow row in convert select new RowInfo(row);
            return Rows.ToArray();
        }

        public static RowInfo[] ConvertToRowInfo(DataGridViewSelectedRowCollection convert)
        {
            var Rows = from DataGridViewRow row in convert select new RowInfo(row);
            return Rows.ToArray();
        }

        public static RowInfo[] ConvertToRowInfo(DataGridViewRowCollection convert, int Subtractor)
        {
            var Rows = from DataGridViewRow row in convert select new RowInfo(row, row.Index - Subtractor);
            return Rows.ToArray();
        }

        public static RowInfo[] ConvertToRowInfo(DataGridViewSelectedRowCollection convert, int Subtractor)
        {
            var Rows = from DataGridViewRow row in convert select new RowInfo(row, row.Index - Subtractor);
            return Rows.ToArray();
        }
    }
}
