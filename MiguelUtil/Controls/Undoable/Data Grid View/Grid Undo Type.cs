﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.Controls.Undoable.Grid
{
    public enum GridUndoType
    {
        PasteRow,
        DeleteRow,
        AddRow,
        MoveRow,
        ChangeValue
    }
}
