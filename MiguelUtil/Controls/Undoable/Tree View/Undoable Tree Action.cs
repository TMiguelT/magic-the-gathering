﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MiguelUtil.Controls.Undoable.Tree
{
    public class UndoableTreeAction : IUndoableAction
    {
        public TreeUndoType Type;
        public TreeNode[] Nodes;

        /// <summary>
        /// Will be a TreeNodeCollection for most events, but RBFNode[] for change value events.
        /// </summary>
        public object Value;

        /// <summary>
        /// Will be a TreeNodeCollection determining the previous position of the node before it was moved.
        /// </summary>
        public object Value2;

        public UndoableTreeAction(TreeUndoType type, TreeNode[] nodes, object value, object value2 = null)
        {
            Type = type;
            Nodes = nodes;
            Value = value;
            Value2 = value2;
        }

        public UndoableTreeAction()
        {

        }

        public void Undo()
        {
            switch (Type)
            {
                case TreeUndoType.AddNode:
                    UndoAdd();
                    break;

                case TreeUndoType.ChangeValue:
                    UndoChangeValue();
                    break;

                case TreeUndoType.DeleteNode:
                    UndoDelete();
                    break;

                case TreeUndoType.MoveNode:
                    UndoMove();
                    break;
            }
        }

        virtual protected void UndoAdd()
        {
            foreach (TreeNode Node in Nodes)
            {
                Node.TreeView.Nodes.Remove(Node);
            }
        }

        virtual protected void UndoChangeValue()
        {
            string[] Values = (string[])Value;
            string[] NewValues = new string[Values.Length];
            for (int i = 0; i < Values.Length; i++)
            {
                NewValues[i] = (string)Nodes[i].Text;
                Nodes[i].Text = Values[i];
            }
            Value = NewValues;
        }

        virtual protected void UndoDelete()
        {
            TreeNodeCollection DeleteTarget = (TreeNodeCollection)Value;
            foreach (TreeNode Node in Nodes)
            {
                DeleteTarget.Insert(Node.Index, Node);
            }
        }

        virtual protected void UndoMove()
        {
            TreeNodeCollection OldPosition = (TreeNodeCollection)Value2;
            foreach (TreeNode Node in Nodes)
            {
                Node.TreeView.Nodes.Remove(Node);
                OldPosition.Add(Node);
            }
        }

        public void Redo()
        {
            switch (Type)
            {
                case TreeUndoType.AddNode:
                    RedoAdd();
                    break;

                case TreeUndoType.ChangeValue:
                    RedoChangeValue();
                    break;

                case TreeUndoType.DeleteNode:
                    RedoDelete();
                    break;

                case TreeUndoType.MoveNode:
                    RedoMove();
                    break;
            }
        }

        virtual protected void RedoAdd()
        {
            TreeNodeCollection AddTarget = (TreeNodeCollection)Value;
            foreach (TreeNode Node in Nodes)
            {
                AddTarget.Insert(Node.Index, Node);
            }
        }

        virtual protected void RedoChangeValue()
        {
            string[] Values = (string[])Value;
            string[] OldValues = new string[Values.Length];
            for (int i = 0; i < Values.Length; i++)
            {
                OldValues[i] = (string)Nodes[i].Text;
                Nodes[i].Text = Values[i];
            }
            Value = OldValues;
        }

        virtual protected void RedoDelete()
        {
            TreeNodeCollection DeleteTarget = (TreeNodeCollection)Value;
            foreach (TreeNode Node in Nodes)
            {
                DeleteTarget.Remove(Node);
            }
        }

        virtual protected void RedoMove()
        {
            TreeNodeCollection NewPosition = (TreeNodeCollection)Value;
            foreach (TreeNode Node in Nodes)
            {
                Node.TreeView.Nodes.Remove(Node);
                NewPosition.Add(Node);
            }
        }
    }
}
