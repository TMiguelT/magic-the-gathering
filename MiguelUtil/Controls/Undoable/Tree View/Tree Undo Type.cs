﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.Controls.Undoable.Tree
{
    public enum TreeUndoType
    {
        DeleteNode,
        AddNode,
        MoveNode,
        ChangeValue
    }
}
