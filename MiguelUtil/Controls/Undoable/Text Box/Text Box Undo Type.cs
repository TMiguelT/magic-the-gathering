﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.Controls.Undoable.Text
{
    public enum TextBoxUndoType
    {
        AddText,
        BackspaceText,
        DeleteText,
        None
    }
}
