﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MiguelUtil.Controls
{
    public class ChangedEventArgs : EventArgs
    {
        public bool Changed;

        public ChangedEventArgs(bool changed)
        {
            Changed = changed;
        }
    }
}
