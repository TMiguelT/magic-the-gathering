﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MiguelUtil.Controls.Undoable.Text
{
    public class UndoableTextAction : IUndoableAction
    {
        public TextBoxBase Control;
        public TextBoxUndoType Type;
        public TextBoxUndoSection Data;

        public UndoableTextAction(TextBoxUndoType type, TextBoxUndoSection data, TextBoxBase control)
        {
            Type = type;
            Data = data;
            Control = control;
        }

        public UndoableTextAction(TextBoxUndoType type, int position, string text, TextBoxBase control)
        {
            Type = type;
            Data = new TextBoxUndoSection(text, position);
            Control = control;
        }

        public UndoableTextAction(TextBoxUndoType type, int position, string text, int length, TextBoxBase control)
        {
            Type = type;
            Data = new TextBoxUndoSection(text, position, length);
            Control = control;
        }

        public UndoableTextAction() { }

        public void Undo()
        {
            switch (Type)
            {
                case TextBoxUndoType.AddText:
                    UndoAdd();
                    break;

                case TextBoxUndoType.DeleteText:
                    UndoDelete();
                    break;

                case TextBoxUndoType.BackspaceText:
                    UndoBackSpace();
                    break;
            }
        }

        virtual protected void UndoAdd()
        {
            int Start = Control.SelectionStart;
            int Length = Control.SelectionLength;

            Control.Select(Data.Index, Data.Length);
            Control.SelectedText = "";

            Control.Select(Data.Index, 0);
        }

        virtual protected void UndoBackSpace()
        {
            int Start = Control.SelectionStart;
            int Length = Control.SelectionLength;

            Control.Select(Data.Index - Data.Length, 0);
            if (Control is RichTextBox)
                ((RichTextBox)Control).SelectedRtf = Data.Text;
            else
                Control.SelectedText = Data.Text;

            Control.Select(Start, Length);
        }

        virtual protected void UndoDelete()
        {
            int Start = Control.SelectionStart;
            int Length = Control.SelectionLength;

            Control.Select(Data.Index, 0);
            if (Control is RichTextBox)
                ((RichTextBox)Control).SelectedRtf = Data.Text;
            else
                Control.SelectedText = Data.Text;

            Control.Select(Start, Length);
        }

        public void Redo()
        {
            switch (Type)
            {
                case TextBoxUndoType.AddText:
                    RedoAdd();
                    break;

                case TextBoxUndoType.DeleteText:
                    RedoDelete();
                    break;

                case TextBoxUndoType.BackspaceText:
                    RedoBackSpace();
                    break;
            }
        }

        virtual protected void RedoAdd()
        {
            int Start = Control.SelectionStart;
            int Length = Control.SelectionLength;

            Control.Select(Data.Index, 0);
            //if (Control is RichTextBox)
            //    Control.SelectedText = Data.Text;
            //else
            Control.SelectedText = Data.Text;

            Control.Select(Start, Length);
        }

        virtual protected void RedoDelete()
        {
            int Start = Control.SelectionStart;
            int Length = Control.SelectionLength;

            Control.Select(Data.Index, Data.Length);
            Control.SelectedText = "";

            Control.Select(Data.Index, 0);
        }

        virtual protected void RedoBackSpace()
        {
            int Start = Control.SelectionStart;
            int Length = Control.SelectionLength;

            Control.Select(Data.Index - 1, Data.Length);
            Control.SelectedText = "";

            Control.Select(Data.Index - Data.Length, 0);
        }

        public override string ToString()
        {
            return Data.Text;
        }
    }
}
