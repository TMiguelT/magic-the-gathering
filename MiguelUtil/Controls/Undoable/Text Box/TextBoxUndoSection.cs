﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.Controls.Undoable.Text
{
    public class TextBoxUndoSection
    {
        public string Text;
        public int Index;
        public int Length;

        public TextBoxUndoSection(string text, int index)
        {
            Text = text;
            Index = index;
            Length = text.Length;
        }

        public TextBoxUndoSection(string text, int index, int length)
        {
            Text = text;
            Index = index;
            Length = length;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
