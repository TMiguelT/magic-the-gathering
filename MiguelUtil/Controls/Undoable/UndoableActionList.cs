﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MiguelUtil.Controls.Undoable.Text;

namespace MiguelUtil.Controls.Undoable
{
    public class UndoableActionList
    {
        Stack<IUndoableAction> BeforeCurrent = new Stack<IUndoableAction>();
        Stack<IUndoableAction> AfterCurrent = new Stack<IUndoableAction>();

        private int OffsetFromSave = 0;
        public bool PermanentChange = false;

        public bool HasChanged
        {
            get
            {
                return OffsetFromSave != 0;
            }
        }

        public void ResetOffset()
        {
            OffsetFromSave = 0;
        }

        public void ClearUndo()
        {
            BeforeCurrent.Clear();
            AfterCurrent.Clear();
        }

        public bool CanUndo
        {
            get
            {
                return BeforeCurrent.Count > 0;
            }
        }

        public bool CanRedo
        {
            get
            {
                return AfterCurrent.Count > 0;
            }
        }

        public bool ActionPeformed(IUndoableAction action)
        {
            AfterCurrent.Clear();
            if (BeforeCurrent.Count > 0 && action.Equals(BeforeCurrent.Peek()))
                return HasChanged;
            BeforeCurrent.Push(action);
            OffsetFromSave++;
            return HasChanged;
        }

        public IUndoableAction NextUndo
        {
            get
            {
                return BeforeCurrent.Peek();
            }
        }

        public IUndoableAction NextRedo
        {
            get
            {
                return AfterCurrent.Peek();
            }
        }

        public bool Undo()
        {
            IUndoableAction Action = BeforeCurrent.Pop();
            AfterCurrent.Push(Action);
            Action.Undo();
            OffsetFromSave--;
            return HasChanged || PermanentChange;
        }

        public bool Redo()
        {
            IUndoableAction Action = AfterCurrent.Pop();
            BeforeCurrent.Push(Action);
            Action.Redo();
            OffsetFromSave++;
            return HasChanged || PermanentChange;
        }
    }
}
