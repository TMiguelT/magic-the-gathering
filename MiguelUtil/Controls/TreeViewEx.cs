﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MiguelUtil.Controls.Undoable;
using MiguelUtil.Controls.Undoable.Tree;
using MiguelUtil.Find;
using MiguelUtil.Find.ResultTypes;

namespace MiguelUtil.Controls
{
    public partial class TreeViewEx : TreeView, IUndoableControl
    {
        private UndoableActionList undoqueue = new UndoableActionList();

        public UndoableActionList UndoQueue
        {
            get
            {
                return undoqueue;
            }
            set
            {
                undoqueue = value;
            }
        }

        public bool AddToQueue(UndoableTreeAction action)
        {
            return UndoQueue.ActionPeformed(action);
        }

        public bool AddToQueue(TreeUndoType type, TreeNode[] nodes, object value, object value2 = null)
        {
            return UndoQueue.ActionPeformed(new UndoableTreeAction(type, nodes, value, value2));
        }

        public bool AddToQueue(TreeUndoType type, TreeNode node, object value, object value2 = null)
        {
            TreeNode[] Nodes = new TreeNode[] { node };
            return UndoQueue.ActionPeformed(new UndoableTreeAction(type, Nodes, value, value2));
        }

        public virtual TreeSearchResult[] Find(FindOptions options)
        {
            List<TreeSearchResult> Results = new List<TreeSearchResult>();
            foreach (TreeNode node in Nodes)
                FindInTree(node, Results, options);

            return Results.ToArray();
        }

        private void FindInTree(TreeNode FindIn, List<TreeSearchResult> AddTo, FindOptions options)
        {
            if (FindIn.Text.Contains(options.Find))
            {
                AddTo.Add(new TreeSearchResult(FindIn));
            }
            foreach (TreeNode SubNode in FindIn.Nodes)
                FindInTree(SubNode, AddTo, options);
        }
    }
}
