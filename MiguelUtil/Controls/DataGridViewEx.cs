﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MiguelUtil.Controls.Undoable;
using MiguelUtil.Controls.Undoable.Grid;
using MiguelUtil.Controls.Undoable.Data_Grid_View;

namespace MiguelUtil.Controls
{
    public class DataGridViewEx : DataGridView, IUndoableControl
    {
        private UndoableActionList undoqueue = new UndoableActionList();

        public UndoableActionList UndoQueue
        {
            get
            {
                return undoqueue;
            }
            set
            {
                undoqueue = value;
            }
        }

        public DataGridViewEx()
        {
            UndoQueue = new UndoableActionList();
        }

        public bool AddToQueue(UndoableGridAction action)
        {
            return UndoQueue.ActionPeformed(action);
        }

        public bool AddToQueue(GridUndoType type, RowInfo[] rows)
        {
            return UndoQueue.ActionPeformed(new UndoableGridAction(this, type, rows));
        }

        public bool AddToQueue(GridUndoType type, DataGridViewRowCollection rows)
        {
            return UndoQueue.ActionPeformed(new UndoableGridAction(this, type, RowInfo.ConvertToRowInfo(rows, rows.Count)));
        }

        public bool AddToQueue(GridUndoType type, DataGridViewSelectedRowCollection rows)
        {
            return UndoQueue.ActionPeformed(new UndoableGridAction(this, type, RowInfo.ConvertToRowInfo(rows, rows.Count)));
        }
    }
}
