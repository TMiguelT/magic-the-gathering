﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace MiguelUtil.Controls.APIClasses
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct CharRange
    {
        public int cpMin;
        public int cpMax;
    }
}
