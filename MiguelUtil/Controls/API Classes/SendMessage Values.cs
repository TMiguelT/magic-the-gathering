﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.Controls.API_Classes
{
    public static class SendMessageValues
    {
        public static readonly long WM_USER = 0x400L;
        public static readonly long CB_GETEDITSEL = 0x140;
        public static readonly long CB_LIMITTEXT = 0x141;
        public static readonly long CB_SETEDITSEL = 0x142;
        public static readonly long CB_ADDSTRING = 0x143;
        public static readonly long CB_DELETESTRING = 0x144;
        public static readonly long CB_DIR = 0x145;
        public static readonly long CB_GETCOUNT = 0x146;
        public static readonly long CB_GETCURSEL = 0x147;
        public static readonly long CB_GETLBTEXT = 0x148;
        public static readonly long CB_GETLBTEXTLEN = 0x149;
        public static readonly long CB_INSERTSTRING = 0x14a;
        public static readonly long CB_RESETCONTENT = 0x14b;
        public static readonly long CB_FINDSTRING = 0x14c;
        public static readonly long CB_SELECTSTRING = 0x14d;
        public static readonly long CB_SETCURSEL = 0x14e;
        public static readonly long CB_SHOWDROPDOWN = 0x14f;
        public static readonly long CB_GETITEMDATA = 0x150;
        public static readonly long CB_SETITEMDATA = 0x151;
        public static readonly long CB_GETDROPPEDCONTROLRECT = 0x152;
        public static readonly long CB_SETITEMHEIGHT = 0x153;
        public static readonly long CB_GETITEMHEIGHT = 0x154;
        public static readonly long CB_SETEXTENDEDUI = 0x155;
        public static readonly long CB_GETEXTENDEDUI = 0x156;
        public static readonly long CB_GETDROPPEDSTATE = 0x157;
        public static readonly long CB_FINDSTRINGEXACT = 0x158;
        public static readonly long CB_SETLOCALE = 0x159;
        public static readonly long CB_GETLOCALE = 0x15a;
        public static readonly long CB_GETTOPINDEX = 0x15b;
        public static readonly long CB_SETTOPINDEX = 0x15c;
        public static readonly long CB_GETHORIZONTALEXTENT = 0x15d;
        public static readonly long CB_SETHORIZONTALEXTENT = 0x15e;
        public static readonly long CB_GETDROPPEDWIDTH = 0x15f;
        public static readonly long CB_SETDROPPEDWIDTH = 0x160;
        public static readonly long CB_INITSTORAGE = 0x161;
        public static readonly long CB_MSGMAX = 0x162;
        public static readonly long EM_CANUNDO = 0xc6;
        public static readonly long EM_EMPTYUNDOBUFFER = 0xcd;
        public static readonly long EM_FMTLINES = 0xc8;
        public static readonly long EM_FORMATRANGE = WM_USER + 57;
        public static readonly long EM_GETFIRSTVISIBLELINE = 0xce;
        public static readonly long EM_GETHANDLE = 0xbd;
        public static readonly long EM_GETLINE = 0xc4;
        public static readonly long EM_GETLINECOUNT = 0xba;
        public static readonly long EM_GETMODIFY = 0xb8;
        public static readonly long EM_GETPASSWORDCHAR = 0xd2;
        public static readonly long EM_GETRECT = 0xb2;
        public static readonly long EM_GETSEL = 0xb0;
        public static readonly long EM_GETTHUMB = 0xbe;
        public static readonly long EM_GETWORDBREAKPROC = 0xd1;
        public static readonly long EM_LIMITTEXT = 0xc5;
        public static readonly long EM_LINEFROMCHAR = 0xc9;
        public static readonly long EM_LINEINDEX = 0xbb;
        public static readonly long EM_LINELENGTH = 0xc1;
        public static readonly long EM_LINESCROLL = 0xb6;
        public static readonly long EM_REPLACESEL = 0xc2;
        public static readonly long EM_SCROLL = 0xb5;
        public static readonly long EM_SCROLLCARET = 0xb7;
        public static readonly long EM_SETHANDLE = 0xbc;
        public static readonly long EM_SETMODIFY = 0xb9;
        public static readonly long EM_SETPASSWORDCHAR = 0xcc;
        public static readonly long EM_SETREADONLY = 0xcf;
        public static readonly long EM_SETRECT = 0xb3;
        public static readonly long EM_SETRECTNP = 0xb4;
        public static readonly long EM_SETSEL = 0xb1;
        public static readonly long EM_SETTABSTOPS = 0xcb;
        public static readonly long EM_SETTARGETDEVICE = WM_USER + 72;
        public static readonly long EM_SETWORDBREAKPROC = 0xd0;
        public static readonly long EM_UNDO = 0xc7;
        public static readonly long HDS_HOTTRACK = 0x4;
        public static readonly long HDI_BITMAP = 0x10;
        public static readonly long HDI_IMAGE = 0x20;
        public static readonly long HDI_ORDER = 0x80;
        public static readonly long HDI_FORMAT = 0x4;
        public static readonly long HDI_TEXT = 0x2;
        public static readonly long HDI_WIDTH = 0x1;
        public static readonly long HDI_HEIGHT = HDI_WIDTH;
        public static readonly long HDF_LEFT = 0;
        public static readonly long HDF_RIGHT = 1;
        public static readonly long HDF_IMAGE = 0x800;
        public static readonly long HDF_BITMAP_ON_RIGHT = 0x1000;
        public static readonly long HDF_BITMAP = 0x2000;
        public static readonly long HDF_STRING = 0x4000;
        public static readonly long HDM_FIRST = 0x1200;
        public static readonly long HDM_SETITEM = (HDM_FIRST + 4);
        public static readonly long LB_ADDFILE = 0x196;
        public static readonly long LB_ADDSTRING = 0x180;
        public static readonly long LB_CTLCODE = 0L;
        public static readonly long LB_DELETESTRING = 0x182;
        public static readonly long LB_DIR = 0x18d;
        public static readonly long LB_ERR = (-1);
        public static readonly long LB_ERRSPACE = (-2);
        public static readonly long LB_FINDSTRING = 0x18f;
        public static readonly long LB_FINDSTRINGEXACT = 0x1a2;
        public static readonly long LB_GETANCHORINDEX = 0x19d;
        public static readonly long LB_GETCARETINDEX = 0x19f;
        public static readonly long LB_GETCOUNT = 0x18b;
        public static readonly long LB_GETCURSEL = 0x188;
        public static readonly long LB_GETHORIZONTALEXTENT = 0x193;
        public static readonly long LB_GETITEMDATA = 0x199;
        public static readonly long LB_GETITEMHEIGHT = 0x1a1;
        public static readonly long LB_GETITEMRECT = 0x198;
        public static readonly long LB_GETLOCALE = 0x1a6;
        public static readonly long LB_GETSEL = 0x187;
        public static readonly long LB_GETSELCOUNT = 0x190;
        public static readonly long LB_GETSELITEMS = 0x191;
        public static readonly long LB_GETTEXT = 0x189;
        public static readonly long LB_GETTEXTLEN = 0x18a;
        public static readonly long LB_GETTOPINDEX = 0x18e;
        public static readonly long LB_INSERTSTRING = 0x181;
        public static readonly long LB_MSGMAX = 0x1a8;
        public static readonly long LB_OKAY = 0;
        public static readonly long LB_RESETCONTENT = 0x184;
        public static readonly long LB_SELECTSTRING = 0x18c;
        public static readonly long LB_SELITEMRANGE = 0x19b;
        public static readonly long LB_SELITEMRANGEEX = 0x183;
        public static readonly long LB_SETANCHORINDEX = 0x19c;
        public static readonly long LB_SETCARETINDEX = 0x19e;
        public static readonly long LB_SETCOLUMNWIDTH = 0x195;
        public static readonly long LB_SETCOUNT = 0x1a7;
        public static readonly long LB_SETCURSEL = 0x186;
        public static readonly long LB_SETHORIZONTALEXTENT = 0x194;
        public static readonly long LB_SETITEMDATA = 0x19a;
        public static readonly long LB_SETITEMHEIGHT = 0x1a0;
        public static readonly long LB_SETLOCALE = 0x1a5;
        public static readonly long LB_SETSEL = 0x185;
        public static readonly long LB_SETTABSTOPS = 0x192;
        public static readonly long LB_SETTOPINDEX = 0x197;
        public static readonly long LBN_DBLCLK = 2;
        public static readonly long LBN_ERRSPACE = (-2);
        public static readonly long LBN_KILLFOCUS = 5;
        public static readonly long LBN_SELCANCEL = 3;
        public static readonly long LBN_SELCHANGE = 1;
        public static readonly long LBN_SETFOCUS = 4;
        public static readonly long LVM_FIRST = 0x1000;
        public static readonly long LVM_GETHEADER = (LVM_FIRST + 31);
        public static readonly long LVM_GETBKCOLOR = (LVM_FIRST + 0);
        public static readonly long LVM_SETBKCOLOR = (LVM_FIRST + 1);
        public static readonly long LVM_GETIMAGELIST = (LVM_FIRST + 2);
        public static readonly long LVM_SETIMAGELIST = (LVM_FIRST + 3);
        public static readonly long LVM_GETITEMCOUNT = (LVM_FIRST + 4);
        public static readonly long LVM_GETITEMA = (LVM_FIRST + 5);
        public static readonly long LVM_GETITEM = LVM_GETITEMA;
        public static readonly long LVM_SETITEMA = (LVM_FIRST + 6);
        public static readonly long LVM_SETITEM = LVM_SETITEMA;
        public static readonly long LVM_INSERTITEMA = (LVM_FIRST + 7);
        public static readonly long LVM_INSERTITEM = LVM_INSERTITEMA;
        public static readonly long LVM_DELETEITEM = (LVM_FIRST + 8);
        public static readonly long LVM_DELETEALLITEMS = (LVM_FIRST + 9);
        public static readonly long LVM_GETCALLBACKMASK = (LVM_FIRST + 10);
        public static readonly long LVM_SETCALLBACKMASK = (LVM_FIRST + 11);
        public static readonly long LVM_GETNEXTITEM = (LVM_FIRST + 12);
        public static readonly long LVM_FINDITEMA = (LVM_FIRST + 13);
        public static readonly long LVM_FINDITEM = LVM_FINDITEMA;
        public static readonly long LVM_GETITEMRECT = (LVM_FIRST + 14);
        public static readonly long LVM_SETITEMPOSITION = (LVM_FIRST + 15);
        public static readonly long LVM_GETITEMPOSITION = (LVM_FIRST + 16);
        public static readonly long LVM_GETSTRINGWIDTHA = (LVM_FIRST + 17);
        public static readonly long LVM_GETSTRINGWIDTH = LVM_GETSTRINGWIDTHA;
        public static readonly long LVM_HITTEST = (LVM_FIRST + 18);
        public static readonly long LVM_ENSUREVISIBLE = (LVM_FIRST + 19);
        public static readonly long LVM_SCROLL = (LVM_FIRST + 20);
        public static readonly long LVM_REDRAWITEMS = (LVM_FIRST + 21);
        public static readonly long LVM_ARRANGE = (LVM_FIRST + 22);
        public static readonly long LVM_EDITLABELA = (LVM_FIRST + 23);
        public static readonly long LVM_EDITLABEL = LVM_EDITLABELA;
        public static readonly long LVM_GETEDITCONTROL = (LVM_FIRST + 24);
        public static readonly long LVM_GETCOLUMNA = (LVM_FIRST + 25);
        public static readonly long LVM_GETCOLUMN = LVM_GETCOLUMNA;
        public static readonly long LVM_SETCOLUMNA = (LVM_FIRST + 26);
        public static readonly long LVM_SETCOLUMN = LVM_SETCOLUMNA;
        public static readonly long LVM_INSERTCOLUMNA = (LVM_FIRST + 27);
        public static readonly long LVM_INSERTCOLUMN = LVM_INSERTCOLUMNA;
        public static readonly long LVM_DELETECOLUMN = (LVM_FIRST + 28);
        public static readonly long LVM_GETCOLUMNWIDTH = (LVM_FIRST + 29);
        public static readonly long LVM_SETCOLUMNWIDTH = (LVM_FIRST + 30);
        public static readonly long LVM_CREATEDRAGIMAGE = (LVM_FIRST + 33);
        public static readonly long LVM_GETVIEWRECT = (LVM_FIRST + 34);
        public static readonly long LVM_GETTEXTCOLOR = (LVM_FIRST + 35);
        public static readonly long LVM_SETTEXTCOLOR = (LVM_FIRST + 36);
        public static readonly long LVM_GETTEXTBKCOLOR = (LVM_FIRST + 37);
        public static readonly long LVM_SETTEXTBKCOLOR = (LVM_FIRST + 38);
        public static readonly long LVM_GETTOPINDEX = (LVM_FIRST + 39);
        public static readonly long LVM_GETCOUNTPERPAGE = (LVM_FIRST + 40);
        public static readonly long LVM_GETORIGIN = (LVM_FIRST + 41);
        public static readonly long LVM_UPDATE = (LVM_FIRST + 42);
        public static readonly long LVM_SETITEMSTATE = (LVM_FIRST + 43);
        public static readonly long LVM_GETITEMSTATE = (LVM_FIRST + 44);
        public static readonly long LVM_GETITEMTEXTA = (LVM_FIRST + 45);
        public static readonly long LVM_GETITEMTEXT = LVM_GETITEMTEXTA;
        public static readonly long LVM_SETITEMTEXTA = (LVM_FIRST + 46);
        public static readonly long LVM_SETITEMTEXT = LVM_SETITEMTEXTA;
        public static readonly long LVM_SETITEMCOUNT = (LVM_FIRST + 47);
        public static readonly long LVM_SORTITEMS = (LVM_FIRST + 48);
        public static readonly long LVM_SETITEMPOSITION32 = (LVM_FIRST + 49);
        public static readonly long LVM_GETSELECTEDCOUNT = (LVM_FIRST + 50);
        public static readonly long LVM_GETITEMSPACING = (LVM_FIRST + 51);
        public static readonly long LVM_GETISEARCHSTRINGA = (LVM_FIRST + 52);
        public static readonly long LVM_GETISEARCHSTRING = LVM_GETISEARCHSTRINGA;
        public static readonly long LVM_SETICONSPACING = (LVM_FIRST + 53);
        public static readonly long LVM_SETEXTENDEDLISTVIEWSTYLE = (LVM_FIRST + 54);
        public static readonly long LVM_GETEXTENDEDLISTVIEWSTYLE = (LVM_FIRST + 55);
        public static readonly long LVM_GETSUBITEMRECT = (LVM_FIRST + 56);
        public static readonly long LVM_SUBITEMHITTEST = (LVM_FIRST + 57);
        public static readonly long LVM_SETCOLUMNORDERARRAY = (LVM_FIRST + 58);
        public static readonly long LVM_GETCOLUMNORDERARRAY = (LVM_FIRST + 59);
        public static readonly long LVM_SETHOTITEM = (LVM_FIRST + 60);
        public static readonly long LVM_GETHOTITEM = (LVM_FIRST + 61);
        public static readonly long LVM_SETHOTCURSOR = (LVM_FIRST + 62);
        public static readonly long LVM_GETHOTCURSOR = (LVM_FIRST + 63);
        public static readonly long LVM_APPROXIMATEVIEWRECT = (LVM_FIRST + 64);
        public static readonly long LVS_EX_FULLROWSELECT = 0x20;
        public static readonly long LVSCW_AUTOSIZE = -1;
        public static readonly long LVSCW_AUTOSIZE_USEHEADER = -2;
        public static readonly long WM_ACTIVATE = 0x6;
        public static readonly long WM_ACTIVATEAPP = 0x1c;
        public static readonly long WM_ASKCBFORMATNAME = 0x30c;
        public static readonly long WM_CANCELJOURNAL = 0x4b;
        public static readonly long WM_CANCELMODE = 0x1f;
        public static readonly long WM_CHANGECBCHAIN = 0x30d;
        public static readonly long WM_CHAR = 0x102;
        public static readonly long WM_CHARTOITEM = 0x2f;
        public static readonly long WM_CHILDACTIVATE = 0x22;
        public static readonly long WM_CHOOSEFONT_GETLOGFONT = (WM_USER + 1);
        public static readonly long WM_CHOOSEFONT_SETFLAGS = (WM_USER + 102);
        public static readonly long WM_CHOOSEFONT_SETLOGFONT = (WM_USER + 101);
        public static readonly long WM_CLEAR = 0x303;
        public static readonly long WM_CLOSE = 0x10;
        public static readonly long WM_COMMAND = 0x111;
        public static readonly long WM_COMMNOTIFY = 0x44;
        public static readonly long WM_COMPACTING = 0x41;
        public static readonly long WM_COMPAREITEM = 0x39;
        public static readonly long WM_CONVERTREQUESTEX = 0x108;
        public static readonly long WM_COPY = 0x301;
        public static readonly long WM_COPYDATA = 0x4a;
        public static readonly long WM_CREATE = 0x1;
        public static readonly long WM_CTLCOLORBTN = 0x135;
        public static readonly long WM_CTLCOLORDLG = 0x136;
        public static readonly long WM_CTLCOLOREDIT = 0x133;
        public static readonly long WM_CTLCOLORLISTBOX = 0x134;
        public static readonly long WM_CTLCOLORMSGBOX = 0x132;
        public static readonly long WM_CTLCOLORSCROLLBAR = 0x137;
        public static readonly long WM_CTLCOLORSTATIC = 0x138;
        public static readonly long WM_CUT = 0x300;
        public static readonly long WM_DDE_FIRST = 0x3e0;
        public static readonly long WM_DDE_ACK = (WM_DDE_FIRST + 4);
        public static readonly long WM_DDE_ADVISE = (WM_DDE_FIRST + 2);
        public static readonly long WM_DDE_DATA = (WM_DDE_FIRST + 5);
        public static readonly long WM_DDE_EXECUTE = (WM_DDE_FIRST + 8);
        public static readonly long WM_DDE_INITIATE = (WM_DDE_FIRST);
        public static readonly long WM_DDE_LAST = (WM_DDE_FIRST + 8);
        public static readonly long WM_DDE_POKE = (WM_DDE_FIRST + 7);
        public static readonly long WM_DDE_REQUEST = (WM_DDE_FIRST + 6);
        public static readonly long WM_DDE_TERMINATE = (WM_DDE_FIRST + 1);
        public static readonly long WM_DDE_UNADVISE = (WM_DDE_FIRST + 3);
        public static readonly long WM_DEADCHAR = 0x103;
        public static readonly long WM_DELETEITEM = 0x2d;
        public static readonly long WM_DESTROY = 0x2;
        public static readonly long WM_DESTROYCLIPBOARD = 0x307;
        public static readonly long WM_DEVMODECHANGE = 0x1b;
        public static readonly long WM_DRAWCLIPBOARD = 0x308;
        public static readonly long WM_DRAWITEM = 0x2b;
        public static readonly long WM_DROPFILES = 0x233;
        public static readonly long WM_ENABLE = 0xa;
        public static readonly long WM_ENDSESSION = 0x16;
        public static readonly long WM_ENTERIDLE = 0x121;
        public static readonly long WM_ENTERMENULOOP = 0x211;
        public static readonly long WM_ERASEBKGND = 0x14;
        public static readonly long WM_EXITMENULOOP = 0x212;
        public static readonly long WM_FONTCHANGE = 0x1d;
        public static readonly long WM_GETFONT = 0x31;
        public static readonly long WM_GETDLGCODE = 0x87;
        public static readonly long WM_GETHOTKEY = 0x33;
        public static readonly long WM_GETMINMAXINFO = 0x24;
        public static readonly long WM_GETTEXT = 0xd;
        public static readonly long WM_GETTEXTLENGTH = 0xe;
        public static readonly long WM_HOTKEY = 0x312;
        public static readonly long WM_HSCROLL = 0x114;
        public static readonly long WM_HSCROLLCLIPBOARD = 0x30e;
        public static readonly long WM_ICONERASEBKGND = 0x27;
        public static readonly long WM_IME_CHAR = 0x286;
        public static readonly long WM_IME_COMPOSITION = 0x10f;
        public static readonly long WM_IME_COMPOSITIONFULL = 0x284;
        public static readonly long WM_IME_CONTROL = 0x283;
        public static readonly long WM_IME_ENDCOMPOSITION = 0x10e;
        public static readonly long WM_IME_KEYDOWN = 0x290;
        public static readonly long WM_IME_KEYLAST = 0x10f;
        public static readonly long WM_IME_KEYUP = 0x291;
        public static readonly long WM_IME_NOTIFY = 0x282;
        public static readonly long WM_IME_SELECT = 0x285;
        public static readonly long WM_IME_SETCONTEXT = 0x281;
        public static readonly long WM_IME_STARTCOMPOSITION = 0x10d;
        public static readonly long WM_INITDIALOG = 0x110;
        public static readonly long WM_INITMENU = 0x116;
        public static readonly long WM_INITMENUPOPUP = 0x117;
        public static readonly long WM_KEYDOWN = 0x100;
        public static readonly long WM_KEYFIRST = 0x100;
        public static readonly long WM_KEYLAST = 0x108;
        public static readonly long WM_KEYUP = 0x101;
        public static readonly long WM_KILLFOCUS = 0x8;
        public static readonly long WM_LBUTTONDBLCLK = 0x203;
        public static readonly long WM_LBUTTONDOWN = 0x201;
        public static readonly long WM_LBUTTONUP = 0x202;
        public static readonly long WM_MBUTTONDBLCLK = 0x209;
        public static readonly long WM_MBUTTONDOWN = 0x207;
        public static readonly long WM_MBUTTONUP = 0x208;
        public static readonly long WM_MDIACTIVATE = 0x222;
        public static readonly long WM_MDICASCADE = 0x227;
        public static readonly long WM_MDICREATE = 0x220;
        public static readonly long WM_MDIDESTROY = 0x221;
        public static readonly long WM_MDIGETACTIVE = 0x229;
        public static readonly long WM_MDIICONARRANGE = 0x228;
        public static readonly long WM_MDIMAXIMIZE = 0x225;
        public static readonly long WM_MDINEXT = 0x224;
        public static readonly long WM_MDIREFRESHMENU = 0x234;
        public static readonly long WM_MDIRESTORE = 0x223;
        public static readonly long WM_MDISETMENU = 0x230;
        public static readonly long WM_MDITILE = 0x226;
        public static readonly long WM_MEASUREITEM = 0x2c;
        public static readonly long WM_MENUCHAR = 0x120;
        public static readonly long WM_MENUSELECT = 0x11f;
        public static readonly long WM_MOUSEACTIVATE = 0x21;
        public static readonly long WM_MOUSEFIRST = 0x200;
        public static readonly long WM_MOUSELAST = 0x209;
        public static readonly long WM_MOUSEMOVE = 0x200;
        public static readonly long WM_MOVE = 0x3;
        public static readonly long WM_NCACTIVATE = 0x86;
        public static readonly long WM_NCCALCSIZE = 0x83;
        public static readonly long WM_NCCREATE = 0x81;
        public static readonly long WM_NCDESTROY = 0x82;
        public static readonly long WM_NCHITTEST = 0x84;
        public static readonly long WM_NCLBUTTONDBLCLK = 0xa3;
        public static readonly long WM_NCLBUTTONDOWN = 0xa1;
        public static readonly long WM_NCLBUTTONUP = 0xa2;
        public static readonly long WM_NCMBUTTONDBLCLK = 0xa9;
        public static readonly long WM_NCMBUTTONDOWN = 0xa7;
        public static readonly long WM_NCMBUTTONUP = 0xa8;
        public static readonly long WM_NCMOUSEMOVE = 0xa0;
        public static readonly long WM_NCPAlong = 0x85;
        public static readonly long WM_NCRBUTTONDBLCLK = 0xa6;
        public static readonly long WM_NCRBUTTONDOWN = 0xa4;
        public static readonly long WM_NCRBUTTONUP = 0xa5;
        public static readonly long WM_NEXTDLGCTL = 0x28;
        public static readonly long WM_NULL = 0x0;
        public static readonly long WM_OTHERWINDOWCREATED = 0x42;
        public static readonly long WM_OTHERWINDOWDESTROYED = 0x43;
        public static readonly long WM_PAlong = 0xf;
        public static readonly long WM_PAlongCLIPBOARD = 0x309;
        public static readonly long WM_PAlongICON = 0x26;
        public static readonly long WM_PALETTECHANGED = 0x311;
        public static readonly long WM_PALETTEISCHANGING = 0x310;
        public static readonly long WM_PARENTNOTIFY = 0x210;
        public static readonly long WM_PASTE = 0x302;
        public static readonly long WM_PENWINFIRST = 0x380;
        public static readonly long WM_PENWINLAST = 0x38f;
        public static readonly long WM_POWER = 0x48;
        public static readonly long WM_PSD_ENVSTAMPRECT = (WM_USER + 5);
        public static readonly long WM_PSD_FULLPAGERECT = (WM_USER + 1);
        public static readonly long WM_PSD_GREEKTEXTRECT = (WM_USER + 4);
        public static readonly long WM_PSD_MARGINRECT = (WM_USER + 3);
        public static readonly long WM_PSD_MINMARGINRECT = (WM_USER + 2);
        public static readonly long WM_PSD_PAGESETUPDLG = (WM_USER);
        public static readonly long WM_PSD_YAFULLPAGERECT = (WM_USER + 6);
        public static readonly long WM_QUERYDRAGICON = 0x37;
        public static readonly long WM_QUERYENDSESSION = 0x11;
        public static readonly long WM_QUERYNEWPALETTE = 0x30f;
        public static readonly long WM_QUERYOPEN = 0x13;
        public static readonly long WM_QUEUESYNC = 0x23;
        public static readonly long WM_QUIT = 0x12;
        public static readonly long WM_RBUTTONDBLCLK = 0x206;
        public static readonly long WM_RBUTTONDOWN = 0x204;
        public static readonly long WM_RBUTTONUP = 0x205;
        public static readonly long WM_RENDERALLFORMATS = 0x306;
        public static readonly long WM_RENDERFORMAT = 0x305;
        public static readonly long WM_SETCURSOR = 0x20;
        public static readonly long WM_SETFOCUS = 0x7;
        public static readonly long WM_SETFONT = 0x30;
        public static readonly long WM_SETHOTKEY = 0x32;
        public static readonly long WM_SETREDRAW = 0xb;
        public static readonly long WM_SETTEXT = 0xc;
        public static readonly long WM_SHOWWINDOW = 0x18;
        public static readonly long WM_SIZE = 0x5;
        public static readonly long WM_SIZECLIPBOARD = 0x30b;
        public static readonly long WM_SPOOLERSTATUS = 0x2a;
        public static readonly long WM_SYSCHAR = 0x106;
        public static readonly long WM_SYSCOLORCHANGE = 0x15;
        public static readonly long WM_SYSCOMMAND = 0x112;
        public static readonly long WM_SYSDEADCHAR = 0x107;
        public static readonly long WM_SYSKEYDOWN = 0x104;
        public static readonly long WM_SYSKEYUP = 0x105;
        public static readonly long WM_TIMECHANGE = 0x1e;
        public static readonly long WM_TIMER = 0x113;
        public static readonly long WM_UNDO = 0x304;
        public static readonly long WM_VKEYTOITEM = 0x2e;
        public static readonly long WM_VSCROLL = 0x115;
        public static readonly long WM_VSCROLLCLIPBOARD = 0x30a;
        public static readonly long WM_WINDOWPOSCHANGED = 0x47;
        public static readonly long WM_WINDOWPOSCHANGING = 0x46;
        public static readonly long WM_WININICHANGE = 0x1a;
        public static readonly long WS_BORDER = 0x800000;
        public static readonly long WS_CAPTION = 0xc00000;
        public static readonly long WS_CHILD = 0x40000000;
        public static readonly long WS_CHILDWINDOW = (WS_CHILD);
        public static readonly long WS_CLIPCHILDREN = 0x2000000;
        public static readonly long WS_CLIPSIBLINGS = 0x4000000;
        public static readonly long WS_DISABLED = 0x8000000;
        public static readonly long WS_DLGFRAME = 0x400000;
        public static readonly long WS_EX_ACCEPTFILES = 0x10L;
        public static readonly long WS_EX_DLGMODALFRAME = 0x1L;
        public static readonly long WS_EX_NOPARENTNOTIFY = 0x4L;
        public static readonly long WS_EX_TOPMOST = 0x8L;
        public static readonly long WS_EX_TRANSPARENT = 0x20L;
        public static readonly long WS_GROUP = 0x20000;
        public static readonly long WS_HSCROLL = 0x100000;
        public static readonly long WS_MINIMIZE = 0x20000000;
        public static readonly long WS_ICONIC = WS_MINIMIZE;
        public static readonly long WS_MAXIMIZE = 0x1000000;
        public static readonly long WS_MAXIMIZEBOX = 0x10000;
        public static readonly long WS_MINIMIZEBOX = 0x20000;
        public static readonly long WS_SYSMENU = 0x80000;
        public static readonly long WS_THICKFRAME = 0x40000;
        public static readonly long WS_OVERLAPPED = 0x0L;
        public static readonly long WS_OVERLAPPEDWINDOW = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);
        public static readonly long WS_POPUP = 0x80000000;
        public static readonly long WS_POPUPWINDOW = (WS_POPUP | WS_BORDER | WS_SYSMENU);
        public static readonly long WS_SIZEBOX = WS_THICKFRAME;
        public static readonly long WS_TABSTOP = 0x10000;
        public static readonly long WS_TILED = WS_OVERLAPPED;
        public static readonly long WS_TILEDWINDOW = WS_OVERLAPPEDWINDOW;
        public static readonly long WS_VISIBLE = 0x10000000;
        public static readonly long WS_VSCROLL = 0x200000;
        public static readonly long LBS_DISABLENOSCROLL = 0x1000L;
        public static readonly long LBS_EXTENDEDSEL = 0x800L;
        public static readonly long LBS_HASSTRINGS = 0x40L;
        public static readonly long LBS_MULTICOLUMN = 0x200L;
        public static readonly long LBS_MULTIPLESEL = 0x8L;
        public static readonly long LBS_NODATA = 0x2000L;
        public static readonly long LBS_NOlongEGRALHEIGHT = 0x100L;
        public static readonly long LBS_NOREDRAW = 0x4L;
        public static readonly long LBS_NOTIFY = 0x1L;
        public static readonly long LBS_OWNERDRAWFIXED = 0x10L;
        public static readonly long LBS_OWNERDRAWVARIABLE = 0x20L;
        public static readonly long LBS_SORT = 0x2L;
        public static readonly long LBS_STANDARD = (LBS_NOTIFY | LBS_SORT | WS_VSCROLL | WS_BORDER);
        public static readonly long LBS_USETABSTOPS = 0x80L;
        public static readonly long LBS_WANTKEYBOARDINPUT = 0x400L;
        public static readonly string LBSELCHSTRING = "commdlg_LBSelChangedNotify";
        public static readonly long TB_ENABLEBUTTON = (WM_USER + 1);
        public static readonly long TB_CHECKBUTTON = (WM_USER + 2);
        public static readonly long TB_PRESSBUTTON = (WM_USER + 3);
        public static readonly long TB_HIDEBUTTON = (WM_USER + 4);
        public static readonly long TB_INDETERMINATE = (WM_USER + 5);
        public static readonly long TB_MARKBUTTON = (WM_USER + 6);
        public static readonly long TB_ISBUTTONENABLED = (WM_USER + 9);
        public static readonly long TB_ISBUTTONCHECKED = (WM_USER + 10);
        public static readonly long TB_ISBUTTONPRESSED = (WM_USER + 11);
        public static readonly long TB_ISBUTTONHIDDEN = (WM_USER + 12);
        public static readonly long TB_ISBUTTONINDETERMINATE = (WM_USER + 13);
        public static readonly long TB_ISBUTTONHIGHLIGHTED = (WM_USER + 14);
        public static readonly long TB_SETSTATE = (WM_USER + 17);
        public static readonly long TB_GETSTATE = (WM_USER + 18);
        public static readonly long TB_ADDBITMAP = (WM_USER + 19);
        public static readonly long TB_ADDBUTTONSA = (WM_USER + 20);
        public static readonly long TB_INSERTBUTTONA = (WM_USER + 21);
        public static readonly long TB_ADDBUTTONS = (WM_USER + 20);
        public static readonly long TB_INSERTBUTTON = (WM_USER + 21);
        public static readonly long TB_DELETEBUTTON = (WM_USER + 22);
        public static readonly long TB_GETBUTTON = (WM_USER + 23);
        public static readonly long TB_BUTTONCOUNT = (WM_USER + 24);
        public static readonly long TB_COMMANDTOINDEX = (WM_USER + 25);
        public static readonly long TB_SAVERESTOREA = (WM_USER + 26);
        public static readonly long TB_SAVERESTOREW = (WM_USER + 76);
        public static readonly long TB_CUSTOMIZE = (WM_USER + 27);
        public static readonly long TB_ADDSTRINGA = (WM_USER + 28);
        public static readonly long TB_ADDSTRINGW = (WM_USER + 77);
        public static readonly long TB_GETITEMRECT = (WM_USER + 29);
        public static readonly long TB_BUTTONSTRUCTSIZE = (WM_USER + 30);
        public static readonly long TB_SETBUTTONSIZE = (WM_USER + 31);
        public static readonly long TB_SETBITMAPSIZE = (WM_USER + 32);
        public static readonly long TB_AUTOSIZE = (WM_USER + 33);
        public static readonly long TB_GETTOOLTIPS = (WM_USER + 35);
        public static readonly long TB_SETTOOLTIPS = (WM_USER + 36);
        public static readonly long TB_SETPARENT = (WM_USER + 37);
        public static readonly long TB_SETROWS = (WM_USER + 39);
        public static readonly long TB_GETROWS = (WM_USER + 40);
        public static readonly long TB_SETCMDID = (WM_USER + 42);
        public static readonly long TB_CHANGEBITMAP = (WM_USER + 43);
        public static readonly long TB_GETBITMAP = (WM_USER + 44);
        public static readonly long TB_GETBUTTONTEXTA = (WM_USER + 45);
        public static readonly long TB_GETBUTTONTEXTW = (WM_USER + 75);
        public static readonly long TB_REPLACEBITMAP = (WM_USER + 46);
        public static readonly long TB_SETINDENT = (WM_USER + 47);
        public static readonly long TB_SETIMAGELIST = (WM_USER + 48);
        public static readonly long TB_GETIMAGELIST = (WM_USER + 49);
        public static readonly long TB_LOADIMAGES = (WM_USER + 50);
        public static readonly long TB_GETRECT = (WM_USER + 51);
        public static readonly long TB_SETHOTIMAGELIST = (WM_USER + 52);
        public static readonly long TB_GETHOTIMAGELIST = (WM_USER + 53);
        public static readonly long TB_SETDISABLEDIMAGELIST = (WM_USER + 54);
        public static readonly long TB_GETDISABLEDIMAGELIST = (WM_USER + 55);
        public static readonly long TB_SETSTYLE = (WM_USER + 56);
        public static readonly long TB_GETSTYLE = (WM_USER + 57);
        public static readonly long TB_GETBUTTONSIZE = (WM_USER + 58);
        public static readonly long TB_SETBUTTONWIDTH = (WM_USER + 59);
        public static readonly long TB_SETMAXTEXTROWS = (WM_USER + 60);
        public static readonly long TB_GETTEXTROWS = (WM_USER + 61);
        public static readonly long TBSTYLE_BUTTON = 0x0;
        public static readonly long TBSTYLE_SEP = 0x1;
        public static readonly long TBSTYLE_CHECK = 0x2;
        public static readonly long TBSTYLE_GROUP = 0x4;
        public static readonly long TBSTYLE_CHECKGROUP = (TBSTYLE_GROUP | TBSTYLE_CHECK);
        public static readonly long TBSTYLE_DROPDOWN = 0x8;
        public static readonly long TBSTYLE_AUTOSIZE = 0x10;
        public static readonly long TBSTYLE_NOPREFIX = 0x20;
        public static readonly long TBSTYLE_TOOLTIPS = 0x100;
        public static readonly long TBSTYLE_WRAPABLE = 0x200;
        public static readonly long TBSTYLE_ALTDRAG = 0x400;
        public static readonly long TBSTYLE_FLAT = 0x800;
        public static readonly long TBSTYLE_LIST = 0x1000;
        public static readonly long TBSTYLE_CUSTOMERASE = 0x2000;
        public static readonly long TBSTYLE_REGISTERDROP = 0x4000;
        public static readonly long TBSTYLE_TRANSPARENT = 0x8000;
        public static readonly long TBSTYLE_EX_DRAWDDARROWS = 0x1;

    }
}
