﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using MiguelUtil.Lua;
using MiguelUtil;
using StringExtensions;

namespace MiguelUtil.SimplifiedLuaFormat
{
    /// <summary>
    /// Represents a Simplified Lua Format File that contains human-readable data
    /// </summary>
    public class SLFFile
    {
        public SLFTable Data;
        private const string SplitRegex = "(\r\n)|(\t)|(.*--.*)|(\\{)|(\\})|(=)|(,)|(\".*\")|(\\()|(\\))";

        public SLFFile()
        {
            Data = new SLFTable();
        }

        public SLFFile(string filepath) : this(new StreamReader(filepath)) { }

        public SLFFile(Stream Input) : this(new StreamReader(Input)) { }

        public SLFFile(StreamReader Reader)
        {
            ReadFile(Reader);
        }

        public void WriteFile(string path)
        {
            WriteFile(new StreamWriter(path));
        }

        public void WriteFile(Stream input)
        {
            WriteFile(new StreamWriter(input));
        }

        public void WriteFile(StreamWriter Write)
        {
            Write.BaseStream.Position = 0;

            StringWriter Writer = new StringWriter();
            foreach (KeyValuePair<object, object> table in Data)
            {
                Writer.WriteLine("[" + (string)table.Key + "]");
                Writer.WriteLine();
                WriteTableContents(Writer, (SLFTable)table.Value);
                Writer.WriteLine();
            }
            Write.Write(Writer.ToString().TrimEnd());
            Write.Close();
        }

        private void WriteTableContents(StringWriter Writer, SLFTable table)
        {
            foreach (KeyValuePair<string, object> KVP in table.StringIndexed)
            {
                if (KVP.Value is SLFTable)
                {
                    SLFTable Value = KVP.Value as SLFTable;
                    Writer.Write(KVP.Key.Indent(Value.Level) + "\r\n" + "{".Indent(Value.Level) + "\r\n");
                    WriteTableContents(Writer, Value);
                    Writer.Write("}".Indent(Value.Level) + "\r\n");
                }
                else if (KVP.Value is bool)
                    Writer.WriteLine((KVP.Key + " = " + KVP.Value.ToString().ToLower()).Indent(table.Level));
                else
                    Writer.WriteLine((KVP.Key + " = " + KVP.Value.ToString()).Indent(table.Level));
            }

            foreach (object intindexed in table.IntIndexed)
            {
                if (intindexed is bool)
                    Writer.WriteLine(intindexed.ToString().ToLower().Indent(table.Level + 1));
                else
                    Writer.WriteLine(intindexed.ToString().Indent(table.Level + 1));
            }
        }

        public void ReadFile(StreamReader Reader)
        {
            string[] Tokens = Regex.Split(Reader.ReadToEnd(), SplitRegex);
            var FilteredTokens = from i in Tokens
                                 where i.Trim() != ""
                                 select i.Contains("--") ? i.Substring(0, i.IndexOf("--")).Trim() : i.Trim();

            string Key = "";
            string KeyOrValue = "";
            float Out;
            Data = new SLFTable();
            SLFTable CurrentTable = Data;

            foreach (string Token in FilteredTokens)
            {
                if (Token != "")
                {
                    // Is true
                    if (Token == "true")
                    {
                        CurrentTable.Add(Key, true);
                        CurrentTable.State = SLFReadState.WaitingForTableItem;
                        Key = null;
                    }
                    // Is false
                    else if (Token == "false")
                    {
                        CurrentTable.Add(Key, false);
                        CurrentTable.State = SLFReadState.WaitingForTableItem;
                        Key = null;
                    }
                    // Is title
                    else if (Token[0] == '[' && Token[Token.Length - 1] == ']')
                    {
                        if (CurrentTable.State == SLFReadState.WaitingForTableItem)
                        {
                            // Add unknown word because it has to be a value
                            if (KeyOrValue != null && CurrentTable.State == SLFReadState.WillAddOrAssign)
                            {
                                CurrentTable.Add(KeyOrValue);
                                KeyOrValue = null;
                            }

                            if (CurrentTable.Parent != null)
                                CurrentTable = CurrentTable.Parent;
                            SLFTable NewTable = new SLFTable(CurrentTable);
                            CurrentTable.Add(Token.Remove(new char[] { '[', ']' }), NewTable);
                            CurrentTable = NewTable;
                            Key = null;
                            KeyOrValue = null;
                        }
                        else
                            throw new Exception("Key invalid in this position.");
                    }
                    // Is equals
                    else if (Token[0] == '=')
                    {
                        if (CurrentTable.State == SLFReadState.WillAddOrAssign)
                        {
                            Key = KeyOrValue;
                            KeyOrValue = null;
                            CurrentTable.State = SLFReadState.WaitingForValue;
                        }
                        else
                            throw new Exception("Equals invalid in this position.");
                    }
                    // Is opened bracket
                    else if (Token[0] == '{')
                    {
                        if (CurrentTable.State == SLFReadState.WillAddOrAssign)
                        {
                            SLFTable NewTable = new SLFTable(CurrentTable);
                            CurrentTable.Add(KeyOrValue, NewTable);
                            CurrentTable = NewTable;
                            Key = null;
                            KeyOrValue = null;
                            CurrentTable.State = SLFReadState.WaitingForTableItem;
                        }
                        else
                            throw new Exception("Bracket invalid in this position.");
                    }
                    // Is closed bracket
                    else if (Token[0] == '}')
                    {
                        if (CurrentTable.State == SLFReadState.WaitingForTableItem || CurrentTable.State == SLFReadState.WillAddOrAssign)
                        {
                            // Add unknown word because it has to be a value
                            if (KeyOrValue != null && CurrentTable.State == SLFReadState.WillAddOrAssign)
                            {
                                CurrentTable.Add(KeyOrValue);
                                KeyOrValue = null;
                            }

                            // Go up a level in tables
                            if (CurrentTable.Parent != null)
                            {
                                CurrentTable = CurrentTable.Parent;
                                CurrentTable.State = SLFReadState.WaitingForTableItem;
                            }
                            else
                                return;
                        }
                        else
                            throw new Exception("Bracket invalid in this position.");
                    }

                    // Is number
                    else if (float.TryParse(Token, out Out) && Token.Contains('\"') == false)
                    {
                        CurrentTable.Add(Key, int.Parse(Token));
                        CurrentTable.State = SLFReadState.WaitingForTableItem;
                        Key = null;
                    }
                    // Is word
                    else
                    {
                        string Word = Token;
                        if (Token.StartsWith("\""))
                        {
                            Word = Token.Replace("\"", null);
                        }
                        if (CurrentTable.State == SLFReadState.WaitingForTableItem)
                        {
                            KeyOrValue = Word;
                            CurrentTable.State = SLFReadState.WillAddOrAssign;
                        }
                        else if (CurrentTable.State == SLFReadState.WillAddOrAssign)
                        {
                            CurrentTable.Add(KeyOrValue);
                            KeyOrValue = Word;
                        }
                        else if (CurrentTable.State == SLFReadState.WaitingForValue)
                        {
                            CurrentTable.Add(Key, Word);
                            Key = null;
                            CurrentTable.State = SLFReadState.WaitingForTableItem;
                        }
                    }
                }
            }
            Reader.Close();
        }
    }
}
