﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.SimplifiedLuaFormat
{
    public enum SLFReadState
    {
        WaitingForValue,
        WaitingForTableItem,
        WillAddOrAssign
    }
}
