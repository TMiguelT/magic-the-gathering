﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using MiguelUtil.Lua;

namespace MiguelUtil.SimplifiedLuaFormat
{
    public class SLFTable : LuaTable
    {
        new public SLFReadState State;
        new public SLFTable Parent;

        public SLFTable(SLFReadState state = SLFReadState.WaitingForTableItem)
            : base()
        {
            State = state;
        }

        public SLFTable(SLFTable parent, SLFReadState state = SLFReadState.WaitingForTableItem)
            : base()
        {
            Parent = parent;
            State = state;
        }

        public override string ToString()
        {
            return "Count: " + Count.ToString();
        }
    }
}
