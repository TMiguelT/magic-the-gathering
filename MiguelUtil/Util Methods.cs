using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;
using System.Security.AccessControl;
using System.IO;
namespace MiguelUtil
{
    public static class UtilMethods
    {
        public static Control[] GetControls(Control FindIn)
        {
            List<Control> AllControls = new List<Control>();
            foreach (Control OneControl in FindIn.Controls)
            {
                AllControls.Add(OneControl);
                if (OneControl.Controls.Count > 0 && OneControl is NumericUpDown == false)
                    AllControls.AddRange(GetControls(OneControl));
            }
            return AllControls.ToArray();
        }

        /// <summary>
        /// Associates a file type with the given program.
        /// </summary>
        /// <param name="Extension">The file extension to associate the program with.</param>
        /// <param name="ProgramFileName">The name the Registry will use to link the application and the file type.</param>
        /// <param name="Program">The file path to the program.</param>
        /// <param name="FileType">The name for the file type to be displayed in Windows Explorer.</param>
        /// <param name="IconPath">The path to the icon used for this file type.</param>
        public static void SetAssociation(string Extension, string ProgramFileName, string Program, string FileType, string IconPath)
        {
            RegistryKey BaseKey;
            RegistryKey OpenMethod;
            RegistryKey Shell;
            RegistryKey CurrentUser;

            BaseKey = Registry.CurrentUser.OpenSubKey("Software\\Classes", true).CreateSubKey(Extension);
            BaseKey.SetValue("", ProgramFileName);

            OpenMethod = Registry.CurrentUser.OpenSubKey("Software\\Classes", true).CreateSubKey(ProgramFileName);
            OpenMethod.SetValue("", FileType);
            OpenMethod.CreateSubKey("DefaultIcon").SetValue("", "\"" + IconPath + "\"");
            Shell = OpenMethod.CreateSubKey("shell");
            Shell.CreateSubKey("edit").CreateSubKey("command").SetValue("", "\"" + Program + "\"" + " \"%1\"");
            Shell.CreateSubKey("open").CreateSubKey("command").SetValue("", "\"" + Program + "\"" + " \"%1\"");
            BaseKey.Close();
            OpenMethod.Close();
            Shell.Close();

            CurrentUser = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\" + Extension, true);
            if (CurrentUser != null)
            {
                CurrentUser.DeleteSubKey("UserChoice", false);
                CurrentUser.DeleteValue("Application", false);
                CurrentUser.Close();
            }

            SHChangeNotify(0x08000000, 0x0000, IntPtr.Zero, IntPtr.Zero);
        }

        public static bool NotDefault(string extension, string program, string key)
        {
            RegistryKey Explorer = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\" + extension, RegistryKeyPermissionCheck.ReadWriteSubTree, System.Security.AccessControl.RegistryRights.FullControl);
            if (Registry.ClassesRoot.OpenSubKey(extension).GetValue("", "").ToString() != key || (Explorer != null && Explorer.SubKeyCount > 2))
                return true;
            else
                return false;
        }

        public static bool IsAdmin()
        {
            return new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator);
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void Shuffle<T>(this IList<T> list, int seed)
        {
            Random rng = new Random(seed);
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void DebugWrite(string path, string contents)
        {
            // File.AppendAllText(path, contents + "\r\n");
        }

        [DllImport("shell32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern void SHChangeNotify(uint wEventId, uint uFlags, IntPtr dwItem1, IntPtr dwItem2);
    }
}
