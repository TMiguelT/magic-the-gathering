﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.Lua
{
    public enum LuaReadState
    {
        WaitingForKey,
        WaitingForEquals,
        WaitingForValue
    }
}
