﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace MiguelUtil.Lua
{
    public class LuaTable : IEnumerable
    {
        public LuaReadState State = LuaReadState.WaitingForKey;
        public LuaTable Parent;
        internal Dictionary<string, object> StringIndexed = new Dictionary<string, object>();
        internal List<object> IntIndexed = new List<object>();

        public LuaTable() : base() { }

        public LuaTable(LuaTable parent)
            : base()
        {
            Parent = parent;
        }

        public int Level
        {
            get
            {
                LuaTable Parent = this;
                int Level = 1;
                while (Parent.Parent != null)
                {
                    Parent = Parent.Parent;
                    Level++;
                }
                return Level;
            }
        }

        public bool Contains(object Value)
        {
            return StringIndexed.ContainsValue(Value) || IntIndexed.Contains(Value);
        }

        public bool ContainsKey(object Value)
        {
            return AsDictionary().Keys.Contains(Value);
        }

        public void AddRange(IEnumerable<object> items)
        {
            IntIndexed.AddRange(items);
        }

        public void AddRange(IDictionary<string, object> items)
        {
            foreach (KeyValuePair<string, object> KVP in items)
            {
                StringIndexed.Add(KVP.Key, KVP.Value);
            }
        }

        public object KeyOf(object Value)
        {
            foreach (KeyValuePair<string, object> KVP in StringIndexed)
            {
                if (KVP.Value == Value)
                {
                    return KVP.Key;
                }
            }

            return IntIndexed.IndexOf(Value);
        }

        public void Add(object item)
        {
            IntIndexed.Add(item);
        }

        public void Add(int key, object item)
        {
            IntIndexed.Insert(key, item);
        }

        public void Add(string key, object item)
        {
            if (key != null)
                StringIndexed.Add(key, item);
            else
                IntIndexed.Add(item);
        }

        public int Count
        {
            get
            {
                return IntIndexed.Count + StringIndexed.Count;
            }
        }

        public object this[int key]
        {
            get
            {
                return IntIndexed[key];
            }
            set
            {
                IntIndexed[key] = value;
            }
        }

        public object this[string key]
        {
            get
            {
                return StringIndexed[key];
            }
            set
            {
                StringIndexed[key] = value; ;
            }
        }

        private Dictionary<object, object> AsDictionary()
        {
            Dictionary<object, object> Enumerate = new Dictionary<object, object>();
            foreach (KeyValuePair<string, object> KVP in StringIndexed)
            {
                Enumerate.Add(KVP.Key, KVP.Value);
            }
            foreach (object item in IntIndexed)
            {
                Enumerate.Add(IntIndexed.IndexOf(item), item);
            }
            return Enumerate;
        }

        private string SortDictionaryByKey(KeyValuePair<string, object> KVP)
        {
            return KVP.Key;
        }

        private object SortDictionaryByValue(KeyValuePair<string, object> KVP)
        {
            return KVP.Value;
        }

        public void SortIntIndexed(IComparer<object> comparer)
        {
            IntIndexed.Sort(comparer);
        }

        public void SortStringIndexedByIndex(IComparer<string> comparer)
        {
            Dictionary<string, object> NewDictionary= new Dictionary<string,object>();
            foreach (KeyValuePair<string, object> Pair in StringIndexed.OrderBy(SortDictionaryByKey, comparer))
            {
                NewDictionary.Add(Pair.Key, Pair.Value);
            }
            StringIndexed = NewDictionary;
        }

        public void SortStringIndexedByValue(IComparer<object> comparer)
        {
            Dictionary<string, object> NewDictionary = new Dictionary<string, object>();
            foreach (KeyValuePair<string, object> Pair in StringIndexed.OrderBy(SortDictionaryByValue, comparer))
            {
                NewDictionary.Add(Pair.Key, Pair.Value);
            }
            StringIndexed = NewDictionary;
        }

        public IEnumerator GetEnumerator()
        {

            return AsDictionary().GetEnumerator();
        }
    }
}
