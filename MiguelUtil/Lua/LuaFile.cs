﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using StringExtensions;

namespace MiguelUtil.Lua
{
    public class LuaFile
    {
        public LuaTable Data;
        private const string SplitRegex = "(\\{)|(\\})|(=)|(,)|(\\()|(\\))|(\\[)|(\\])";

        public LuaFile(Stream input) : this(new StreamReader(input)) { }

        public LuaFile(StreamReader Reader)
        {
            string Trimmed = Reader.ReadToEnd().Remove(new char[] { '\r', '\n', ' ', '\t' });
            string[] Tokens = Regex.Split(Trimmed, SplitRegex);

            string Key = "";
            //  object Value;
            Data = new LuaTable();
            LuaTable CurrentTable = Data;

            foreach (string Token in Tokens)
            {
                if (Token != "")
                {
                    // Is true
                    if (Token == "true")
                    {
                        CurrentTable.Add(Key, true);
                        CurrentTable.State = LuaReadState.WaitingForKey;
                        Key = null;
                    }
                    // Is false
                    else if (Token == "false")
                    {
                        CurrentTable.Add(Key, false);
                        CurrentTable.State = LuaReadState.WaitingForKey;
                        Key = null;
                    }
                    // Is key
                    else if (char.IsLetter(Token[0]))
                    {
                        if (CurrentTable.State == LuaReadState.WaitingForKey)
                        {
                            CurrentTable.State = LuaReadState.WaitingForEquals;
                            Key = Token;
                        }
                        else if (CurrentTable.State == LuaReadState.WaitingForValue)
                        {
                            Key = Token;
                            CurrentTable.State = LuaReadState.WaitingForEquals;
                        }
                        else
                            throw new Exception("Key invalid in this position.");
                    }
                    // Is equals
                    else if (Token[0] == '=')
                    {
                        if (CurrentTable.State == LuaReadState.WaitingForEquals)
                        {
                            CurrentTable.State = LuaReadState.WaitingForValue;
                        }
                        else
                            throw new Exception("\"=\" invalid in this position.");
                    }
                    // Is opened bracket
                    else if (Token[0] == '{')
                    {
                        if (CurrentTable.State == LuaReadState.WaitingForValue)
                        {
                            LuaTable NewTable = new LuaTable(CurrentTable);
                            CurrentTable.Add(Key, NewTable);
                            CurrentTable = NewTable;
                            Key = null;
                        }
                    }
                    // Is closed bracket
                    else if (Token[0] == '}')
                    {
                        if (CurrentTable.Parent != null)
                            CurrentTable = CurrentTable.Parent;
                        else
                            return;
                    }
                    // Is comma
                    else if (Token[0] == ',')
                    {

                    }
                    // Is string
                    else if (Token[0] == '\"')
                    {
                        CurrentTable.Add(Key, Token.Replace("\"", ""));
                        CurrentTable.State = LuaReadState.WaitingForKey;
                        Key = null;
                    }
                    // Is number
                    else if (char.IsDigit(Token[0]))
                    {
                        CurrentTable.Add(Key, int.Parse(Token));
                        CurrentTable.State = LuaReadState.WaitingForKey;
                        Key = null;
                    }
                }
            }
        }
    }
}
