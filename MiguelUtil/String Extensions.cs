using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Diagnostics;
using MiguelUtil.Find;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;

namespace StringExtensions
{
    public static class StringExtensions
    {
        public static char[] Vowels = new char[] { 'a', 'e', 'i', 'o', 'u' };

        public static string RemoveDirectories(this string input, int number)
        {
            string Return = input;
            for (int i = 0; i < number; i++)
            {
                Return = Return.Substring(Return.IndexOf('\\'));
            }
            return Return;
        }

        public static string RemoveDirectory(this string input)
        {
            return RemoveDirectories(input, 1);
        }

        public static string EscapeExtended(this string escape)
        {
            return EscapeExcept(escape, new string[] { @"\r", @"\n", @"\t", @"\v" });
        }

        public static string EscapeExcept(this string escape, string[] except)
        {
            string escaped = Regex.Escape(escape);
            foreach (string unescape in except)
            {
                escaped = escaped.Replace("\\" + unescape, unescape);
            }
            return escaped;
        }

        public static string Indent(this string Indent, int BaseIndent)
        {
            int ExtraIndent = 0;
            StringBuilder IndentBuild = new StringBuilder();
            StringReader Reader = new StringReader(Indent);
            while (true)
            {
                string Read = Reader.ReadLine();
                if (Read == null)
                    break;
                else
                {

                    if (Read.Contains("}"))
                        ExtraIndent--;
                    for (int i = 0; i < BaseIndent + ExtraIndent; i++)
                    {
                        IndentBuild.Append('\t');
                    }
                    if (Read.Contains("{"))
                        ExtraIndent++;
                    IndentBuild.AppendLine(Read);
                }
            }
            return IndentBuild.ToString().TrimEnd('\r', '\n');
        }

        public static string TrimLines(this string Trim)
        {
            StringBuilder IndentBuild = new StringBuilder();
            StringReader Reader = new StringReader(Trim);
            while (true)
            {
                string Read = Reader.ReadLine();
                if (Read == null)
                    break;
                else
                {
                    IndentBuild.AppendLine(Read.Trim());
                }
            }
            return IndentBuild.ToString();
        }

        public static bool ContainsAnyCase(this string input, string search)
        {
            return input.IndexOf(search, StringComparison.InvariantCultureIgnoreCase) != -1;
        }

        public static bool Contains(this string input, string search, StringComparison comparison)
        {
            return input.IndexOf(search, comparison) != -1;
        }

        public static string SplitCamelCase(this string CamelCase)
        {
            StringBuilder Output = new StringBuilder();

            for (int i = 0; i < CamelCase.Length; i++)
            {
                char Letter = CamelCase[i];
                if (char.IsUpper(Letter) && i != 0)
                {
                    Output.Append(' ');
                }
                Output.Append(Letter);
            }

            return Output.ToString();
        }

        public static string GetIndefiniteArticle(this string input, bool add = false)
        {
            for (int i = 0; i < input.Length; i++)
            {
                if (char.IsLetter(input[i]))
                {
                    if (((IList<char>)Vowels).Contains(char.ToLower(input[i])))
                        if (add)
                            return "an " + input;
                        else
                            return "an";
                    else
                        if (add)
                            return "a " + input;
                        else
                            return "a";
                }
            }
            return input;
        }

        public static string[] ArraySplitCamelCase(this string CamelCase)
        {
            List<string> Array = new List<string>();
            StringBuilder Word = new StringBuilder();

            for (int i = 0; i < CamelCase.Length; i++)
            {
                char Letter = CamelCase[i];
                Word.Append(Letter);
                if (char.IsUpper(Letter) && i != 0)
                {
                    Array.Add(Word.ToString());
                    Word.Clear();
                }
            }

            return Array.ToArray();
        }

        /// <summary>
        /// Returns an array of indecies where the the searched for string
        /// can be found within the container string.
        /// </summary>
        /// <param name="ToFind"></param>
        /// <param name="FindIn"></param>
        /// <param name="FindAll"></param>
        /// <param name="FirstCell"></param>
        /// <param name="Direction"></param>
        /// <param name="CapCompare"></param>
        /// <returns></returns>
        public static int[] FindIndecies(this string ToFind, string FindIn)
        {
            return FindIndecies(ToFind, FindIn, false, SearchDirection.Down, 0, StringComparison.CurrentCulture);
        }

        /// <summary>
        /// Returns an array of indecies where the the searched for string
        /// can be found within the container string.
        /// </summary>
        /// <param name="ToFind"></param>
        /// <param name="FindIn"></param>
        /// <param name="FindAll"></param>
        /// <param name="FirstCell"></param>
        /// <param name="Direction"></param>
        /// <param name="CapCompare"></param>
        /// <returns></returns>
        public static int[] FindIndecies(this string ToFind, string FindIn, StringComparison CapCompare)
        {
            return FindIndecies(ToFind, FindIn, false, SearchDirection.Down, 0, CapCompare);
        }

        /// <summary>
        /// Returns an array of indecies where the the searched for string
        /// can be found within the container string.
        /// </summary>
        /// <param name="ToFind"></param>
        /// <param name="FindIn"></param>
        /// <param name="FindAll"></param>
        /// <param name="FirstCell"></param>
        /// <param name="Direction"></param>
        /// <param name="CapCompare"></param>
        /// <returns></returns>
        public static int[] FindIndecies(this string ToFind, string FindIn, SearchDirection Direction, StringComparison CapCompare)
        {
            return FindIndecies(ToFind, FindIn, false, Direction, 0, CapCompare);
        }

        /// <summary>
        /// Returns an array of indecies where the the searched for string
        /// can be found within the container string.
        /// </summary>
        /// <param name="ToFind"></param>
        /// <param name="FindIn"></param>
        /// <param name="FindAll"></param>
        /// <param name="FirstCell"></param>
        /// <param name="Direction"></param>
        /// <param name="CapCompare"></param>
        /// <returns></returns>
        public static int[] FindIndecies(this string ToFind, string FindIn, bool FindAll, SearchDirection Direction, StringComparison CapCompare)
        {
            return FindIndecies(ToFind, FindIn, FindAll, Direction, 0, CapCompare);
        }

        /// <summary>
        /// Returns an array of indecies where the the searched for string
        /// can be found within the container string before or after a certain starting point.
        /// </summary>
        /// <param name="ToFind"></param>
        /// <param name="findin"></param>
        /// <param name="FindAll"></param>
        /// <param name="FirstCell"></param>
        /// <param name="Direction"></param>
        /// <param name="StartPoint"></param>
        /// <param name="CapCompare"></param>
        /// <returns></returns>
        public static int[] FindIndecies(this string ToFind, string findin, bool FindAll, SearchDirection Direction, int StartPoint, StringComparison CapCompare)
        {
            if (FindAll)
            {
                string FindIn = findin;
                int CurrentIndex;
                bool KeepGoing = true;
                List<int> ToReturn = new List<int>();

                if (Direction == SearchDirection.Down)
                {
                    while (KeepGoing)
                    {
                        // Find the string
                        CurrentIndex = FindIn.IndexOf(ToFind, CapCompare);

                        // Add it and loop if its possible there is another instance of the string.
                        if (CurrentIndex != -1)
                        {
                            ToReturn.Add(CurrentIndex);
                            if (FindIn.Length > CurrentIndex + 1)
                            {
                                FindIn = FindIn.Substring(CurrentIndex + 1);
                            }
                            else
                            {
                                KeepGoing = false;
                            }
                        }
                        else
                        {
                            KeepGoing = false;
                        }
                    }
                }
                else
                {
                    while (KeepGoing)
                    {
                        // Find the string
                        CurrentIndex = FindIn.LastIndexOf(ToFind, CapCompare);

                        // Add it and loop if its possible there is another instance of the string.
                        if (CurrentIndex != -1)
                        {
                            ToReturn.Add(CurrentIndex);
                            if (FindIn.Length > CurrentIndex + 1)
                            {
                                FindIn = FindIn.Substring(0, CurrentIndex);
                            }
                            else
                            {
                                KeepGoing = false;
                            }
                        }
                        else
                        {
                            KeepGoing = false;
                        }
                    }
                }
                return ToReturn.ToArray();
            }
            else
            {
                if (Direction == SearchDirection.Down)
                {
                    return new int[1] { findin.IndexOf(ToFind, StartPoint, CapCompare) };
                }
                else
                {
                    return new int[1] { findin.LastIndexOf(ToFind, StartPoint, CapCompare) };
                }
            }
        }

        public static int[] FindIndecies(this string ToFind, IList<string> FindIn)
        {
            List<int> Indecies = new List<int>();
            List<string> Copy = new List<string>(FindIn);

            int Found;
            while (true)
            {
                Found = Copy.IndexOf(ToFind);
                if (Found == -1)
                {
                    break;
                }
                Indecies.Add(Found);
                Copy.RemoveAt(Found);
            }
            return Indecies.ToArray();
        }

        /// <summary>
        /// Counts the number of the specified character
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="ToCount"></param>
        /// <returns></returns>
        public static int CountChars(this string Input, char ToCount)
        {
            int Count = 0;
            foreach (char CurrentChar in Input)
            {
                if (CurrentChar == ToCount)
                {
                    Count++;
                }
            }
            return Count;
        }

        public static int CountStrings(this string Input, string ToCount)
        {
            string Check = Input;
            int Count = 0;
            int Index;
            while (true)
            {
                Index = Check.IndexOf(ToCount);
                if (Index != -1)
                {
                    Count++;
                    Check = Check.Remove(Index, ToCount.Length);
                }
                else
                {
                    return Count;
                }
            }
        }

        public static string Remove(this string RemoveFrom, string[] Remove)
        {
            string Output = RemoveFrom;
            foreach (string Replace in Remove)
            {
                Output = Output.Replace(Replace, null);
            }
            return Output;
        }

        public static string Remove(this string RemoveFrom, char[] Remove)
        {
            string Output = RemoveFrom;
            foreach (char Replace in Remove)
            {
                Output = Output.Replace(Replace.ToString(), null);
            }
            return Output;
        }

        public static string Remove(this string RemoveFrom, string Remove)
        {
            return RemoveFrom.Remove(new string[] { Remove });
        }

        public static string Remove(this string RemoveFrom, char Remove)
        {
            return RemoveFrom.Remove(new char[] { Remove });
        }

        public static string Replace(this string RemoveFrom, Dictionary<string, string> Remove)
        {
            string Output = RemoveFrom;
            foreach (KeyValuePair<string, string> Replace in Remove)
            {
                Output = Output.Replace(Replace.Key, Replace.Value);
            }
            return Output;
        }

        public static string ToCamelCase(this string Input, char Splitter = '_')
        {
            string[] Split;
            string Output = "";

            Split = Input.Split(Splitter);
            for (int i = 0; i < Split.Length; i++)
            {
                string Capital = Split[i][0].ToString().ToUpper();
                Split[i] = Split[i].Remove(0, 1).Insert(0, Capital);
            }

            foreach (string Word in Split)
            {
                Output += Word;
            }

            return Output;
        }

        public static string[] SplitRange(this string split, string[] splitters, int start, int count)
        {
            StringBuilder LastPart = new StringBuilder();
            List<string> ToReturn = new List<string>(split.Split(splitters, StringSplitOptions.None));

            return ToReturn.GetRange(start, count).ToArray();
        }

        public static string[] SplitRange(this string split, string splitter, int lessthantotal)
        {
            StringBuilder LastPart = new StringBuilder();
            List<string> ToReturn = new List<string>(split.Split(new string[] { splitter }, StringSplitOptions.RemoveEmptyEntries));

            for (int i = 0; i < ToReturn.Count; i++)
            {
                ToReturn[i] += splitter;
            }

            return ToReturn.GetRange(0, ToReturn.Count - lessthantotal).ToArray();
        }

        public static bool TryConvertToInt(this string input, out int Result)
        {
            try
            {
                Result = input.ConvertToInt();
                return true;
            }
            catch
            {
                Result = -1;
                return false;
            }
        }

        public static int ConvertToInt(this string input)
        {
            const string Hundred = "Hundred";
            string[] UpToTwenty = { "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
            string[] Tens = { "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
            string[] Split = input.Split(' ');

            StringBuilder Builder = new StringBuilder();
            foreach (string Token in Split)
            {
                if (Token.Equals("Negative", StringComparison.InvariantCultureIgnoreCase))
                {
                    Builder.Append("-");
                }
                else if (UpToTwenty.Contains(Token, StringComparer.InvariantCultureIgnoreCase))
                {
                    Builder.Append(Array.FindIndex(UpToTwenty, t => t.Equals(Token, StringComparison.InvariantCultureIgnoreCase)) + 1);
                }
            }
            if (Builder.Length > 0)
                return int.Parse(Builder.ToString());
            else
                throw new Exception("String Unable to be Converted");
        }
    }
}
