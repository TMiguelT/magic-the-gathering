namespace MiguelUtil.Find
{
    public enum FindTab
    {
        Find,
        Replace,
        FindInFiles,
        Other
    }
}