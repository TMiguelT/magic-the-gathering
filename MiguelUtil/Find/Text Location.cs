﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MiguelUtil.Find
{
    public struct TextLocation
    {
        public int LineIndex;
        public int RelativeCharIndex;
        public int Length;

        public TextLocation(int lineindex, int relativecharindex, int length)
        {
            LineIndex = lineindex;
            RelativeCharIndex = relativecharindex;
            Length = length;
        }

        public override string ToString()
        {
            return "Line: " + LineIndex.ToString() + ", Index: " + RelativeCharIndex.ToString() + ", Length: " + Length.ToString();
        }
    }
}