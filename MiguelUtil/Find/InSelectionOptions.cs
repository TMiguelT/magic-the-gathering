﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.Find
{
    public enum InSelectionOptions
    {
        InSelection,
        NotInSelection,
        Disabled
    }
}
