﻿/*
 * Created by SharpDevelop.
 * User: Miguel
 * Date: 18/01/2010
 * Time: 6:11 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace MiguelUtil.Find
{
	partial class Find
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.Tabs = new System.Windows.Forms.TabControl();
            this.tab_find = new System.Windows.Forms.TabPage();
            this.Button_Find_FindAllInOpened = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Radio_Find_Regex = new System.Windows.Forms.RadioButton();
            this.Radio_Find_Standard = new System.Windows.Forms.RadioButton();
            this.Radio_Find_Extended = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Check_Find_Caps = new System.Windows.Forms.CheckBox();
            this.Check_Find_Selection = new System.Windows.Forms.CheckBox();
            this.Check_Find_Whole = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Radio_Find_Up = new System.Windows.Forms.RadioButton();
            this.Radio_Find_Down = new System.Windows.Forms.RadioButton();
            this.Button_Find_Close = new System.Windows.Forms.Button();
            this.Button_Find_FindAll = new System.Windows.Forms.Button();
            this.Button_Find_FindNext = new System.Windows.Forms.Button();
            this.Label_Find = new System.Windows.Forms.Label();
            this.Text_Find_FindWhat = new System.Windows.Forms.TextBox();
            this.tab_replace = new System.Windows.Forms.TabPage();
            this.Button_Replace_ReplaceAllInOpened = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Radio_Replace_Standard = new System.Windows.Forms.RadioButton();
            this.Radio_Replace_Extended = new System.Windows.Forms.RadioButton();
            this.Radio_Replace_Regex = new System.Windows.Forms.RadioButton();
            this.Group_ReplaceSettings = new System.Windows.Forms.GroupBox();
            this.Check_Replace_Caps = new System.Windows.Forms.CheckBox();
            this.Check_Replace_Whole = new System.Windows.Forms.CheckBox();
            this.Check_Replace_Selection = new System.Windows.Forms.CheckBox();
            this.Group_ReplaceDirection = new System.Windows.Forms.GroupBox();
            this.Radio_Replace_Down = new System.Windows.Forms.RadioButton();
            this.Radio_Replace_Up = new System.Windows.Forms.RadioButton();
            this.Button_Replace_Close = new System.Windows.Forms.Button();
            this.Button_Replace_ReplaceAll = new System.Windows.Forms.Button();
            this.Button_Replace_ReplaceNext = new System.Windows.Forms.Button();
            this.Label_ReplaceWith = new System.Windows.Forms.Label();
            this.Label_FindWhat = new System.Windows.Forms.Label();
            this.Text_Replace_ReplaceWith = new System.Windows.Forms.TextBox();
            this.Text_Replace_FindWhat = new System.Windows.Forms.TextBox();
            this.tab_findinfiles = new System.Windows.Forms.TabPage();
            this.Button_Files_Browse = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Text_Files_Folder = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Radio_Files_Standard = new System.Windows.Forms.RadioButton();
            this.Radio_Files_Extended = new System.Windows.Forms.RadioButton();
            this.Radio_Files_Regex = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Check_Files_Subdirectories = new System.Windows.Forms.CheckBox();
            this.Check_Files_Caps = new System.Windows.Forms.CheckBox();
            this.Check_Files_Whole = new System.Windows.Forms.CheckBox();
            this.Button_Files_Close = new System.Windows.Forms.Button();
            this.Button_ReplaceButton_Files_ReplaceAll = new System.Windows.Forms.Button();
            this.Button_Files_FindAll = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Text_Files_ReplaceWith = new System.Windows.Forms.TextBox();
            this.Text_Files_FindWhat = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Tabs.SuspendLayout();
            this.tab_find.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tab_replace.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Group_ReplaceSettings.SuspendLayout();
            this.Group_ReplaceDirection.SuspendLayout();
            this.tab_findinfiles.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tabs
            // 
            this.Tabs.Controls.Add(this.tab_find);
            this.Tabs.Controls.Add(this.tab_replace);
            this.Tabs.Controls.Add(this.tab_findinfiles);
            this.Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabs.Location = new System.Drawing.Point(0, 0);
            this.Tabs.Name = "Tabs";
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(390, 267);
            this.Tabs.TabIndex = 7;
            this.Tabs.SelectedIndexChanged += new System.EventHandler(this.Tabs_SelectedIndexChanged);
            // 
            // tab_find
            // 
            this.tab_find.Controls.Add(this.Button_Find_FindAllInOpened);
            this.tab_find.Controls.Add(this.groupBox2);
            this.tab_find.Controls.Add(this.groupBox3);
            this.tab_find.Controls.Add(this.groupBox4);
            this.tab_find.Controls.Add(this.Button_Find_Close);
            this.tab_find.Controls.Add(this.Button_Find_FindAll);
            this.tab_find.Controls.Add(this.Button_Find_FindNext);
            this.tab_find.Controls.Add(this.Label_Find);
            this.tab_find.Controls.Add(this.Text_Find_FindWhat);
            this.tab_find.Location = new System.Drawing.Point(4, 22);
            this.tab_find.Name = "tab_find";
            this.tab_find.Padding = new System.Windows.Forms.Padding(3);
            this.tab_find.Size = new System.Drawing.Size(382, 241);
            this.tab_find.TabIndex = 0;
            this.tab_find.Text = "Find";
            this.tab_find.UseVisualStyleBackColor = true;
            // 
            // Button_Find_FindAllInOpened
            // 
            this.Button_Find_FindAllInOpened.Location = new System.Drawing.Point(200, 210);
            this.Button_Find_FindAllInOpened.Name = "Button_Find_FindAllInOpened";
            this.Button_Find_FindAllInOpened.Size = new System.Drawing.Size(103, 26);
            this.Button_Find_FindAllInOpened.TabIndex = 43;
            this.Button_Find_FindAllInOpened.Text = "Find All in Opened";
            this.Button_Find_FindAllInOpened.UseVisualStyleBackColor = true;
            this.Button_Find_FindAllInOpened.Click += new System.EventHandler(this.Button_Find_FindAllInOpened_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Radio_Find_Regex);
            this.groupBox2.Controls.Add(this.Radio_Find_Standard);
            this.groupBox2.Controls.Add(this.Radio_Find_Extended);
            this.groupBox2.Location = new System.Drawing.Point(248, 132);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(127, 70);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search Mode";
            // 
            // Radio_Find_Regex
            // 
            this.Radio_Find_Regex.AutoSize = true;
            this.Radio_Find_Regex.Location = new System.Drawing.Point(7, 50);
            this.Radio_Find_Regex.Name = "Radio_Find_Regex";
            this.Radio_Find_Regex.Size = new System.Drawing.Size(116, 17);
            this.Radio_Find_Regex.TabIndex = 29;
            this.Radio_Find_Regex.Text = "Regular Expression";
            this.Radio_Find_Regex.UseVisualStyleBackColor = true;
            // 
            // Radio_Find_Standard
            // 
            this.Radio_Find_Standard.AutoSize = true;
            this.Radio_Find_Standard.Checked = true;
            this.Radio_Find_Standard.Location = new System.Drawing.Point(7, 14);
            this.Radio_Find_Standard.Name = "Radio_Find_Standard";
            this.Radio_Find_Standard.Size = new System.Drawing.Size(68, 17);
            this.Radio_Find_Standard.TabIndex = 12;
            this.Radio_Find_Standard.TabStop = true;
            this.Radio_Find_Standard.Text = "Standard";
            this.Radio_Find_Standard.UseVisualStyleBackColor = true;
            // 
            // Radio_Find_Extended
            // 
            this.Radio_Find_Extended.AutoSize = true;
            this.Radio_Find_Extended.Location = new System.Drawing.Point(7, 32);
            this.Radio_Find_Extended.Name = "Radio_Find_Extended";
            this.Radio_Find_Extended.Size = new System.Drawing.Size(119, 17);
            this.Radio_Find_Extended.TabIndex = 28;
            this.Radio_Find_Extended.Text = "Extended (\\r, \\t etc)";
            this.Radio_Find_Extended.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Check_Find_Caps);
            this.groupBox3.Controls.Add(this.Check_Find_Selection);
            this.groupBox3.Controls.Add(this.Check_Find_Whole);
            this.groupBox3.Location = new System.Drawing.Point(6, 128);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(127, 74);
            this.groupBox3.TabIndex = 41;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Settings";
            // 
            // Check_Find_Caps
            // 
            this.Check_Find_Caps.AutoSize = true;
            this.Check_Find_Caps.Location = new System.Drawing.Point(12, 54);
            this.Check_Find_Caps.Name = "Check_Find_Caps";
            this.Check_Find_Caps.Size = new System.Drawing.Size(96, 17);
            this.Check_Find_Caps.TabIndex = 11;
            this.Check_Find_Caps.Text = "Caps Sensitive";
            this.Check_Find_Caps.UseVisualStyleBackColor = true;
            // 
            // Check_Find_Selection
            // 
            this.Check_Find_Selection.AutoSize = true;
            this.Check_Find_Selection.Location = new System.Drawing.Point(12, 35);
            this.Check_Find_Selection.Name = "Check_Find_Selection";
            this.Check_Find_Selection.Size = new System.Drawing.Size(82, 17);
            this.Check_Find_Selection.TabIndex = 12;
            this.Check_Find_Selection.Text = "In Selection";
            this.Check_Find_Selection.UseVisualStyleBackColor = true;
            // 
            // Check_Find_Whole
            // 
            this.Check_Find_Whole.AutoSize = true;
            this.Check_Find_Whole.Location = new System.Drawing.Point(12, 16);
            this.Check_Find_Whole.Name = "Check_Find_Whole";
            this.Check_Find_Whole.Size = new System.Drawing.Size(110, 17);
            this.Check_Find_Whole.TabIndex = 10;
            this.Check_Find_Whole.Text = "Whole Word Only";
            this.Check_Find_Whole.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Radio_Find_Up);
            this.groupBox4.Controls.Add(this.Radio_Find_Down);
            this.groupBox4.Location = new System.Drawing.Point(139, 132);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(100, 55);
            this.groupBox4.TabIndex = 40;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Search Direction";
            // 
            // Radio_Find_Up
            // 
            this.Radio_Find_Up.AutoSize = true;
            this.Radio_Find_Up.Location = new System.Drawing.Point(25, 14);
            this.Radio_Find_Up.Name = "Radio_Find_Up";
            this.Radio_Find_Up.Size = new System.Drawing.Size(39, 17);
            this.Radio_Find_Up.TabIndex = 12;
            this.Radio_Find_Up.Text = "Up";
            this.Radio_Find_Up.UseVisualStyleBackColor = true;
            // 
            // Radio_Find_Down
            // 
            this.Radio_Find_Down.AutoSize = true;
            this.Radio_Find_Down.Checked = true;
            this.Radio_Find_Down.Location = new System.Drawing.Point(25, 33);
            this.Radio_Find_Down.Name = "Radio_Find_Down";
            this.Radio_Find_Down.Size = new System.Drawing.Size(53, 17);
            this.Radio_Find_Down.TabIndex = 28;
            this.Radio_Find_Down.TabStop = true;
            this.Radio_Find_Down.Text = "Down";
            this.Radio_Find_Down.UseVisualStyleBackColor = true;
            // 
            // Button_Find_Close
            // 
            this.Button_Find_Close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Find_Close.Location = new System.Drawing.Point(309, 210);
            this.Button_Find_Close.Name = "Button_Find_Close";
            this.Button_Find_Close.Size = new System.Drawing.Size(67, 26);
            this.Button_Find_Close.TabIndex = 39;
            this.Button_Find_Close.Text = "Close";
            this.Button_Find_Close.UseVisualStyleBackColor = true;
            this.Button_Find_Close.Click += new System.EventHandler(this.Button_Find_Close_Click);
            // 
            // Button_Find_FindAll
            // 
            this.Button_Find_FindAll.Location = new System.Drawing.Point(104, 210);
            this.Button_Find_FindAll.Name = "Button_Find_FindAll";
            this.Button_Find_FindAll.Size = new System.Drawing.Size(90, 26);
            this.Button_Find_FindAll.TabIndex = 38;
            this.Button_Find_FindAll.Text = "Find All";
            this.Button_Find_FindAll.UseVisualStyleBackColor = true;
            this.Button_Find_FindAll.Click += new System.EventHandler(this.Button_FindAll_Click);
            // 
            // Button_Find_FindNext
            // 
            this.Button_Find_FindNext.Location = new System.Drawing.Point(5, 210);
            this.Button_Find_FindNext.Name = "Button_Find_FindNext";
            this.Button_Find_FindNext.Size = new System.Drawing.Size(93, 26);
            this.Button_Find_FindNext.TabIndex = 36;
            this.Button_Find_FindNext.Text = "Find Next";
            this.Button_Find_FindNext.UseVisualStyleBackColor = true;
            this.Button_Find_FindNext.Click += new System.EventHandler(this.Button_FindNext_Click);
            // 
            // Label_Find
            // 
            this.Label_Find.Location = new System.Drawing.Point(151, 6);
            this.Label_Find.Name = "Label_Find";
            this.Label_Find.Size = new System.Drawing.Size(85, 17);
            this.Label_Find.TabIndex = 35;
            this.Label_Find.Text = "Find";
            this.Label_Find.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Text_Find_FindWhat
            // 
            this.Text_Find_FindWhat.Location = new System.Drawing.Point(55, 26);
            this.Text_Find_FindWhat.Name = "Text_Find_FindWhat";
            this.Text_Find_FindWhat.Size = new System.Drawing.Size(267, 20);
            this.Text_Find_FindWhat.TabIndex = 33;
            // 
            // tab_replace
            // 
            this.tab_replace.Controls.Add(this.Button_Replace_ReplaceAllInOpened);
            this.tab_replace.Controls.Add(this.groupBox1);
            this.tab_replace.Controls.Add(this.Group_ReplaceSettings);
            this.tab_replace.Controls.Add(this.Group_ReplaceDirection);
            this.tab_replace.Controls.Add(this.Button_Replace_Close);
            this.tab_replace.Controls.Add(this.Button_Replace_ReplaceAll);
            this.tab_replace.Controls.Add(this.Button_Replace_ReplaceNext);
            this.tab_replace.Controls.Add(this.Label_ReplaceWith);
            this.tab_replace.Controls.Add(this.Label_FindWhat);
            this.tab_replace.Controls.Add(this.Text_Replace_ReplaceWith);
            this.tab_replace.Controls.Add(this.Text_Replace_FindWhat);
            this.tab_replace.Location = new System.Drawing.Point(4, 22);
            this.tab_replace.Name = "tab_replace";
            this.tab_replace.Padding = new System.Windows.Forms.Padding(3);
            this.tab_replace.Size = new System.Drawing.Size(382, 241);
            this.tab_replace.TabIndex = 1;
            this.tab_replace.Text = "Replace";
            this.tab_replace.UseVisualStyleBackColor = true;
            // 
            // Button_Replace_ReplaceAllInOpened
            // 
            this.Button_Replace_ReplaceAllInOpened.Location = new System.Drawing.Point(200, 210);
            this.Button_Replace_ReplaceAllInOpened.Name = "Button_Replace_ReplaceAllInOpened";
            this.Button_Replace_ReplaceAllInOpened.Size = new System.Drawing.Size(122, 26);
            this.Button_Replace_ReplaceAllInOpened.TabIndex = 44;
            this.Button_Replace_ReplaceAllInOpened.Text = "Replace All in Opened";
            this.Button_Replace_ReplaceAllInOpened.UseVisualStyleBackColor = true;
            this.Button_Replace_ReplaceAllInOpened.Click += new System.EventHandler(this.Button_Replace_ReplaceAllInOpened_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Radio_Replace_Standard);
            this.groupBox1.Controls.Add(this.Radio_Replace_Extended);
            this.groupBox1.Controls.Add(this.Radio_Replace_Regex);
            this.groupBox1.Location = new System.Drawing.Point(248, 132);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(127, 70);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Mode";
            // 
            // Radio_Replace_Standard
            // 
            this.Radio_Replace_Standard.AutoSize = true;
            this.Radio_Replace_Standard.Checked = true;
            this.Radio_Replace_Standard.Location = new System.Drawing.Point(7, 14);
            this.Radio_Replace_Standard.Name = "Radio_Replace_Standard";
            this.Radio_Replace_Standard.Size = new System.Drawing.Size(68, 17);
            this.Radio_Replace_Standard.TabIndex = 12;
            this.Radio_Replace_Standard.TabStop = true;
            this.Radio_Replace_Standard.Text = "Standard";
            this.Radio_Replace_Standard.UseVisualStyleBackColor = true;
            // 
            // Radio_Replace_Extended
            // 
            this.Radio_Replace_Extended.AutoSize = true;
            this.Radio_Replace_Extended.Location = new System.Drawing.Point(7, 32);
            this.Radio_Replace_Extended.Name = "Radio_Replace_Extended";
            this.Radio_Replace_Extended.Size = new System.Drawing.Size(119, 17);
            this.Radio_Replace_Extended.TabIndex = 28;
            this.Radio_Replace_Extended.Text = "Extended (\\r, \\t etc)";
            this.Radio_Replace_Extended.UseVisualStyleBackColor = true;
            // 
            // Radio_Replace_Regex
            // 
            this.Radio_Replace_Regex.AutoSize = true;
            this.Radio_Replace_Regex.Location = new System.Drawing.Point(7, 50);
            this.Radio_Replace_Regex.Name = "Radio_Replace_Regex";
            this.Radio_Replace_Regex.Size = new System.Drawing.Size(116, 17);
            this.Radio_Replace_Regex.TabIndex = 29;
            this.Radio_Replace_Regex.Text = "Regular Expression";
            this.Radio_Replace_Regex.UseVisualStyleBackColor = true;
            // 
            // Group_ReplaceSettings
            // 
            this.Group_ReplaceSettings.Controls.Add(this.Check_Replace_Caps);
            this.Group_ReplaceSettings.Controls.Add(this.Check_Replace_Whole);
            this.Group_ReplaceSettings.Controls.Add(this.Check_Replace_Selection);
            this.Group_ReplaceSettings.Location = new System.Drawing.Point(6, 128);
            this.Group_ReplaceSettings.Name = "Group_ReplaceSettings";
            this.Group_ReplaceSettings.Size = new System.Drawing.Size(127, 74);
            this.Group_ReplaceSettings.TabIndex = 31;
            this.Group_ReplaceSettings.TabStop = false;
            this.Group_ReplaceSettings.Text = "Settings";
            // 
            // Check_Replace_Caps
            // 
            this.Check_Replace_Caps.AutoSize = true;
            this.Check_Replace_Caps.Location = new System.Drawing.Point(12, 54);
            this.Check_Replace_Caps.Name = "Check_Replace_Caps";
            this.Check_Replace_Caps.Size = new System.Drawing.Size(96, 17);
            this.Check_Replace_Caps.TabIndex = 11;
            this.Check_Replace_Caps.Text = "Caps Sensitive";
            this.Check_Replace_Caps.UseVisualStyleBackColor = true;
            // 
            // Check_Replace_Whole
            // 
            this.Check_Replace_Whole.AutoSize = true;
            this.Check_Replace_Whole.Location = new System.Drawing.Point(12, 16);
            this.Check_Replace_Whole.Name = "Check_Replace_Whole";
            this.Check_Replace_Whole.Size = new System.Drawing.Size(110, 17);
            this.Check_Replace_Whole.TabIndex = 10;
            this.Check_Replace_Whole.Text = "Whole Word Only";
            this.Check_Replace_Whole.UseVisualStyleBackColor = true;
            // 
            // Check_Replace_Selection
            // 
            this.Check_Replace_Selection.AutoSize = true;
            this.Check_Replace_Selection.Location = new System.Drawing.Point(12, 35);
            this.Check_Replace_Selection.Name = "Check_Replace_Selection";
            this.Check_Replace_Selection.Size = new System.Drawing.Size(82, 17);
            this.Check_Replace_Selection.TabIndex = 12;
            this.Check_Replace_Selection.Text = "In Selection";
            this.Check_Replace_Selection.UseVisualStyleBackColor = true;
            // 
            // Group_ReplaceDirection
            // 
            this.Group_ReplaceDirection.Controls.Add(this.Radio_Replace_Down);
            this.Group_ReplaceDirection.Controls.Add(this.Radio_Replace_Up);
            this.Group_ReplaceDirection.Location = new System.Drawing.Point(139, 132);
            this.Group_ReplaceDirection.Name = "Group_ReplaceDirection";
            this.Group_ReplaceDirection.Size = new System.Drawing.Size(100, 55);
            this.Group_ReplaceDirection.TabIndex = 30;
            this.Group_ReplaceDirection.TabStop = false;
            this.Group_ReplaceDirection.Text = "Search Direction";
            // 
            // Radio_Replace_Down
            // 
            this.Radio_Replace_Down.AutoSize = true;
            this.Radio_Replace_Down.Checked = true;
            this.Radio_Replace_Down.Location = new System.Drawing.Point(25, 33);
            this.Radio_Replace_Down.Name = "Radio_Replace_Down";
            this.Radio_Replace_Down.Size = new System.Drawing.Size(53, 17);
            this.Radio_Replace_Down.TabIndex = 28;
            this.Radio_Replace_Down.TabStop = true;
            this.Radio_Replace_Down.Text = "Down";
            this.Radio_Replace_Down.UseVisualStyleBackColor = true;
            // 
            // Radio_Replace_Up
            // 
            this.Radio_Replace_Up.AutoSize = true;
            this.Radio_Replace_Up.Location = new System.Drawing.Point(25, 14);
            this.Radio_Replace_Up.Name = "Radio_Replace_Up";
            this.Radio_Replace_Up.Size = new System.Drawing.Size(39, 17);
            this.Radio_Replace_Up.TabIndex = 12;
            this.Radio_Replace_Up.Text = "Up";
            this.Radio_Replace_Up.UseVisualStyleBackColor = true;
            // 
            // Button_Replace_Close
            // 
            this.Button_Replace_Close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Replace_Close.Location = new System.Drawing.Point(328, 210);
            this.Button_Replace_Close.Name = "Button_Replace_Close";
            this.Button_Replace_Close.Size = new System.Drawing.Size(48, 26);
            this.Button_Replace_Close.TabIndex = 15;
            this.Button_Replace_Close.Text = "Close";
            this.Button_Replace_Close.UseVisualStyleBackColor = true;
            this.Button_Replace_Close.Click += new System.EventHandler(this.Button_Find_Close_Click);
            // 
            // Button_Replace_ReplaceAll
            // 
            this.Button_Replace_ReplaceAll.Location = new System.Drawing.Point(104, 210);
            this.Button_Replace_ReplaceAll.Name = "Button_Replace_ReplaceAll";
            this.Button_Replace_ReplaceAll.Size = new System.Drawing.Size(90, 26);
            this.Button_Replace_ReplaceAll.TabIndex = 14;
            this.Button_Replace_ReplaceAll.Text = "Replace All";
            this.Button_Replace_ReplaceAll.UseVisualStyleBackColor = true;
            this.Button_Replace_ReplaceAll.Click += new System.EventHandler(this.Button_ReplaceAll_Click);
            // 
            // Button_Replace_ReplaceNext
            // 
            this.Button_Replace_ReplaceNext.Location = new System.Drawing.Point(5, 210);
            this.Button_Replace_ReplaceNext.Name = "Button_Replace_ReplaceNext";
            this.Button_Replace_ReplaceNext.Size = new System.Drawing.Size(93, 26);
            this.Button_Replace_ReplaceNext.TabIndex = 13;
            this.Button_Replace_ReplaceNext.Text = "Replace Next";
            this.Button_Replace_ReplaceNext.UseVisualStyleBackColor = true;
            this.Button_Replace_ReplaceNext.Click += new System.EventHandler(this.Button_ReplaceNext_Click);
            // 
            // Label_ReplaceWith
            // 
            this.Label_ReplaceWith.Location = new System.Drawing.Point(151, 51);
            this.Label_ReplaceWith.Name = "Label_ReplaceWith";
            this.Label_ReplaceWith.Size = new System.Drawing.Size(85, 14);
            this.Label_ReplaceWith.TabIndex = 13;
            this.Label_ReplaceWith.Text = "Replace With";
            this.Label_ReplaceWith.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label_FindWhat
            // 
            this.Label_FindWhat.Location = new System.Drawing.Point(151, 6);
            this.Label_FindWhat.Name = "Label_FindWhat";
            this.Label_FindWhat.Size = new System.Drawing.Size(85, 17);
            this.Label_FindWhat.TabIndex = 12;
            this.Label_FindWhat.Text = "Find";
            this.Label_FindWhat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Text_Replace_ReplaceWith
            // 
            this.Text_Replace_ReplaceWith.Location = new System.Drawing.Point(55, 68);
            this.Text_Replace_ReplaceWith.Name = "Text_Replace_ReplaceWith";
            this.Text_Replace_ReplaceWith.Size = new System.Drawing.Size(267, 20);
            this.Text_Replace_ReplaceWith.TabIndex = 9;
            // 
            // Text_Replace_FindWhat
            // 
            this.Text_Replace_FindWhat.Location = new System.Drawing.Point(55, 26);
            this.Text_Replace_FindWhat.Name = "Text_Replace_FindWhat";
            this.Text_Replace_FindWhat.Size = new System.Drawing.Size(267, 20);
            this.Text_Replace_FindWhat.TabIndex = 8;
            // 
            // tab_findinfiles
            // 
            this.tab_findinfiles.Controls.Add(this.Button_Files_Browse);
            this.tab_findinfiles.Controls.Add(this.label3);
            this.tab_findinfiles.Controls.Add(this.Text_Files_Folder);
            this.tab_findinfiles.Controls.Add(this.groupBox5);
            this.tab_findinfiles.Controls.Add(this.groupBox6);
            this.tab_findinfiles.Controls.Add(this.Button_Files_Close);
            this.tab_findinfiles.Controls.Add(this.Button_ReplaceButton_Files_ReplaceAll);
            this.tab_findinfiles.Controls.Add(this.Button_Files_FindAll);
            this.tab_findinfiles.Controls.Add(this.label1);
            this.tab_findinfiles.Controls.Add(this.Text_Files_ReplaceWith);
            this.tab_findinfiles.Controls.Add(this.Text_Files_FindWhat);
            this.tab_findinfiles.Controls.Add(this.label2);
            this.tab_findinfiles.Location = new System.Drawing.Point(4, 22);
            this.tab_findinfiles.Name = "tab_findinfiles";
            this.tab_findinfiles.Size = new System.Drawing.Size(382, 241);
            this.tab_findinfiles.TabIndex = 2;
            this.tab_findinfiles.Text = "Find in Files";
            this.tab_findinfiles.UseVisualStyleBackColor = true;
            // 
            // Button_Files_Browse
            // 
            this.Button_Files_Browse.Location = new System.Drawing.Point(325, 109);
            this.Button_Files_Browse.Name = "Button_Files_Browse";
            this.Button_Files_Browse.Size = new System.Drawing.Size(50, 20);
            this.Button_Files_Browse.TabIndex = 45;
            this.Button_Files_Browse.Text = "Browse";
            this.Button_Files_Browse.UseVisualStyleBackColor = true;
            this.Button_Files_Browse.Click += new System.EventHandler(this.Button_Files_Browse_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(149, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 14);
            this.label3.TabIndex = 44;
            this.label3.Text = "Find In";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Text_Files_Folder
            // 
            this.Text_Files_Folder.Location = new System.Drawing.Point(55, 110);
            this.Text_Files_Folder.Name = "Text_Files_Folder";
            this.Text_Files_Folder.Size = new System.Drawing.Size(267, 20);
            this.Text_Files_Folder.TabIndex = 43;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Radio_Files_Standard);
            this.groupBox5.Controls.Add(this.Radio_Files_Extended);
            this.groupBox5.Controls.Add(this.Radio_Files_Regex);
            this.groupBox5.Location = new System.Drawing.Point(248, 132);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(127, 70);
            this.groupBox5.TabIndex = 42;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Search Mode";
            // 
            // Radio_Files_Standard
            // 
            this.Radio_Files_Standard.AutoSize = true;
            this.Radio_Files_Standard.Checked = true;
            this.Radio_Files_Standard.Location = new System.Drawing.Point(7, 14);
            this.Radio_Files_Standard.Name = "Radio_Files_Standard";
            this.Radio_Files_Standard.Size = new System.Drawing.Size(68, 17);
            this.Radio_Files_Standard.TabIndex = 12;
            this.Radio_Files_Standard.TabStop = true;
            this.Radio_Files_Standard.Text = "Standard";
            this.Radio_Files_Standard.UseVisualStyleBackColor = true;
            // 
            // Radio_Files_Extended
            // 
            this.Radio_Files_Extended.AutoSize = true;
            this.Radio_Files_Extended.Location = new System.Drawing.Point(7, 32);
            this.Radio_Files_Extended.Name = "Radio_Files_Extended";
            this.Radio_Files_Extended.Size = new System.Drawing.Size(119, 17);
            this.Radio_Files_Extended.TabIndex = 28;
            this.Radio_Files_Extended.Text = "Extended (\\r, \\t etc)";
            this.Radio_Files_Extended.UseVisualStyleBackColor = true;
            // 
            // Radio_Files_Regex
            // 
            this.Radio_Files_Regex.AutoSize = true;
            this.Radio_Files_Regex.Location = new System.Drawing.Point(7, 50);
            this.Radio_Files_Regex.Name = "Radio_Files_Regex";
            this.Radio_Files_Regex.Size = new System.Drawing.Size(116, 17);
            this.Radio_Files_Regex.TabIndex = 29;
            this.Radio_Files_Regex.Text = "Regular Expression";
            this.Radio_Files_Regex.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.Check_Files_Subdirectories);
            this.groupBox6.Controls.Add(this.Check_Files_Caps);
            this.groupBox6.Controls.Add(this.Check_Files_Whole);
            this.groupBox6.Location = new System.Drawing.Point(6, 128);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(127, 74);
            this.groupBox6.TabIndex = 41;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Settings";
            // 
            // Check_Files_Subdirectories
            // 
            this.Check_Files_Subdirectories.AutoSize = true;
            this.Check_Files_Subdirectories.Location = new System.Drawing.Point(12, 35);
            this.Check_Files_Subdirectories.Name = "Check_Files_Subdirectories";
            this.Check_Files_Subdirectories.Size = new System.Drawing.Size(105, 17);
            this.Check_Files_Subdirectories.TabIndex = 48;
            this.Check_Files_Subdirectories.Text = "In Subdirectories";
            this.Check_Files_Subdirectories.UseVisualStyleBackColor = true;
            // 
            // Check_Files_Caps
            // 
            this.Check_Files_Caps.AutoSize = true;
            this.Check_Files_Caps.Location = new System.Drawing.Point(12, 54);
            this.Check_Files_Caps.Name = "Check_Files_Caps";
            this.Check_Files_Caps.Size = new System.Drawing.Size(96, 17);
            this.Check_Files_Caps.TabIndex = 11;
            this.Check_Files_Caps.Text = "Caps Sensitive";
            this.Check_Files_Caps.UseVisualStyleBackColor = true;
            // 
            // Check_Files_Whole
            // 
            this.Check_Files_Whole.AutoSize = true;
            this.Check_Files_Whole.Location = new System.Drawing.Point(12, 16);
            this.Check_Files_Whole.Name = "Check_Files_Whole";
            this.Check_Files_Whole.Size = new System.Drawing.Size(110, 17);
            this.Check_Files_Whole.TabIndex = 10;
            this.Check_Files_Whole.Text = "Whole Word Only";
            this.Check_Files_Whole.UseVisualStyleBackColor = true;
            // 
            // Button_Files_Close
            // 
            this.Button_Files_Close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Files_Close.Location = new System.Drawing.Point(295, 209);
            this.Button_Files_Close.Name = "Button_Files_Close";
            this.Button_Files_Close.Size = new System.Drawing.Size(81, 26);
            this.Button_Files_Close.TabIndex = 39;
            this.Button_Files_Close.Text = "Close";
            this.Button_Files_Close.UseVisualStyleBackColor = true;
            this.Button_Files_Close.Click += new System.EventHandler(this.Button_Find_Close_Click);
            // 
            // Button_ReplaceButton_Files_ReplaceAll
            // 
            this.Button_ReplaceButton_Files_ReplaceAll.Location = new System.Drawing.Point(103, 209);
            this.Button_ReplaceButton_Files_ReplaceAll.Name = "Button_ReplaceButton_Files_ReplaceAll";
            this.Button_ReplaceButton_Files_ReplaceAll.Size = new System.Drawing.Size(93, 26);
            this.Button_ReplaceButton_Files_ReplaceAll.TabIndex = 38;
            this.Button_ReplaceButton_Files_ReplaceAll.Text = "Replace All";
            this.Button_ReplaceButton_Files_ReplaceAll.UseVisualStyleBackColor = true;
            this.Button_ReplaceButton_Files_ReplaceAll.Click += new System.EventHandler(this.Button_ReplaceButton_Files_ReplaceAll_Click);
            // 
            // Button_Files_FindAll
            // 
            this.Button_Files_FindAll.Location = new System.Drawing.Point(5, 209);
            this.Button_Files_FindAll.Name = "Button_Files_FindAll";
            this.Button_Files_FindAll.Size = new System.Drawing.Size(93, 26);
            this.Button_Files_FindAll.TabIndex = 36;
            this.Button_Files_FindAll.Text = "Find All";
            this.Button_Files_FindAll.UseVisualStyleBackColor = true;
            this.Button_Files_FindAll.Click += new System.EventHandler(this.Button_Files_FindAll_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(151, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 14);
            this.label1.TabIndex = 37;
            this.label1.Text = "Replace With";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Text_Files_ReplaceWith
            // 
            this.Text_Files_ReplaceWith.Location = new System.Drawing.Point(55, 68);
            this.Text_Files_ReplaceWith.Name = "Text_Files_ReplaceWith";
            this.Text_Files_ReplaceWith.Size = new System.Drawing.Size(267, 20);
            this.Text_Files_ReplaceWith.TabIndex = 34;
            // 
            // Text_Files_FindWhat
            // 
            this.Text_Files_FindWhat.Location = new System.Drawing.Point(55, 26);
            this.Text_Files_FindWhat.Name = "Text_Files_FindWhat";
            this.Text_Files_FindWhat.Size = new System.Drawing.Size(267, 20);
            this.Text_Files_FindWhat.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(151, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 17);
            this.label2.TabIndex = 35;
            this.label2.Text = "Find";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Find
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 267);
            this.Controls.Add(this.Tabs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Find";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Find";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Find_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Replace_KeyDown);
            this.Tabs.ResumeLayout(false);
            this.tab_find.ResumeLayout(false);
            this.tab_find.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tab_replace.ResumeLayout(false);
            this.tab_replace.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Group_ReplaceSettings.ResumeLayout(false);
            this.Group_ReplaceSettings.PerformLayout();
            this.Group_ReplaceDirection.ResumeLayout(false);
            this.Group_ReplaceDirection.PerformLayout();
            this.tab_findinfiles.ResumeLayout(false);
            this.tab_findinfiles.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        private System.Windows.Forms.TabControl Tabs;
        private System.Windows.Forms.TabPage tab_find;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton Radio_Find_Regex;
        private System.Windows.Forms.RadioButton Radio_Find_Standard;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox Check_Find_Caps;
        private System.Windows.Forms.CheckBox Check_Find_Selection;
        private System.Windows.Forms.CheckBox Check_Find_Whole;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton Radio_Find_Up;
        private System.Windows.Forms.RadioButton Radio_Find_Down;
        private System.Windows.Forms.Button Button_Find_Close;
        private System.Windows.Forms.Button Button_Find_FindAll;
        private System.Windows.Forms.Button Button_Find_FindNext;
        private System.Windows.Forms.Label Label_Find;
        private System.Windows.Forms.TextBox Text_Find_FindWhat;
        private System.Windows.Forms.RadioButton Radio_Find_Extended;
        private System.Windows.Forms.TabPage tab_findinfiles;
        private System.Windows.Forms.TabPage tab_replace;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox Group_ReplaceSettings;
        private System.Windows.Forms.GroupBox Group_ReplaceDirection;
        private System.Windows.Forms.Button Button_Replace_Close;
        private System.Windows.Forms.Button Button_Replace_ReplaceAll;
        private System.Windows.Forms.Button Button_Replace_ReplaceNext;
        private System.Windows.Forms.Label Label_ReplaceWith;
        private System.Windows.Forms.Label Label_FindWhat;
        private System.Windows.Forms.TextBox Text_Replace_ReplaceWith;
        private System.Windows.Forms.TextBox Text_Replace_FindWhat;
        private System.Windows.Forms.RadioButton Radio_Replace_Down;
        private System.Windows.Forms.RadioButton Radio_Replace_Up;
        private System.Windows.Forms.CheckBox Check_Replace_Whole;
        private System.Windows.Forms.CheckBox Check_Replace_Selection;
        private System.Windows.Forms.CheckBox Check_Replace_Caps;
        private System.Windows.Forms.RadioButton Radio_Replace_Extended;
        private System.Windows.Forms.RadioButton Radio_Replace_Standard;
        private System.Windows.Forms.RadioButton Radio_Replace_Regex;
        private System.Windows.Forms.CheckBox Check_Files_Subdirectories;
        private System.Windows.Forms.Button Button_Files_Browse;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Text_Files_Folder;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton Radio_Files_Standard;
        private System.Windows.Forms.RadioButton Radio_Files_Extended;
        private System.Windows.Forms.RadioButton Radio_Files_Regex;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox Check_Files_Caps;
        private System.Windows.Forms.CheckBox Check_Files_Whole;
        private System.Windows.Forms.Button Button_Files_Close;
        private System.Windows.Forms.Button Button_ReplaceButton_Files_ReplaceAll;
        private System.Windows.Forms.Button Button_Files_FindAll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Text_Files_ReplaceWith;
        private System.Windows.Forms.TextBox Text_Files_FindWhat;
        private System.Windows.Forms.Button Button_Find_FindAllInOpened;
        private System.Windows.Forms.Button Button_Replace_ReplaceAllInOpened;
	}
}
