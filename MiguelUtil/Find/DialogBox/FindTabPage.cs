﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MiguelUtil.Find.DialogBox
{
    public abstract class FindTabPage : TabPage
    {
        public abstract FindOptions Options { get; }
        public abstract void SetFromOptions(FindOptions options);
    }
}
