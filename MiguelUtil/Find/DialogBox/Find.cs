/*
 * Created by SharpDevelop.
 * User: Miguel
 * Date: 18/01/2010
 * Time: 6:11 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using MiguelUtil;
using System.IO;
using MiguelUtil.Find.DialogBox;
using System.Collections;

namespace MiguelUtil.Find
{
    /// <summary>
    /// Description of Replace.
    /// </summary>
    public partial class Find : Form
    {
        public Form MainForm;

        bool FloatingMode = false;
        Action<FindOptions> CallbackFunction;

        FindTab Initial;
        bool HasOtherTabs = false;
        FindTabPage[] ExtraTabs;

        InSelectionOptions HasSelection = InSelectionOptions.Disabled;
        string ToFind = "";
        string ToReplace = "";
        bool InSelection = false;
        bool MatchCase = false;
        bool WholeWord = false;
        SearchDirection Direction = SearchDirection.Down;
        FindMode SearchMode = FindMode.Standard;
        FindType FindType = FindType.FindNext;
        string FindFolder = "";
        bool Subdirectories = true;
        public FindOptions CombinedOptions;

        public Find(FindTab StartTab, InSelectionOptions hasselection)
        {
            InitializeComponent();
            HasSelection = hasselection;
            Initial = StartTab;
        }

        public Find(FindTab StartTab, InSelectionOptions hasselection, FindOptions previousoptions)
            : this(StartTab, hasselection)
        {
            LoadFromData(previousoptions);
        }

        public Find(FindTab StartTab, InSelectionOptions hasselection, params FindTabPage[] NewTab)
            : this(FindTab.Other, hasselection)
        {
            AddNewTabs(NewTab);
        }

        public Find(FindTab StartTab, InSelectionOptions hasselection, FindOptions previousoptions, params FindTabPage[] NewTab)
            : this(FindTab.Other, hasselection)
        {
            AddNewTabs(NewTab);
            LoadFromData(previousoptions);
        }

        public Find(Action<FindOptions> Callback, FindTab StartTab, InSelectionOptions hasselection)
        {
            InitializeComponent();
            FloatingMode = true;
            CallbackFunction = Callback;
            HasSelection = hasselection;
            Initial = StartTab;
        }

        public Find(Action<FindOptions> Callback, FindTab StartTab, InSelectionOptions hasselection, FindOptions previousoptions)
            : this(Callback, StartTab, hasselection)
        {
            LoadFromData(previousoptions);
        }

        public Find(Action<FindOptions> Callback, FindTab StartTab, InSelectionOptions hasselection, params FindTabPage[] NewTab)
            : this(Callback, StartTab, hasselection)
        {
            AddNewTabs(NewTab);
        }

        public Find(Action<FindOptions> Callback, FindTab StartTab, InSelectionOptions hasselection, FindOptions previousoptions, params FindTabPage[] NewTab)
            : this(Callback, StartTab, hasselection)
        {
            AddNewTabs(NewTab);
            LoadFromData(previousoptions);
        }

        private void AddNewTabs(FindTabPage[] extratabs)
        {
            HasOtherTabs = true;
            ExtraTabs = extratabs;
            foreach (FindTabPage extratab in extratabs)
                Tabs.TabPages.Add(extratab);
        }

        public void LoadFromData(FindOptions options)
        {
            Check_Files_Caps.Checked = Check_Replace_Caps.Checked = Check_Find_Caps.Checked = options.CaseSensitive;
            Check_Files_Whole.Checked = Check_Find_Whole.Checked = Check_Replace_Whole.Checked = options.WholeWord;
            Radio_Find_Down.Checked = Radio_Replace_Down.Checked = options.Direction == SearchDirection.Down;
            Radio_Find_Up.Checked = Radio_Replace_Up.Checked = options.Direction == SearchDirection.Up;
            Text_Files_FindWhat.Text = Text_Find_FindWhat.Text = Text_Replace_FindWhat.Text = options.Find;
            Text_Files_ReplaceWith.Text = Text_Replace_ReplaceWith.Text = options.Replace;
            Radio_Files_Standard.Checked = Radio_Find_Standard.Checked = Radio_Replace_Standard.Checked = options.Mode == FindMode.Standard;
            Radio_Files_Extended.Checked = Radio_Find_Extended.Checked = Radio_Replace_Extended.Checked = options.Mode == FindMode.Extended;
            Radio_Files_Regex.Checked = Radio_Find_Regex.Checked = Radio_Replace_Regex.Checked = options.Mode == FindMode.RegularExpression;
            Check_Files_Subdirectories.Checked = options.Subdirectories;
            Text_Files_Folder.Text = options.FindFolder;

            if (HasOtherTabs)
                foreach (FindTabPage page in ExtraTabs)
                    page.SetFromOptions(options);

            switch (HasSelection)
            {
                case InSelectionOptions.Disabled:
                    Check_Find_Selection.Enabled = Check_Find_Selection.Checked = Check_Replace_Selection.Enabled = Check_Replace_Selection.Checked = false;
                    break;

                case InSelectionOptions.InSelection:
                    Check_Find_Selection.Enabled = Check_Find_Selection.Checked = Check_Replace_Selection.Enabled = Check_Replace_Selection.Checked = true;
                    break;

                case InSelectionOptions.NotInSelection:
                    Check_Find_Selection.Enabled = Check_Replace_Selection.Enabled = true;
                    Check_Find_Selection.Checked = Check_Replace_Selection.Checked = false;
                    break;
            }

            if (Initial == FindTab.Find)
            {
                Tabs.SelectedTab = tab_find;
                Text_Find_FindWhat.Select();
            }
            else if (Initial == FindTab.Replace)
            {
                Tabs.SelectedTab = tab_replace;
                Text_Replace_FindWhat.Select();
            }
            else if (Initial == FindTab.FindInFiles)
            {
                Tabs.SelectedTab = tab_findinfiles;
                Text_Files_FindWhat.Select();
            }
        }

        public void CustomTabResult(DialogResult result, bool hide)
        {
            FindType = MiguelUtil.Find.FindType.Other;
            SetData(result);
            if (hide)
                Hide();
        }

        private void SetData(DialogResult Result = System.Windows.Forms.DialogResult.OK)
        {
            if (Tabs.SelectedTab == tab_find)
            {
                Direction = Radio_Find_Up.Checked ? SearchDirection.Up : SearchDirection.Down;

                if (Radio_Find_Standard.Checked)
                    SearchMode = FindMode.Standard;
                else if (Radio_Find_Extended.Checked)
                    SearchMode = FindMode.Extended;
                else if (Radio_Find_Regex.Checked)
                    SearchMode = FindMode.RegularExpression;

                MatchCase = Check_Find_Caps.Checked;
                InSelection = Check_Find_Selection.Checked;
                ToFind = Text_Find_FindWhat.Text;
                WholeWord = Check_Find_Whole.Checked;
            }
            else if (Tabs.SelectedTab == tab_replace)
            {
                Direction = Radio_Replace_Up.Checked ? SearchDirection.Up : SearchDirection.Down;

                if (Radio_Replace_Standard.Checked)
                    SearchMode = FindMode.Standard;
                else if (Radio_Replace_Extended.Checked)
                    SearchMode = FindMode.Extended;
                else if (Radio_Replace_Regex.Checked)
                    SearchMode = FindMode.RegularExpression;

                MatchCase = Check_Replace_Caps.Checked;
                InSelection = Check_Replace_Selection.Checked;
                ToFind = Text_Replace_FindWhat.Text;
                ToReplace = Text_Replace_ReplaceWith.Text;
                WholeWord = Check_Replace_Whole.Checked;
            }
            else if (Tabs.SelectedTab == tab_findinfiles)
            {
                if (Radio_Files_Standard.Checked)
                    SearchMode = FindMode.Standard;
                else if (Radio_Files_Extended.Checked)
                    SearchMode = FindMode.Extended;
                else if (Radio_Files_Regex.Checked)
                    SearchMode = FindMode.RegularExpression;

                MatchCase = Check_Files_Caps.Checked;
                ToFind = Text_Files_FindWhat.Text;
                ToReplace = Text_Files_ReplaceWith.Text;
                WholeWord = Check_Files_Whole.Checked;
                Subdirectories = Check_Files_Subdirectories.Checked;
                FindFolder = Text_Files_Folder.Text;
            }
            else
            {
                FindTabPage OtherTab = (FindTabPage)Tabs.SelectedTab;
                CombinedOptions = OtherTab.Options;
                goto End;
            }

            CombinedOptions = new FindOptions(InSelection, WholeWord, MatchCase, Direction, SearchMode, FindType, ToFind, ToReplace, FindFolder, Subdirectories);
        End: if (FloatingMode)
                CallbackFunction(CombinedOptions);
            else
                DialogResult = Result;
        }

        private void Replace_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                if (FloatingMode)
                    Hide();
                else
                    SetData(DialogResult.Cancel);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                if (Tabs.SelectedIndex == 0)
                    Button_FindNext_Click(Button_Find_FindNext, new EventArgs());
                else if (Tabs.SelectedIndex == 1)
                    Button_ReplaceNext_Click(Button_Replace_ReplaceNext, new EventArgs());
                else
                    Button_Files_FindAll_Click(Button_Files_FindAll, new EventArgs());
            }
        }

        private void Tabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            Text = Tabs.SelectedTab.Text;
            if (Tabs.SelectedTab == tab_find)
            {
                Text_Find_FindWhat.Text = Text_Replace_FindWhat.Text;
                Check_Find_Selection.Checked = Check_Replace_Whole.Checked;
                Check_Find_Caps.Checked = Check_Replace_Caps.Checked;

                Radio_Find_Standard.Checked = Radio_Replace_Standard.Checked;
                Radio_Find_Extended.Checked = Radio_Replace_Extended.Checked;
                Radio_Find_Regex.Checked = Radio_Replace_Regex.Checked;

                Radio_Find_Down.Checked = Radio_Replace_Down.Checked;
                Radio_Find_Up.Checked = Radio_Replace_Up.Checked;

                Text_Find_FindWhat.Select();
            }
            else if (Tabs.SelectedTab == tab_replace)
            {
                Text_Replace_FindWhat.Text = Text_Find_FindWhat.Text;
                Check_Replace_Whole.Checked = Check_Find_Selection.Checked;
                Check_Replace_Caps.Checked = Check_Find_Caps.Checked;

                Radio_Replace_Standard.Checked = Radio_Find_Standard.Checked;
                Radio_Replace_Extended.Checked = Radio_Find_Extended.Checked;
                Radio_Replace_Regex.Checked = Radio_Find_Regex.Checked;

                Radio_Replace_Down.Checked = Radio_Find_Down.Checked;
                Radio_Replace_Up.Checked = Radio_Find_Up.Checked;
                Text_Replace_FindWhat.Select();
            }
        }

        private void Button_ReplaceNext_Click(object sender, EventArgs e)
        {
            FindType = FindType.ReplaceNext;
            SetData();
        }

        private void Button_ReplaceAll_Click(object sender, EventArgs e)
        {
            FindType = FindType.ReplaceAll;
            SetData();
            Hide();
        }

        private void Button_FindNext_Click(object sender, EventArgs e)
        {
            FindType = FindType.FindNext;
            SetData();
        }

        private void Button_FindAll_Click(object sender, EventArgs e)
        {
            FindType = FindType.FindAll;
            SetData();
            Hide();
        }

        private void Button_Files_Browse_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog OpenFolder = new FolderBrowserDialog())
            {
                if (OpenFolder.ShowDialog() == DialogResult.OK)
                {
                    Text_Files_Folder.Text = OpenFolder.SelectedPath;
                }
            }
        }

        private void Button_Find_Close_Click(object sender, EventArgs e)
        {
            if (FloatingMode)
                Hide();
            else
                DialogResult = DialogResult.Cancel;
        }

        private void Button_Files_FindAll_Click(object sender, EventArgs e)
        {
            if (CheckFilesSettings())
            {
                FindType = FindType.FindAllInFiles;
                SetData();
                Hide();
            }
        }

        private void Button_ReplaceButton_Files_ReplaceAll_Click(object sender, EventArgs e)
        {
            if (CheckFilesSettings())
            {
                FindType = FindType.ReplaceAllInFiles;
                SetData();
                Hide();
            }
        }

        private bool CheckFilesSettings()
        {
            if (Directory.Exists(Text_Files_Folder.Text))
                return true;
            return false;
        }

        private void Find_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FloatingMode)
            {
                e.Cancel = true;
                Hide();
            }
        }

        private void Button_Find_FindAllInOpened_Click(object sender, EventArgs e)
        {
            FindType = FindType.FindAllInOpened;
            SetData();
            Hide();
        }

        private void Button_Replace_ReplaceAllInOpened_Click(object sender, EventArgs e)
        {
            FindType = FindType.ReplaceAllInOpened;
            SetData();
            Hide();
        }
    }
}
