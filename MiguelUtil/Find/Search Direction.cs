using System;
using System.Collections.Generic;
using System.Text;

namespace MiguelUtil.Find
{
    [Serializable]
    public enum SearchDirection
    {
        Up,
        Down
    }
}
