using System;
namespace MiguelUtil.Find
{
    [Serializable]
    public enum FindMode
    {
        Standard,
        Extended,
        RegularExpression
    }
}