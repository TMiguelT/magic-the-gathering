﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MiguelUtil.Find;

namespace MiguelUtil.Find
{
    public interface ISearchResult
    {
        /// <summary>
        /// Selects the search result in the associated control.
        /// </summary>
        /// <returns>A bool determining if the result was able to be selected.</returns>
        bool SelectResult();

        /// <summary>
        /// Replaces the search result with the data from the findoptions
        /// </summary>
        void ReplaceResult(FindOptions replace);

    }
}
