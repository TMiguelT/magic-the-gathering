﻿using System;
namespace MiguelUtil.Find
{
    [Serializable]
    public enum FindType
    {
        FindNext,
        FindAll,
        ReplaceNext,
        ReplaceAll,
        FindAllInOpened,
        ReplaceAllInOpened,
        FindAllInFiles,
        ReplaceAllInFiles,
        Other
    }
}