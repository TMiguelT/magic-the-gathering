﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using MiguelUtil.Controls;
using MiguelUtil.Find;

namespace MiguelUtil.Find.ResultTypes
{
    public class TextBoxSearchResult : ISearchResult
    {
        public TextLocation Position;
        public TextBoxBase Control;

        public TextBoxSearchResult(TextBoxBase control, Match match, bool InSelection = false)
        {
            Control = control;
            if (InSelection)
            {
                int AbsoluteIndex = match.Index + control.SelectionStart;
                int AbsoluteLine = control.GetLineFromCharIndex(AbsoluteIndex);
                Position = new TextLocation(AbsoluteLine, AbsoluteIndex - control.GetFirstCharIndexFromLine(AbsoluteLine), match.Length);
            }
            else
            {
                int Line = control.GetLineFromCharIndex(match.Index);
                Position = new TextLocation(Line, match.Index - control.GetFirstCharIndexFromLine(Line), match.Length);
            }
        }

        public TextBoxSearchResult(TextBoxBase control, string line, int findlength, int lineindex, int startlocation)
        {
            Control = control;
            Position = new TextLocation(lineindex, startlocation, findlength);
        }

        public bool SelectResult()
        {
            if (Control is RichTextBoxEx)
                ((RichTextBoxEx)Control).Select(Control.GetFirstCharIndexFromLine(Position.LineIndex) + Position.RelativeCharIndex, Position.Length, true);
            else
                Control.Select(Control.GetFirstCharIndexFromLine(Position.LineIndex) + Position.RelativeCharIndex, Position.Length);
            return true;
        }

        public void ReplaceResult(FindOptions replace)
        {
            if (replace.FindReplace == FindType.ReplaceNext)
            {
                Control.Select(Control.GetFirstCharIndexFromLine(Position.LineIndex) + Position.RelativeCharIndex, Position.Length);
                Control.SelectedText = replace.Replace;
                Control.Select(Control.GetFirstCharIndexFromLine(Position.LineIndex) + Position.RelativeCharIndex, replace.Replace.Length);
            }
            else if (replace.FindReplace == FindType.ReplaceAll)
            {
                Control.SuspendLayout();
                int SelectionStart = Control.SelectionStart;
                int SelectionLength = Control.SelectionLength;
                Control.Select(Control.GetFirstCharIndexFromLine(Position.LineIndex) + Position.RelativeCharIndex, Position.Length);
                Control.SelectedText = replace.Replace;
                Control.Select(SelectionStart, SelectionLength);
                Control.ResumeLayout();
            }
            else
                throw new Exception("Invalid find options for a replace.");
        }
    }
}
