﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MiguelUtil.Find;
using System.Windows.Forms;

namespace MiguelUtil.Find.ResultTypes
{
    public class TreeSearchResult : ISearchResult
    {
        public TreeNode Position;

        public TreeSearchResult(TreeNode node)
        {
            Position = node;
        }

        public bool SelectResult()
        {
            if (Position.TreeView != null)
            {
                Position.TreeView.SelectedNode = Position;
                return true;
            }
            else
                return false;
        }

        public void ReplaceResult(FindOptions replace)
        {
            Position.Text = replace.Replace;
        }
    }
}
