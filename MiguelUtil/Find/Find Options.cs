using System;
using System.Text.RegularExpressions;
using StringExtensions;
using System.IO;
using System.Xml.Serialization;

namespace MiguelUtil.Find
{
    [Serializable]
    public class FindOptions
    {
        public bool InSelection;

        public bool WholeWord;

        public bool CaseSensitive;

        public SearchDirection Direction;

        public FindMode Mode;

        public FindType FindReplace;

        public string Find;

        public string Replace;

        public string FindFolder;

        public bool Subdirectories;

        public object OtherOptions;

        public FindOptions()
        {
            Direction = SearchDirection.Down;
            Mode = FindMode.Standard;
            Find = "";
            Replace = "";
        }

        public FindOptions(bool inselection, bool wholeword, bool casesensitive, SearchDirection direction, FindMode mode, FindType findreplace, string find, string replace, string findfolder = "", bool subdirectories = false)
        {
            InSelection = inselection;
            WholeWord = wholeword;
            CaseSensitive = casesensitive;
            Direction = direction;
            Mode = mode;
            Find = find;
            Replace = replace;
            FindReplace = findreplace;
            FindFolder = findfolder;
            Subdirectories = subdirectories;
        }

        public StringComparison MatchCaseEnum
        {
            get
            {
                return CaseSensitive ? StringComparison.CurrentCulture : StringComparison.CurrentCultureIgnoreCase;
            }
        }

        public bool All
        {
            get
            {
                return FindReplace == FindType.ReplaceAll || FindReplace == FindType.FindAll;
            }
        }

        public SearchOption Option
        {
            get
            {
                return Subdirectories ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            }
        }

        /// <summary>
        /// Returns a Regex that contains the search string, FindMode, WholeWord, CaseSensitive and Direction.
        /// </summary>
        public Regex CompiledRegex
        {
            get
            {
                // Setup search pattern
                string compiled = Find;

                if (Mode == FindMode.Extended)
                {
                    compiled = compiled.EscapeExtended();
                }
                else if (Mode == FindMode.Standard)
                {
                    compiled = Regex.Escape(compiled);
                }

                if (WholeWord)
                    compiled = "\\b" + compiled + "\\b";

                // Setup Regex options
                RegexOptions options = RegexOptions.None;

                if (CaseSensitive == false)
                    options |= RegexOptions.IgnoreCase;

                if (Direction == SearchDirection.Up)
                    options |= RegexOptions.RightToLeft;

                return new Regex(compiled, options);
            }
        }
    }
}