using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace MiguelUtil
{
    public class SearchTermNotFound : Exception
    {
        string Searched;
        public SearchTermNotFound(string searched)
        {
            Searched = searched;
        }

        public override string Message
        {
            get
            {
                return "Search term \"" + Searched + "\" not found!";
            }
        }
    }

    public class UCSExistsException : Exception
    {
        string FilePath;
        public UCSExistsException(string filepath)
        {
            FilePath = filepath;
        }

        public override string Message
        {
            get
            {
                return "UCS file at the file path \"" + FilePath + "\"already exists so cannot be created!";
            }
        }
    }

    public class FileIsntUCS : Exception
    {
        string FilePath;
        public FileIsntUCS(string filepath)
        {
            FilePath = filepath;
        }

        public override string Message
        {
            get
            {
                return "You have attempted to create a UCS called \"" + FilePath + "\" without a .ucs extension!";
            }
        }
    }

    public class NewKeyAlreadyUsed : Exception
    {
        uint OldKey;
        uint NewKey;
        public NewKeyAlreadyUsed(uint oldkey, uint newkey)
        {
            OldKey = oldkey;
            NewKey = newkey;
        }

        public override string Message
        {
            get
            {
                return "Key \"" + OldKey.ToString() + "\" cannot be replaced with \"" + NewKey.ToString() + "\" as is already in use.";
            }
        }
    }

    public class InvalidKeyValue : Exception
    {
        uint OldKey;
        string NewKey;
        public InvalidKeyValue(uint oldkey, string newkey)
        {
            OldKey = oldkey;
            NewKey = newkey;
        }

        public override string Message
        {
            get
            {
                return "Key \"" + OldKey.ToString() + "\" cannot be replaced with \"" + NewKey + "\" as it is not a number.";
            }
        }
    }

    public class InvalidUCSLine : Exception
    {
        string Line;
        string Exception;
        public InvalidUCSLine(string line, string exception)
        {
            Line = line;
            Exception = exception;
        }

        public override string Message
        {
            get
            {
                return "The line \"" + Line + "\" was invalid, and caused a " + Exception;
            }
        }
    }

    public class ProgramNotInstalled : Exception
    {
        string Program;
        public ProgramNotInstalled(string program)
        {
            Program = Path.GetFileName(program);
        }

        public override string Message
        {
            get
            {
                return "You do not have the program \"" + Program + "\" installed on your computer";
            }
        }
    }

    public class InvalidRBFLineException : Exception
    {
        public string Line;
        Exception _InnerException;
        public int LineIndex;

        public InvalidRBFLineException(string line, Exception e)
        {
            Line = line;
            _InnerException = e;
        }

        public InvalidRBFLineException(InvalidRBFLineException e, int lineindex)
        {
            Line = e.Line;
            _InnerException = e.InnerException;
            LineIndex = lineindex;
        }

        public override string Message
        {
            get
            {
                return "Line " + LineIndex.ToString() + " containing \"" + Line + "\", was invalid.";
            }
        }

        new public Exception InnerException
        {
            get
            {
                return _InnerException;
            }
        }
    }
}
