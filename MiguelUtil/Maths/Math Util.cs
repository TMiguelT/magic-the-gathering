﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiguelUtil.Maths
{
    public static class MathUtil
    {
        public static double NumberAfterDecimalPoint(double number)
        {
            double Return = (double)(number - Math.Truncate(number));
            return Return;
        }

        public static double NumberAfterDecimalPoint(decimal number)
        {
            return (double)(number - Math.Truncate(number));
        }

        public static int LengthAfterDecimalPoint(double number)
        {
            return NumberAfterDecimalPoint(number).ToString().Length;
        }

        public static int LengthAfterDecimalPoint(decimal number)
        {
            return NumberAfterDecimalPoint(number).ToString().Length;
        }
    }
}
