﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace MiguelUtil.Maths
{
    public struct Fraction
    {
        public long Numerator;
        public long Denominator;

        public Fraction(double Decimal)
        {
            double tempnumerator = Decimal;
            long tempdenominator = 1;
            while (MathUtil.NumberAfterDecimalPoint(tempnumerator) != 0)
            {
                tempnumerator = tempnumerator * 10;
                tempdenominator = tempdenominator * 10;
            }

            // Assign values
            Numerator = (long)tempnumerator;
            Denominator = tempdenominator;
        }


        public Fraction(double numerator, double denominator)
        {
            double tempnumerator = numerator;
            double tempdenominator = denominator;
            while (MathUtil.NumberAfterDecimalPoint(tempnumerator) != 0 || MathUtil.NumberAfterDecimalPoint(tempdenominator) != 0)
            {
                tempnumerator = tempnumerator * 10;
                tempdenominator = tempdenominator * 10;
            }

            // Assign values
            Numerator = (long)tempnumerator;
            Denominator = (long)tempdenominator;
        }

        public Fraction(long numerator, long denominator)
        {
            Numerator = numerator;
            Denominator = denominator;
        }

        public Fraction(string fraction)
        {
            StringReader Reader = new StringReader(fraction);
            char CharDigit;

            string tempnumerator = "";
            string tempdenominator = "";

            bool AddToNumerator = true;

            long Digit;

            while (true)
            {
                CharDigit = (char)Reader.Read();
                if (long.TryParse(CharDigit.ToString(), out Digit))
                {
                    if (AddToNumerator)
                    {
                        tempnumerator += CharDigit;
                    }
                    else
                    {
                        tempdenominator += CharDigit;
                    }
                }
                else if (CharDigit == '/' || CharDigit == '\\')
                {
                    AddToNumerator = false;
                }
                else if (Reader == null)
                {
                    break;
                }
                else
                {
                    throw new Exception("The input fraction was not in the format \"Numerator\\Denominator\"!");
                }
            }

            Numerator = Convert.ToInt64(tempnumerator);
            Denominator = Convert.ToInt64(tempdenominator);
        }

        public static implicit operator double(Fraction a)
        {
            return (double)a.Numerator / a.Denominator;
        }

        public static implicit operator float(Fraction a)
        {
            return (float)a.Numerator / a.Denominator;
        }

        public static implicit operator decimal(Fraction a)
        {
            return (decimal)a.Numerator / a.Denominator;
        }

        public static Fraction operator +(Fraction a, Fraction b)
        {
            return new Fraction(a.Numerator * b.Denominator + b.Numerator * a.Denominator, a.Denominator * b.Denominator);
        }

        public static Fraction operator *(Fraction b, int a)
        {
            return new Fraction(a * b.Numerator, b.Denominator);
        }

        public static Fraction operator +(Fraction b, int a)
        {
            return new Fraction(a * b.Denominator + b.Numerator, b.Denominator);
        }

        public static Fraction operator *(Fraction a, Fraction b)
        {
            return new Fraction(a.Numerator * b.Numerator, a.Denominator * b.Denominator);
        }

        public override string ToString()
        {
            return Numerator.ToString() + "\\" + Denominator.ToString();
        }
    }
}
