﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiguelUtil.RTF
{
    public class RichText
    {
        private static bool IsBracket(char input)
        {
            return input == '{' || input == '}';
        }

        public static bool HasHeader(string richtext)
        {
            int Index = richtext.IndexOf("\r\n");
            if (Index == -1)
                return false;
            else
            {
                string FirstLine = richtext.Substring(0, Index);
                if ((FirstLine.Count(IsBracket) >= 5))
                    return true;
                return false;
            }
        }

        public static string GetHeader(string richtext)
        {
            int Index = richtext.IndexOf("\r\n");
            if (Index == -1)
                throw new Exception("Header not present.");
            else
            {
                string FirstLine = richtext.Substring(0, Index + 1);
                return FirstLine;
            }
        }

        public static string RemoveHeader(string richtext)
        {
            int Index = richtext.IndexOf("\r\n");
            if (Index == -1)
                return richtext;
            else
            {
                string OtherLines = richtext.Substring(Index + 2);
                return OtherLines;
            }
        }

        private static string AddRichText(string WithHeader, string WithoutHeader)
        {
            string Trimmed = WithHeader.Trim();
            return Trimmed.Insert(Trimmed.Length - 1, WithoutHeader.TrimEnd(new char[]{'\r', '\n', '}'}));
        }

        public static string CombineRichText(string first, string second)
        {
            bool FirstHeader = HasHeader(first);
            bool SecondHeader = HasHeader(second);
            if (FirstHeader && SecondHeader)
                return AddRichText(first, RemoveHeader(second));
            else if (FirstHeader && SecondHeader == false)
                return AddRichText(first, second);
            else if (FirstHeader == false && SecondHeader)
                return AddRichText(second, first);
            else
                throw new Exception("Neither string has a header!");
        }
    }
}
