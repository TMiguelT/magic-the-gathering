﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace MiguelUtil.IO
{
    public static class FormatAnalysis
    {
        public static void XorDebugInt(byte[] ToXor)
        {
            StreamWriter DebugWrite = new StreamWriter(File.Open("Debug.log", FileMode.Create));
            byte[] Copy = new byte[ToXor.Length];
            for (int i = 0; i <= byte.MaxValue; i++)
            {
                ToXor.CopyTo(Copy, 0);
                int AsInt;
                IOMethods.XorArray(Copy, (byte)i);
                AsInt = BitConverter.ToInt32(Copy, 0);
                DebugWrite.Write(AsInt.ToString() + "\r\n\r\n");
            }
            DebugWrite.Close();
        }

        public static void XorDebugString(byte[] ToXor)
        {
            StreamWriter DebugWrite = new StreamWriter(File.Open("Debug.log", FileMode.Create));
            byte[] Copy = new byte[ToXor.Length];
            for (int i = 0; i <= byte.MaxValue; i++)
            {
                ToXor.CopyTo(Copy, 0);
                string AsString;
                IOMethods.XorArray(Copy, (byte)i);
                AsString = System.Text.ASCIIEncoding.ASCII.GetString(Copy);
                DebugWrite.Write(AsString + "\r\n\r\n");
            }
            DebugWrite.Close();
        }

        public static void XorFile(string path)
        {
            BinaryReaderEx reader = new BinaryReaderEx(File.Open(path, FileMode.Open));
            StreamWriter DebugWriter = new StreamWriter(File.Open("debug.log", FileMode.Create));
            byte[] Read = reader.ReadBytes((int)(reader.BaseStream.Length - reader.BaseStream.Position));
            for (int i = 0; i <= byte.MaxValue; i++)
            {
                byte j = (byte)i;
                SearchWithByte(path, Read, j, DebugWriter);
            }
            DebugWriter.Close();
        }

        private static void SearchWithByte(string filename, byte[] readfrom, byte XORByte, StreamWriter DebugWriter)
        {
            for (long i = 0; i < readfrom.Length; i++)
            {
                byte xored = (byte)(readfrom[i] ^ XORByte);
                if (((char)xored) == '.' && i + 3 < readfrom.Length)
                {
                    char First = (char)(readfrom[i + 1] ^ XORByte);
                    char Second = (char)(readfrom[i + 2] ^ XORByte);
                    char Third = (char)(readfrom[i + 3] ^ XORByte);
                    if (IOMethods.IsLowerLetter(First) && IOMethods.IsLowerLetter(Second) && IOMethods.IsLowerLetter(Third))
                    {
                        StringBuilder Debug = new StringBuilder(".");
                        Debug.Append(First);
                        Debug.Append(Second);
                        Debug.Append(Third);
                        WriteToLog(filename, i, Debug.ToString(), XORByte, DebugWriter);
                    }
                }
            }
        }

        public static void WriteToLog(string filename, long filepos, string value, byte XORedWith, StreamWriter writer)
        {
            writer.Write(filename + " - " + filepos.ToString() + ": " + value + " XORedWith: " + XORedWith.ToString() + "\r\n");
        }
    }
}
