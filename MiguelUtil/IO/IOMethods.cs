﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace MiguelUtil.IO
{
    public static class IOMethods
    {
        public static int ThreeByteInt(byte[] Input)
        {
            byte[] Return = new byte[4];
            Input.CopyTo(Return, 0);
            return BitConverter.ToInt32(Return, 0);
        }

        public static void XorArray(IList<byte> ToXor, byte XorWith)
        {
            for (int i = 0; i < ToXor.Count; i++)
            {
                ToXor[i] ^= XorWith;
            }
        }

        public static bool IsLowerLetter(char check)
        {
            return check < 123 && check > 96;
        }
    }
}
