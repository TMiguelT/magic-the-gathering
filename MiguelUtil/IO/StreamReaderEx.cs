﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MiguelUtil.IO
{
    public class StreamReaderEx : StreamReader
    {
        public bool UseOperation;
        public byte OperationByte;
        public BitOperation Operation;

        public StreamReaderEx(Stream input) : base(input) { }

        public StreamReaderEx(Stream input, byte operationbyte, BitOperation operation)
            : base(input)
        {
            OperationByte = operationbyte;
            Operation = operation;
        }

        public StreamReaderEx(Stream input, Encoding encoding) : base(input, encoding) { }

        public StreamReaderEx(Stream input, Encoding encoding, byte operationbyte, BitOperation operation)
            : base(input, encoding)
        {
            OperationByte = operationbyte;
            Operation = operation;
        }

        public override int Read()
        {
            return base.Read();
        }

        public override int Read(char[] buffer, int index, int count)
        {
            return base.Read(buffer, index, count);
        }

        public override int ReadBlock(char[] buffer, int index, int count)
        {
            return base.ReadBlock(buffer, index, count);
        }

        public override string ReadLine()
        {
            return base.ReadLine();
        }

        public override string ReadToEnd()
        {
            return base.ReadToEnd();
        }

        public string ReadNullPaddedString()
        {
            StringBuilder Builder = new StringBuilder();
            while (true)
            {
                char Read = (char) base.Read();
                if (Read == '\0')
                    break;
                else
                    Builder.Append(Read);
            }
            return ASCIIEncoding.ASCII.GetString(BitOperate(Builder.ToString()));
        }

        public string ReadString(int length)
        {
            char[] Chars = new char[length];
            base.Read(Chars, 0, length);
            return ASCIIEncoding.ASCII.GetString(BitOperate(new string(Chars)));
        }

        public void StartOperating()
        {
            UseOperation = true;
        }

        public void StartOperating(BitOperation operation, byte operatewith)
        {
            UseOperation = true;
            Operation = operation;
            OperationByte = operatewith;
        }

        public void StopOperating()
        {
            UseOperation = false;
        }

        private byte[] BitOperate(dynamic operateon)
        {
            byte[] asarray;
            if (operateon is char[] || operateon is string)
            {
                asarray = ASCIIEncoding.ASCII.GetBytes(operateon);
            }
            else if (operateon is byte[])
            {
                asarray = (byte[])operateon;
            }
            else
            {
                asarray = BitConverter.GetBytes(operateon);
            }

            if (UseOperation)
            {
                for (int i = 0; i < asarray.Length; i++)
                {
                    switch (Operation)
                    {
                        case BitOperation.Xor:
                            asarray[i] ^= OperationByte;
                            break;
                        case BitOperation.Or:
                            asarray[i] |= OperationByte;
                            break;
                        case BitOperation.And:
                            asarray[i] &= OperationByte;
                            break;
                    }
                }
            }
            return asarray;
        }
    }
}
