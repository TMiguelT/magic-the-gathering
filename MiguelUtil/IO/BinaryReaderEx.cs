﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MiguelUtil.IO
{
    public class BinaryReaderEx : BinaryReader
    {
        public bool UseOperation;
        public byte OperationByte;
        public BitOperation Operation;

        public BinaryReaderEx(Stream input) : base(input) { }

        public BinaryReaderEx(Stream input, byte operationbyte, BitOperation operation)
            : base(input)
        {
            OperationByte = operationbyte;
            Operation = operation;
        }

        public BinaryReaderEx(Stream input, Encoding encoding) : base(input, encoding) { }

        public BinaryReaderEx(Stream input, Encoding encoding, byte operationbyte, BitOperation operation)
            : base(input, encoding)
        {
            OperationByte = operationbyte;
            Operation = operation;
        }

        //public override int Read()
        //{
        //    return BitConverter.ToInt32(BitOperate(base.Read()), 0);
        //}

        //public override int Read(byte[] buffer, int index, int count)
        //{
        //    return BitConverter.ToInt32(BitOperate(base.Read(buffer, index, count)), 0);
        //}

        //public override int Read(char[] buffer, int index, int count)
        //{
        //    return BitConverter.ToInt32(BitOperate(base.Read(buffer, index, count)), 0);
        //}

        public override bool ReadBoolean()
        {
            return BitConverter.ToBoolean(BitOperate(base.ReadBoolean()), 0);
        }

        public override byte ReadByte()
        {
            return BitOperate(base.ReadByte())[0];
        }

        public override byte[] ReadBytes(int count)
        {
            return BitOperate(base.ReadBytes(count));
        }

        public override char ReadChar()
        {
            return BitConverter.ToChar(BitOperate(base.ReadChar()), 0);
        }

        public override char[] ReadChars(int count)
        {
            return ASCIIEncoding.ASCII.GetChars((BitOperate(base.ReadChars(count))));
        }

        public override decimal ReadDecimal()
        {
            return (decimal)BitConverter.ToDouble(BitOperate(base.ReadDecimal()), 0);
        }

        public override double ReadDouble()
        {
            return BitConverter.ToDouble(BitOperate(base.ReadDouble()), 0);
        }

        public override short ReadInt16()
        {
            return BitConverter.ToInt16(BitOperate(base.ReadInt16()), 0);
        }

        public override int ReadInt32()
        {
            return BitConverter.ToInt32(BitOperate(base.ReadInt32()), 0);
        }

        public override long ReadInt64()
        {
            return BitConverter.ToInt64(BitOperate(base.ReadInt64()), 0);
        }

        public override sbyte ReadSByte()
        {
            return (sbyte)BitConverter.ToInt16(BitOperate(base.ReadSByte()), 0);
        }

        public override float ReadSingle()
        {
            return BitConverter.ToSingle(BitOperate(base.ReadSingle()), 0);
        }

        public override string ReadString()
        {
            return ASCIIEncoding.ASCII.GetString(BitOperate(base.ReadString()));
        }

        /// <summary>
        /// Reads a chunk of data without moving the stream position.
        /// </summary>
        /// <param name="offset">The offset to start reading from.</param>
        /// <param name="count">The length of the chunk.</param>
        /// <returns>The read byte array.</returns>
        public byte[] ReadBlock(int offset, int count)
        {
            long Position = BaseStream.Position;
            BaseStream.Position = offset;
            byte[] Return = ReadBytes(count);
            BaseStream.Position = Position;
            return Return;
        }

        public string ReadNullTerminatedString()
        {
            StringBuilder Builder = new StringBuilder();
            while (true)
            {
                char Read = base.ReadChar();
                if (Read == '\0')
                    break;
                else
                    Builder.Append(Read);
            }
            return ASCIIEncoding.ASCII.GetString(BitOperate(Builder.ToString()));
        }

        public string ReadString(int length)
        {
            char[] Chars = base.ReadChars(length);
            return ASCIIEncoding.ASCII.GetString(BitOperate(new string(Chars)));
        }

        public override ushort ReadUInt16()
        {
            return BitConverter.ToUInt16(BitOperate(base.ReadUInt16()), 0);
        }

        public override uint ReadUInt32()
        {
            return BitConverter.ToUInt32(BitOperate(base.ReadUInt32()), 0);
        }

        public override ulong ReadUInt64()
        {
            return BitConverter.ToUInt64(BitOperate(base.ReadUInt64()), 0);
        }

        public void StartOperating()
        {
            UseOperation = true;
        }

        public void StartOperating(BitOperation operation, byte operatewith)
        {
            UseOperation = true;
            Operation = operation;
            OperationByte = operatewith;
        }

        public void StopOperating()
        {
            UseOperation = false;
        }

        private byte[] BitOperate(dynamic operateon)
        {
            byte[] asarray;
            if (operateon is char[] || operateon is string)
            {
                asarray = ASCIIEncoding.ASCII.GetBytes(operateon);
            }
            else if (operateon is byte[])
            {
                asarray = (byte[])operateon;
            }
            else
            {
                asarray = BitConverter.GetBytes(operateon);
            }

            if (UseOperation)
            {
                for (int i = 0; i < asarray.Length; i++)
                {
                    switch (Operation)
                    {
                        case BitOperation.Xor:
                            asarray[i] ^= OperationByte;
                            break;
                        case BitOperation.Or:
                            asarray[i] |= OperationByte;
                            break;
                        case BitOperation.And:
                            asarray[i] &= OperationByte;
                            break;
                    }
                }
            }
            return asarray;
        }
    }
}
