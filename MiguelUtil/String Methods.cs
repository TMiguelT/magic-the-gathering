using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Diagnostics;
using MiguelUtil.Find;
using System.Text.RegularExpressions;

namespace StringExtensions
{
    public static class StringMethods
    {
        public static StringComparison CaseSensitive(bool sensitive)
        {
            if (sensitive)
            {
                return StringComparison.CurrentCulture;
            }
            else
            {
                return StringComparison.CurrentCultureIgnoreCase;
            }
        }

        public static string ToUnicode(string input)
        {
            StringBuilder sb = new StringBuilder();
            if (input.Substring(0, 2).Equals("0x"))
                input = input.Substring(2);
            for (int i = 0; i <= input.Length - 2; i += 2)
            {
                sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(input.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
            }
            return sb.ToString();
        }
    }
}
